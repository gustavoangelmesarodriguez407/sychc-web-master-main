import React, { useState, useEffect } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { Hidden, LinearProgress } from '@material-ui/core'
import { useStyles } from './useStyles'

/**Components */
import DrawerMenu from '../components/menus/drawers/DrawerMenu'
import Header from '../components/menus/headers/PrivateHeader'

/**API */
import { obtenerSesionApi } from '../api/Sesion'

function LayoutAdmin(props) {
    const classes = useStyles()
    const { routes, match: { path } } = props

    const [tienePermiso, setTienePermiso] = useState(false)
    const [isLogged, setIsLogged] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [open, setOpen] = useState(false)

    useEffect(() => {
        document.title = 'Panel de administración - SyCHC'
        //Inicia la carga
        setIsLoading(true)

        //Si no hay sesión local
        if (!window.localStorage.getItem('SESSION_ID')) {
            setIsLoading(false)
            setIsLogged(false)
            return
        }

        obtenerSesionApi(window.localStorage.getItem('SESSION_ID'))
            .then(response => {
                if (response.status === 200) {
                    response.result.then(ses => {
                        //Evalúa si es admin o gerente
                        if (ses.tipoUsuario === 'admin' || ses.tipoUsuario === 'gerente')
                            setTienePermiso(true)
                        else
                            setTienePermiso(false)

                        //Si hay sesión en la bd
                        setIsLoading(false)
                        setIsLogged(true)
                    })
                } else {
                    //Borra los datos del local storage
                    window.localStorage.removeItem('SESSION_ID')

                    //Si no hay sesión en la bd
                    setIsLoading(false)
                    setIsLogged(false)
                }
            })
    }, [])

    /**Mostrar la carga mientras busca en la bd */
    if (isLoading) {
        return <LinearProgress variant='indeterminate' />
    }

    /**Redirigir a login si no hay sesión en la base de datos o en el local storage 
    */
    if (isLogged === false && !isLoading) {
        return <Redirect to='/' />
    }

    /**Si no tiene permiso, redirige a su panel perteneciente */
    if (isLogged && !isLoading && !tienePermiso) {
        return <Redirect to='/panel' />
    }

    return (
        <div>
            <div className={classes.root}>
                <Header position="fixed" OpenAction={() => setOpen(!open)} />
                <Hidden xsDown>
                    <DrawerMenu
                        variant="permanent"
                        open={true} title={path}
                        url={props.location.pathname} />
                </Hidden>
                <Hidden smUp>
                    <DrawerMenu
                        variant="temporary"
                        open={open}
                        onClose={() => setOpen(!open)}
                        url={props.location.pathname}
                    />
                </Hidden>
                <div className={classes.content}>
                    <div className={classes.toolbar}></div>
                    <LoadRoute routes={routes} />
                </div>
            </div>
        </div>
    )
}

function LoadRoute({ routes }) {
    return (
        <Switch>
            {
                routes.map((route, index) => (
                    <Route
                        key={index}
                        path={route.path}
                        exact={route.exact}
                        component={route.component} />
                ))
            }
        </Switch>
    )
}

export default LayoutAdmin
