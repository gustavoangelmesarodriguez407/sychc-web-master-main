import Login from '../pages/public/Login'

/**Layouts */
import LayoutAdmin from '../layouts/LayoutAdmin'
import LayoutPanel from '../layouts/LayoutPanel'

/**Admin */
import AdminResumen from '../pages/admin/resumen/Resumen'
import AdminReportes from '../pages/admin/reportes/Reportes'

import AdminUsuarios from '../pages/admin/usuarios/Usuarios'
import AdminModificarUsuario from '../pages/admin/usuarios/ModificarUsuario'

import AdminTiposUsuarios from '../pages/admin/tipos_usuario/TiposUsuarios'
import AdminModificarTipoUsuario from '../pages/admin/tipos_usuario/ModificarTipoUsuario'

import AdminConsultores from '../pages/admin/consultores/Consultores'
import AdminModificarConsultor from '../pages/admin/consultores/ModificarConsultor'

import AdminPerfiles from '../pages/admin/perfiles/Perfiles'
import AdminModificarPerfil from '../pages/admin/perfiles/ModificarPerfil'

import AdminClientes from '../pages/admin/clientes/Clientes'
import AdminModificarCliente from '../pages/admin/clientes/ModificarCliente'

import AdminProyectos from '../pages/admin/proyectos/Proyectos'
import AdminModificarProyecto from '../pages/admin/proyectos/ModificarProyecto'

import AdminEtapas from '../pages/admin/etapas/Etapas'
import AdminCategorias from '../pages/admin/categoria/Categoria'
import AdminModificarEtapa from '../pages/admin/etapas/ModificarEtapa'
import AdminModificarCategoria from '../pages/admin/categoria/ModificarCategoria'

import AdminActividad from '../pages/admin/actividades/Actividad'
//import AdminModificarActividad from '../pages/admin/actividades/ModificarActividad'

import AdminModulos from '../pages/admin/modulos/Modulos'
import AdminModificarModulo from '../pages/admin/modulos/ModificarModulo'

import AdminFunciones from '../pages/admin/funciones/Funciones'
import AdminModificarFuncion from '../pages/admin/funciones/ModificarFuncion'

import AdminAccesos from '../pages/admin/accesos/Accesos'
import AdminModificarAcceso from '../pages/admin/accesos/ModificarAcceso'

import AdminSesiones from '../pages/admin/sesiones/Sesiones'

/**Panel y general */
import PanelResumen from '../pages/panel/resumen/Resumen'
import MisProyectos from '../pages/panel/proyectos/MisProyectos'
import PanelReportes from '../pages/panel/reportes/Reportes'
import VerProyecto from '../pages/panel/proyectos/VerProyecto'
import PerfilesConsultor from '../pages/admin/consultores/PerfilesConsultor'
import ConsultoresProyecto from '../pages/admin/proyectos/ConsultoresProyecto'
import EtapasProyecto from '../pages/admin/proyectos/EtapasProyecto'
import ClienteUsuarios from '../pages/admin/clientes/ClienteUsuarios'
import VerActividad from '../pages/panel/actividades/VerActividad'

const routes = [
    /**Página principal */
    {
        path: '/',
        component: Login,
        exact: true,
        routes: [
            {
                component: null
            }
        ]
    },
    {
        path: '/admin',
        component: LayoutAdmin,
        exact: false,
        routes: [
            {
                path: '/admin',
                component: AdminResumen,
                exact: true,
            },
            {
                path: '/admin/reportes',
                component: AdminReportes,
                exact: true,
            },
            {
                path: '/admin/usuarios',
                component: AdminUsuarios,
                exact: true,
            },
            {
                path: '/admin/usuarios/:id',
                component: AdminModificarUsuario,
                exact: true,
            },
            {
                path: '/admin/tiposusuarios',
                component: AdminTiposUsuarios,
                exact: true,
            },
            {
                path: '/admin/tiposusuarios/:id',
                component: AdminModificarTipoUsuario,
                exact: true,
            },
            {
                path: '/admin/consultores',
                component: AdminConsultores,
                exact: true,
            },
            {
                path: '/admin/consultores/:id',
                component: AdminModificarConsultor,
                exact: true,
            },
            {
                path: '/admin/perfiles',
                component: AdminPerfiles,
                exact: true,
            },
            {
                path: '/admin/perfiles/:id',
                component: AdminModificarPerfil,
                exact: true,
            },
            {
                path: '/admin/clientes',
                component: AdminClientes,
                exact: true,
            },
            {
                path: '/admin/clientes/:id',
                component: AdminModificarCliente,
                exact: true,
            },
            {
                path: '/admin/proyectos',
                component: AdminProyectos,
                exact: true,
            },
            {
                path: '/admin/proyectos/:id',
                component: AdminModificarProyecto,
                exact: true,
            },
            {
                path: '/admin/proyectos/panel/:id',
                component: VerProyecto,
                exact: true,
            },
            {
                path: '/admin/ver-actividad/:id',
                component: VerActividad,
                exact: true,
            },
            {
                path: '/admin/etapas',
                component: AdminEtapas,
                exact: true,
            },
            {
                path: '/admin/etapas/:id',
                component: AdminModificarEtapa,
                exact: true,
            },
            {
                path: '/admin/categoria',
                component: AdminCategorias,
                exact: true,
            },
            {
                path: '/admin/categoria/:id',
                component: AdminModificarCategoria,
                exact: true,
            },
            {
                path: '/admin/actividades',
                component: AdminActividad,
                exact: true,
            },
            {
                path: '/admin/actividades/:id',
                component: VerActividad,
                exact: true,
            },
            {
                path: '/admin/modulos',
                component: AdminModulos,
                exact: true,
            },
            {
                path: '/admin/modulos/:id',
                component: AdminModificarModulo,
                exact: true,
            },
            {
                path: '/admin/funciones',
                component: AdminFunciones,
                exact: true,
            },
            {
                path: '/admin/funciones/:id',
                component: AdminModificarFuncion,
                exact: true,
            },
            {
                path: '/admin/accesos',
                component: AdminAccesos,
                exact: true,
            },
            {
                path: '/admin/accesos/:id',
                component: AdminModificarAcceso,
                exact: true,
            },
            {
                path: '/admin/sesiones',
                component: AdminSesiones,
                exact: true,
            },
            {
                path: '/admin/perfiles-consultor/:id',
                component: PerfilesConsultor,
                exact: true,
            },
            {
                path: '/admin/clientes-usuario/:id',
                component: ClienteUsuarios,
                exact: true,
            },
            {
                path: '/admin/consultores-proyecto/:id',
                component: ConsultoresProyecto,
                exact: true,
            },
            {
                path: '/admin/etapas-proyecto/:id',
                component: EtapasProyecto,
                exact: true,
            },
            {
                component: null
            }
        ]
    },
    {
        path: '/panel',
        component: LayoutPanel,
        exact: false,
        routes: [
            {
                path: '/panel',
                component: PanelResumen,
                exact: true,
            },
            {
                path: '/panel/mis-proyectos',
                component: MisProyectos,
                exact: true,
            },
            {
                path: '/panel/reportes',
                component: PanelReportes,
                exact: true,
            },
            {
                path: '/panel/ver-proyecto/:id',
                component: VerProyecto,
                exact: true,
            },
            {
                path: '/panel/ver-actividad/:id',
                component: VerActividad,
                exact: true,
            },
            {
                component: null
            }
        ]
    },
]

export default routes