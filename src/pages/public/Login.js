import React, { Fragment, useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import {
    Paper, Typography, TextField, Button, InputAdornment,
    LinearProgress
} from '@material-ui/core'
import theme from '../../styles/Theme'
import useStyles from './useStyles'

/**Components */
import PublicHeader from '../../components/menus/headers/PublicHeader'
import SnackbarAlert from '../../components/SnackbarAlert'

/**API */
import { getIpApi } from '../../api/GetIp'
import { iniciarSesionApi, obtenerSesionApi } from '../../api/Sesion'

/**Helpers */
import { emailValidation, minLengthValidation } from '../../helpers/Validations'

/**Images */
import logoDark from '../../assets/svg/vsi-logo-dark.svg'
import logoLight from '../../assets/svg/vsi-logo-light.svg'

/**Icons */
import EmailIcon from '@material-ui/icons/Email'
import VpnKeyIcon from '@material-ui/icons/VpnKey'

function Login() {
    const classes = useStyles();
    const [inputs, setInputs] = useState({
        correoElectronico: '',
        contrasena: '',
        direccionIp: ''
    })
    const [sesionData, setSesionData] = useState({})
    const [isLogged, setIsLogged] = useState(null)
    const [isSearching, setIsSearching] = useState(true)

    const [isLoading, setIsLoading] = useState(false)
    const [open, setOpen] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeveriry] = useState('error')

    //Efecto para traer la ip
    useEffect(() => {
        document.title = 'Sistema de Control de Horas de Consultoría'

        //Busca la IP
        getIpApi().then(response => {
            if (response.ip) {
                setInputs({ ...inputs, direccionIp: response.ip })
            } else {
                setSeveriry('info')
                setMessage('Desactive su bloqueador de anuncios para mejorar la navegación.')
                setOpen(true)
                setInputs({ ...inputs, direccionIp: '' })
            }
        })

        //Si no hay sesión local
        if (!window.localStorage.getItem('SESSION_ID')) {
            setIsSearching(false)
            setIsLogged(false)
            return
        }

        //Busca la sesión si hay
        buscarSesion(window.localStorage.getItem('SESSION_ID'))

        return () => setInputs({})
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    //Función para validar e iniciar sesión
    const login = () => {
        const data = inputs

        if (!minLengthValidation(1, data.correoElectronico.trim()) ||
            !minLengthValidation(1, data.contrasena.trim())) {
            setSeveriry('error')
            setMessage('Ingrese sus credenciales.')
            setOpen(true)

            return
        }

        if (!emailValidation(data.correoElectronico.trim())) {
            setSeveriry('error')
            setMessage('Ingrese un correo válido.')
            setOpen(true)

            return
        }

        //Limpia los campos
        data.correoElectronico = data.correoElectronico.trim()
        data.direccionIp = data.direccionIp.trim()

        //Inicia la carga
        setIsLoading(true)

        //Iniciar sesión desde el servicio
        iniciarSesionApi(inputs).then(response => {
            if (response.status === 200) {
                response.result.then(sesion => {

                    window.localStorage.setItem('SESSION_ID', sesion.clave)

                    //Busca la sesión
                    buscarSesion(sesion.clave)
                })
            } else {
                setIsLoading(false)
                response.result.then(text => {
                    setSeveriry('error')
                    setMessage(text)
                    setOpen(true)
                })
            }
        })
    }

    const buscarSesion = (clave) => {
        obtenerSesionApi(clave)
            .then(response => {
                if (response.status === 200) {
                    //Si hay sesión en la bd
                    response.result.then(s => {
                        setSesionData(s)
                        setIsSearching(false)
                        setIsLogged(true)
                    })
                } else {
                    //Si no hay sesión en la bd
                    setIsSearching(false)
                    setIsLogged(false)
                }
            })
    }

    if (!isSearching && isLogged) {
        if (sesionData.tipoUsuario === 'admin' || sesionData.tipoUsuario === 'gerente') {
            return <Redirect to='/admin' />
        } else {
            return <Redirect to='/panel' />
        }
    }

    return (
        <Fragment>
            {
                isSearching ? <LinearProgress variant='indeterminate' /> :
                    <Fragment>
                        <SnackbarAlert
                            open={open}
                            onClose={() => setOpen(false)}
                            severity={severity}>
                            {message}
                        </SnackbarAlert>
                        <PublicHeader />
                        <div className={classes.root}>
                            <Paper elevation={3} className={classes.paper}>
                                <div className={classes.titleContainer}>
                                    <img src={theme.palette.type === 'light' ?
                                        logoLight : logoDark} alt='sychc.svg'
                                        className={classes.logo} />
                                    <Typography className={classes.title}>Iniciar sesión</Typography>
                                </div>
                                <Typography>* Todos los campos son requeridos</Typography>
                                <form onChange={changeForm} className={classes.form}>
                                    <TextField
                                        name='correoElectronico'
                                        type='text'
                                        variant='outlined'
                                        label='Correo electrónico'
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <EmailIcon style={{ color: theme.palette.primary.light }} />
                                                </InputAdornment>
                                            ),
                                        }}
                                        className={classes.textField} />
                                    <TextField
                                        name='contrasena'
                                        type='password'
                                        variant='outlined'
                                        label='Contraseña'
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <VpnKeyIcon style={{ color: theme.palette.primary.light }} />
                                                </InputAdornment>
                                            ),
                                        }}
                                        className={classes.textField} />
                                </form>
                                {
                                    !isLoading ?
                                        <Button onClick={login} color='primary' variant='contained'>
                                            Acceder
                                        </Button> :
                                        <LinearProgress variant='indeterminate' />
                                }
                            </Paper>
                        </div>
                    </Fragment>
            }
        </Fragment>
    )
}

export default Login
