import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        width: '360px',
        height: '416px',
        marginTop: theme.spacing(5),
        marginBottom: theme.spacing(5),
        marginInline: theme.spacing(2),
        padding: theme.spacing(2)
    },
    titleContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        marginBottom: theme.spacing(5)
    },
    title: {
        fontSize: '4vh',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    form: {
        marginBlock: 15
    },
    textField: {
        width: '100%',
        marginBottom: 20
    },
    logo: {
        width: '200px',
        alignSelf: 'center',
        marginBottom: theme.spacing(2)
    }
}))

export default useStyles;