import React from 'react'
import { Typography, Grid } from '@material-ui/core'
import { useStyles } from './useStyles'

/**Compoents */
import CantidadProyectos from '../../../components/graficas/admin/CantidadProyectos'
import CantidadUsuarios from '../../../components/graficas/admin/CantidadUsuarios'
import ActividadesSemana from '../../../components/graficas/admin/ActividadesSemana'

function Resumen() {
    const classes = useStyles()
    return (
        <div>
            <Typography variant='h4' gutterBottom>
                Inicio
            </Typography>
            <div className={classes.seccion}>
                <CantidadProyectos />
            </div>
            <div className={classes.seccion}>
                <Grid container spacing={2}>
                    <Grid item xl={5} lg={5} md={5} sm={12} xs={12}>
                        <CantidadUsuarios />
                    </Grid>
                    <Grid item xl={7} lg={7} md={7} sm={12} xs={12}>
                        <ActividadesSemana />
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}

export default Resumen
