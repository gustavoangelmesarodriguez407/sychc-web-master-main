import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles(theme => ({
    seccion: {
        marginBottom: theme.spacing(2)
    }
}))