import React, { useState, Fragment } from 'react'
import {
    Dialog, DialogTitle, DialogContent,
    DialogActions, DialogContentText, TextField,
    Button, Grid, FormControl, InputLabel, Select,
    MenuItem, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**API */
import { PostApi } from '../../../api/Services'

function CrearPerfil(props) {
    const classes = useStyles()
    const { open, handleClose, handleReload } = props
    const [inputs, setInputs] = useState({
        nombre: '',
        nivel: 'Junior',
        costo: 0,
    })
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        if (e.target.type === 'number') {
            setInputs({
                ...inputs,
                [e.target.name]: Number(e.target.value)
            })
        } else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            })
        }
    }

    //Función para crear usuario
    const create = () => {
        const data = inputs

        if (!minLengthValidation(3, data.nombre)) {
            setMessage('Ingrese un nombre válido de más de 3 caracteres.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.costo <= 0) {
            setMessage('El costo debe ser mayor a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Inicia la carga
        setIsLoading(true)

        //Guarda los datos
        PostApi('perfil', data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Perfil añadido correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    //Cierra modal
                    handleClose()
                    handleReload()
                    //Limpia los estados
                    setInputs({
                        ...inputs,
                        nombre: '',
                        nivel: 'Junior',
                        costo: 0,
                    })
                    setIsLoading(false)
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Dialog
                open={open}
                onClose={handleClose}>
                <DialogTitle>Crear perfil</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Rellene todos los campos para crear un nuevo perfil.
                    </DialogContentText>
                    <form onChange={changeForm}>
                        <Grid container spacing={2}>
                            <Grid item lg={12}>
                                <TextField
                                    name='nombre'
                                    variant='filled'
                                    label='Nombre'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <TextField
                                    name='costo'
                                    type='number'
                                    variant='filled'
                                    label='Costo (USD)'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="nivelLabel">Nivel</InputLabel>
                                    <Select
                                        name='nivel'
                                        labelId="nivelLabel"
                                        onChange={changeForm}
                                        value={inputs.nivel}>
                                        <MenuItem value='Junior'>Junior</MenuItem>
                                        <MenuItem value='Senior'>Senior</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                </DialogContent>
                <DialogActions>
                    {
                        isLoading ?
                            <CircularProgress variant='indeterminate' /> :
                            <Fragment>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={create}>
                                    Aceptar
                                </Button>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    onClick={handleClose}>
                                    Cancelar
                                </Button>
                            </Fragment>
                    }
                </DialogActions>
            </Dialog>
        </Fragment>
    )
}

export default CrearPerfil
