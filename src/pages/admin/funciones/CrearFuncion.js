import React, { useState, useEffect, Fragment } from 'react'
import {
    Dialog, DialogTitle, DialogContent,
    DialogActions, DialogContentText, Select, MenuItem, InputLabel,
    Button, Grid, CircularProgress, FormControl
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**API */
import { GetApi, GetByIdApi, PostApi } from '../../../api/Services'

function CrearFuncion(props) {
    const classes = useStyles()
    const { open, handleClose, handleReload } = props
    const [inputs, setInputs] = useState({
        accion: 'crear',
        idModulo: '',
        ultimaModificacion: null,
        modificadoPor: ''
    })
    const [modulos, setModulos] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')
    const [idUsuario, setidUsuario] = useState(null)
    const session_id = window.localStorage.getItem('SESSION_ID')

    //Efecto que trae datos del usuario
    useEffect(() => {
        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setidUsuario(ses.idUsuario)
                })
            }
        })

        GetApi('modulo').then(response => {
            if (response.status === 200) {
                response.result.then(mod => {
                    setModulos(mod)
                })
            }
        })

        return () => {
            setidUsuario(null)
            setModulos([])
        }
    }, [session_id])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    //Función para crear usuario
    const create = () => {
        const data = inputs

        if (!minLengthValidation(1, data.idModulo)) {
            setMessage('Seleccione un módulo')
            setSeverity('error')
            setOpenAlert(true)
        }

        //Modificado por
        data.modificadoPor = idUsuario

        //Inicia la carga
        setIsLoading(true)

        //Guarda los datos
        PostApi('funcion', data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Función añadida correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    //Cierra modal
                    handleClose()
                    handleReload()
                    //Limpia los estados
                    setInputs({
                        ...inputs,
                        nombre: '',
                        modificadoPor: idUsuario,
                        ultimaModificacion: null
                    })
                    setIsLoading(false)
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Dialog
                open={open}
                onClose={handleClose}>
                <DialogTitle>Crear usuario</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Rellene todos los campos para crear un nuevo usuario.
                    </DialogContentText>
                    <form onChange={changeForm}>
                        <Grid container spacing={2}>
                            <Grid item lg={6}>
                                <FormControl variant='filled' className={classes.formControl}>
                                    <InputLabel id='accLabel'>Acción</InputLabel>
                                    <Select
                                        name='accion'
                                        onChange={changeForm}
                                        value={inputs.accion}>
                                        <MenuItem value='crear'>Crear</MenuItem>
                                        <MenuItem value='ver'>Ver</MenuItem>
                                        <MenuItem value='modificar'>Modificar</MenuItem>
                                        <MenuItem value='eliminar'>Eliminar</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item lg={6}>
                                <FormControl variant='filled' className={classes.formControl}>
                                    <InputLabel id='modLabel'>Modulo</InputLabel>
                                    <Select
                                        name='idModulo'
                                        onChange={changeForm}
                                        value={inputs.idModulo}>
                                        {
                                            modulos.map((value, index) => (
                                                <MenuItem value={value.id} key={index}>
                                                    {value.nombre}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                </DialogContent>
                <DialogActions>
                    {
                        isLoading ?
                            <CircularProgress variant='indeterminate' /> :
                            <Fragment>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={create}>
                                    Aceptar
                                </Button>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    onClick={handleClose}>
                                    Cancelar
                                </Button>
                            </Fragment>
                    }
                </DialogActions>
            </Dialog>
        </Fragment>
    )
}

export default CrearFuncion
