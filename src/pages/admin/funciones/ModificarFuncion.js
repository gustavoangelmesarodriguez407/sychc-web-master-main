import React, { useState, useEffect, Fragment } from 'react'
import { Redirect, Link } from 'react-router-dom'
import {
    Button, Grid, CircularProgress, Typography,
    Select, FormControl, InputLabel, MenuItem,
    LinearProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
//import { minLengthValidation } from '../../../helpers/Validations'

/**API */
import { GetApi, GetByIdApi, PutApi } from '../../../api/Services'

function ModificarFuncion(props) {
    const { match: { params: { id } } } = props
    const parentPath = '/admin/funciones'
    const classes = useStyles()

    const [inputs, setInputs] = useState({})
    const [modulos, setModulos] = useState([])
    const [notFound, setNotFound] = useState(false)
    const [isLoadingData, setIsLoadingData] = useState(true)
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')
    const [idUsuario, setidUsuario] = useState(null)
    const session_id = window.localStorage.getItem('SESSION_ID')

    //Efecto que trae los tipos de usuario y la info del usuario
    useEffect(() => {
        GetByIdApi('funcion', id).then(response => {
            if (response.status === 200) {
                response.result.then(funcion => {
                    setInputs(funcion)
                    setIsLoadingData(false)
                })
            } else {
                setNotFound(true)
            }
        })

        GetApi('modulo').then(response => {
            if (response.status === 200) {
                response.result.then(mod => {
                    setModulos(mod)
                })
            }
        })

        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setidUsuario(ses.idUsuario)
                })
            }
        })

        return () => {
            setInputs([])
            setModulos([])
        }
    }, [id, session_id])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    //Función para crear usuario
    const update = () => {
        const data = inputs

        //Ultima modificacion
        data.ultimaModificacion = null
        data.modificadoPor = idUsuario

        //Inicia la carga
        setIsLoading(true)

        //Guarda los datos
        PutApi('funcion', id, data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Función modificada correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    setTimeout(() => {
                        setNotFound(true)
                    }, 500);
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    //Si no fue encontrado el dato
    if (notFound) {
        return <Redirect to={parentPath} />
    }

    //En lo que carga la información del registro
    if (isLoadingData) {
        return <LinearProgress variant='indeterminate' />
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Typography variant='h5'>Modificar función</Typography>
            <div>
                <Typography variant='subtitle1'>
                    Rellene todos los campos para modificar los datos.
                </Typography>
                <form onChange={changeForm} className={classes.form}>
                    <Grid container spacing={2}>
                        <Grid item lg={6}>
                            <FormControl variant='filled' className={classes.formControl}>
                                <InputLabel id='accLabel'>Acción</InputLabel>
                                <Select
                                    name='accion'
                                    onChange={changeForm}
                                    value={inputs.accion}
                                    defaultValue={inputs.accion}>
                                    <MenuItem value='crear'>Crear</MenuItem>
                                    <MenuItem value='ver'>Ver</MenuItem>
                                    <MenuItem value='modificar'>Modificar</MenuItem>
                                    <MenuItem value='eliminar'>Eliminar</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={6}>
                            <FormControl variant='filled' className={classes.formControl}>
                                <InputLabel id='modLabel'>Modulo</InputLabel>
                                <Select
                                    name='idModulo'
                                    onChange={changeForm}
                                    value={inputs.idModulo}>
                                    {
                                        modulos.map((value, index) => (
                                            <MenuItem value={value.id} key={index}>
                                                {value.nombre}
                                            </MenuItem>
                                        ))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <div>
                {
                    isLoading ?
                        <CircularProgress variant='indeterminate' /> :
                        <div className={classes.buttonContainer}>
                            <Button
                                variant='contained'
                                color='primary'
                                className={classes.button}
                                onClick={update}>
                                Aceptar
                            </Button>
                            <Link to={parentPath} className={classes.link}>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    className={classes.button}>
                                    Cancelar
                                </Button>
                            </Link>
                        </div>
                }
            </div>
        </Fragment>
    )
}

export default ModificarFuncion
