import React, { useState, useEffect, Fragment } from 'react'
import { Redirect, Link } from 'react-router-dom'
import {
    TextField, Button, Grid, CircularProgress, Typography,
    LinearProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**API */
import { GetByIdApi, PutApi } from '../../../api/Services'

function ModificarTipoUsuario(props) {
    const { match: { params: { id } } } = props
    const parentPath = '/admin/tiposusuarios'
    const classes = useStyles()

    const [inputs, setInputs] = useState({})
    const [notFound, setNotFound] = useState(false)
    const [isLoadingData, setIsLoadingData] = useState(true)
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae los tipos de usuario y la info del usuario
    useEffect(() => {
        GetByIdApi('tiposusuario', id).then(response => {
            if (response.status === 200) {
                response.result.then(data => {
                    setInputs(data)
                    setIsLoadingData(false)
                })
            } else {
                setNotFound(true)
            }
        })

        return () => setInputs([])
    }, [id])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    //Función para crear usuario
    const update = () => {
        const data = inputs

        if (!minLengthValidation(1, data.tipo)) {
            setMessage('Ingrese un nombre.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Inicia la carga
        setIsLoading(true)

        //Guarda los datos
        PutApi('tiposusuario', id, data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Tipo de usuario modificado correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    setTimeout(() => {
                        setNotFound(true)
                    }, 500);
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    //Si no fue encontrado el dato
    if (notFound) {
        return <Redirect to={parentPath} />
    }

    //En lo que carga la información del registro
    if (isLoadingData) {
        return <LinearProgress variant='indeterminate' />
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Typography variant='h5'>Modificar usuario</Typography>
            <div>
                <Typography variant='subtitle1'>
                    Rellene todos los campos para modificar los datos.
                </Typography>
                <form onChange={changeForm} className={classes.form}>
                    <Grid container spacing={2}>
                        <Grid item lg={12}>
                            <TextField
                                name='tipo'
                                variant='filled'
                                label='Nombre (tipo)'
                                defaultValue={inputs.tipo}
                                className={classes.formControl} />
                        </Grid>
                    </Grid>
                </form>
            </div>
            <div>
                {
                    isLoading ?
                        <CircularProgress variant='indeterminate' /> :
                        <div className={classes.buttonContainer}>
                            <Button
                                variant='contained'
                                color='primary'
                                className={classes.button}
                                onClick={update}>
                                Aceptar
                            </Button>
                            <Link to={parentPath} className={classes.link}>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    className={classes.button}>
                                    Cancelar
                                </Button>
                            </Link>
                        </div>
                }
            </div>
        </Fragment>
    )
}

export default ModificarTipoUsuario
