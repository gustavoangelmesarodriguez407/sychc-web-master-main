import React, { useState, useEffect, Fragment } from 'react'
import {
    Dialog, DialogTitle, DialogContent,
    DialogActions, DialogContentText, TextField,
    Button, Grid, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**API */
import { GetByIdApi, PostApi } from '../../../api/Services'

function CrearModulo(props) {
    const classes = useStyles()
    const { open, handleClose, handleReload } = props
    const [inputs, setInputs] = useState({
        nombre: '',
        modificadoPor: '',
        ultimaModificacion: null
    })
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')
    const [idUsuario, setIdUsuario] = useState(null)
    const session_id = window.localStorage.getItem('SESSION_ID')

    //Efecto que trae el id de usaurio
    useEffect(() => {
        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setIdUsuario(ses.idUsuario)
                })
            }
        })
        return () => {
            setIdUsuario(null)
        }
    }, [session_id])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    //Función para crear usuario
    const create = () => {
        const data = inputs

        if (!minLengthValidation(1, data.nombre)) {
            setMessage('El módulo debe tener un nombre.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Inicia la carga
        setIsLoading(true)

        //Modificado por
        data.modificadoPor = idUsuario

        //Guarda los datos
        PostApi('modulo', data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Módulo añadido correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    //Cierra modal
                    handleClose()
                    handleReload()
                    //Limpia los estados
                    setInputs({
                        ...inputs,
                        nombre: ''
                    })

                    setIsLoading(false)
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Dialog
                open={open}
                onClose={handleClose}>
                <DialogTitle>Crear Módulo</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Rellene todos los campos para crear un nuevo tipo de usuario.
                        Los nuevos módulos deben estar escritos en plural y empezar en mayúscula. Ejemplo: Proveedores, Departamentos
                    </DialogContentText>
                    <form onChange={changeForm}>
                        <Grid container spacing={2}>
                            <Grid item lg={12}>
                                <TextField
                                    name='nombre'
                                    variant='filled'
                                    label='Nombre'
                                    className={classes.formControl} />
                            </Grid>
                        </Grid>
                    </form>
                </DialogContent>
                <DialogActions>
                    {
                        isLoading ?
                            <CircularProgress variant='indeterminate' /> :
                            <Fragment>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={create}>
                                    Aceptar
                                </Button>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    onClick={handleClose}>
                                    Cancelar
                                </Button>
                            </Fragment>
                    }
                </DialogActions>
            </Dialog>
        </Fragment>
    )
}

export default CrearModulo
