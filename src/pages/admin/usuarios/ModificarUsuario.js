import React, { useState, useEffect, Fragment } from 'react'
import { Redirect, Link } from 'react-router-dom'
import {
    TextField, Button, Grid, FormControl, InputLabel, Select,
    MenuItem, CircularProgress, Typography, LinearProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { emailValidation, minLengthValidation } from '../../../helpers/Validations'

/**API */
import { GetApi, GetByIdApi, PutApi } from '../../../api/Services'

function ModificarUsuario(props) {
    const { match: { params: { id } } } = props
    const parentPath = '/admin/usuarios'
    const classes = useStyles()

    const [tiposUsuario, setTiposUsuario] = useState([])
    const [inputs, setInputs] = useState({})
    const [viejaContrasena, setViejaContrasena] = useState('')
    const [notFound, setNotFound] = useState(false)
    const [isLoadingData, setIsLoadingData] = useState(true)
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae los tipos de usuario y la info del usuario
    useEffect(() => {
        GetByIdApi('usuario/id', id).then(response => {
            if (response.status === 200) {
                response.result.then(usuario => {
                    setInputs(usuario)
                    setViejaContrasena(usuario.contrasena)
                    setIsLoadingData(false)
                })
            } else {
                setNotFound(true)
            }
        })

        GetApi('tiposusuario').then(response => {
            if (response.status === 200) {
                response.result.then(tipos => {
                    setTiposUsuario(tipos)
                })
            }
        })

        return () => setTiposUsuario([])
    }, [id])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    //Función para crear usuario
    const update = () => {
        const data = inputs

        //Cambia la vieja contrasena si no se modificó
        if (data.contrasena.trim() === '') {
            data.contrasena = viejaContrasena
        }

        if (!emailValidation(data.correoElectronico.trim())) {
            setMessage('Ingrese un correo válido.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.nombreUsuario.trim())) {
            setMessage('El nombre de usuario debe tener al menos 3 caracteres.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(8, data.contrasena.trim())) {
            setMessage('La contraseña debe tener al menos 8 dígitos.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Inicia la carga
        setIsLoading(true)

        //Guarda los datos
        PutApi('usuario', id, data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Usuario modificado correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    setTimeout(() => {
                        setNotFound(true)
                    }, 500);
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    //Si no fue encontrado el dato
    if (notFound) {
        return <Redirect to={parentPath} />
    }

    //En lo que carga la información del registro
    if (isLoadingData) {
        return <LinearProgress variant='indeterminate' />
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Typography variant='h5'>Modificar usuario</Typography>
            <div>
                <Typography variant='subtitle1'>
                    Rellene todos los campos para modificar los datos. Rellene el campo
                    de contraseña para cambiarla, si no desea hacerlo, dejarlo vacío.
                </Typography>
                <form onChange={changeForm} className={classes.form}>
                    <Grid container spacing={2}>
                        <Grid item lg={12}>
                            <TextField
                                name='correoElectronico'
                                variant='filled'
                                label='Correo electrónico'
                                defaultValue={inputs.correoElectronico}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='nombreUsuario'
                                variant='filled'
                                label='Nombre de usuario'
                                defaultValue={inputs.nombreUsuario}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='contrasena'
                                type='password'
                                variant='filled'
                                label='Nueva Contraseña (dejar vacío para no cambiar)'
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <FormControl variant="filled" className={classes.formControl}>
                                <InputLabel id="tipoUsuarioLabel">Tipo de usuario</InputLabel>
                                <Select
                                    name='tipoUsuario'
                                    labelId="tipoUsuarioLabel"
                                    onChange={changeForm}
                                    value={inputs.tipoUsuario}>
                                    {
                                        tiposUsuario.map((values, index) => (
                                            <MenuItem key={index} value={values.tipo}>
                                                {values.tipo.toString().toUpperCase()}
                                            </MenuItem>
                                        ))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={6}>
                            <FormControl variant="filled" className={classes.formControl}>
                                <InputLabel id="estadoLabel">Estado</InputLabel>
                                <Select
                                    name='estado'
                                    labelId="estadoLabel"
                                    onChange={changeForm}
                                    value={inputs.estado}>
                                    <MenuItem value={true}>Habilitado</MenuItem>
                                    <MenuItem value={false}>Deshabilitado</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <div>
                {
                    isLoading ?
                        <CircularProgress variant='indeterminate' /> :
                        <div className={classes.buttonContainer}>
                            <Button
                                variant='contained'
                                color='primary'
                                className={classes.button}
                                onClick={update}>
                                Aceptar
                            </Button>
                            <Link to={parentPath} className={classes.link}>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    className={classes.button}>
                                    Cancelar
                                </Button>
                            </Link>
                        </div>
                }
            </div>
        </Fragment>
    )
}

export default ModificarUsuario
