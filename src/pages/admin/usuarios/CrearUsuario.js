import React, { useState, useEffect, Fragment } from 'react'
import {
    Dialog, DialogTitle, DialogContent,
    DialogActions, DialogContentText, TextField,
    Button, Grid, FormControl, InputLabel, Select,
    MenuItem, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { emailValidation, minLengthValidation } from '../../../helpers/Validations'

/**API */
import { GetApi, PostApi } from '../../../api/Services'

function CrearUsuario(props) {
    const classes = useStyles()
    const { open, handleClose, handleReload } = props
    const [tiposUsuario, setTiposUsuario] = useState([])
    const [inputs, setInputs] = useState({
        correoElectronico: '',
        contrasena: '',
        nombreUsuario: '',
        tipoUsuario: 'admin',
        estado: true,
        fechaRegistro: null,
        ultimoAcceso: null
    })
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae los tipos de usuario
    useEffect(() => {
        GetApi('tiposusuario').then(response => {
            if (response.status === 200) {
                response.result.then(tipos => {
                    setTiposUsuario(tipos)
                })
            }
        })

        return () => setTiposUsuario([])
    }, [])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    //Función para crear usuario
    const create = () => {
        const data = inputs

        if (!emailValidation(data.correoElectronico.trim())) {
            setMessage('Ingrese un correo válido.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.nombreUsuario.trim())) {
            setMessage('El nombre de usuario debe tener al menos 3 caracteres.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(8, data.contrasena.trim())) {
            setMessage('La contraseña debe tener al menos 8 dígitos.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Asigna la fecha de registro
        data.fechaRegistro = new Date()

        //Inicia la carga
        setIsLoading(true)

        //Guarda los datos
        PostApi('usuario', data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Usuario añadido correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    //Cierra modal
                    handleClose()
                    handleReload()
                    //Limpia los estados
                    setInputs({
                        ...inputs,
                        correoElectronico: '',
                        contrasena: '',
                        tipoUsuario: 'admin',
                        estado: true,
                        fechaRegistro: null,
                        ultimoAcceso: null
                    })
                    setIsLoading(false)
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Dialog
                open={open}
                onClose={handleClose}>
                <DialogTitle>Crear usuario</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Rellene todos los campos para crear un nuevo usuario.
                    </DialogContentText>
                    <form onChange={changeForm}>
                        <Grid container spacing={2}>
                            <Grid item lg={12}>
                                <TextField
                                    name='correoElectronico'
                                    variant='filled'
                                    label='Correo electrónico'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <TextField
                                    name='contrasena'
                                    type='password'
                                    variant='filled'
                                    label='Contraseña'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <TextField
                                    name='nombreUsuario'
                                    variant='filled'
                                    label='Nombre de usuario'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="tipoUsuarioLabel">Tipo de usuario</InputLabel>
                                    <Select
                                        name='tipoUsuario'
                                        labelId="tipoUsuarioLabel"
                                        onChange={changeForm}
                                        value={inputs.tipoUsuario}>
                                        {
                                            tiposUsuario.map((values, index) => (
                                                <MenuItem key={index} value={values.tipo}>
                                                    {values.tipo.toString().toUpperCase()}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item lg={6}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="estadoLabel">Estado</InputLabel>
                                    <Select
                                        name='estado'
                                        labelId="estadoLabel"
                                        onChange={changeForm}
                                        value={inputs.estado}>
                                        <MenuItem value={true}>Habilitado</MenuItem>
                                        <MenuItem value={false}>Deshabilitado</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                </DialogContent>
                <DialogActions>
                    {
                        isLoading ?
                            <CircularProgress variant='indeterminate' /> :
                            <Fragment>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={create}>
                                    Aceptar
                                </Button>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    onClick={handleClose}>
                                    Cancelar
                                </Button>
                            </Fragment>
                    }
                </DialogActions>
            </Dialog>
        </Fragment>
    )
}

export default CrearUsuario
