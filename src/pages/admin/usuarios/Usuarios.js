import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Typography, LinearProgress, IconButton, Button } from '@material-ui/core'
import { DataGrid, GridToolbar } from '@material-ui/data-grid'
import { useStyles } from '../useStyles'

/**Components */
import Crear from './CrearUsuario'
import Notification from '../../../components/Notification'
import SnackbarAlert from '../../../components/SnackbarAlert'

/**API */
import { GetApi, DeleteApi } from '../../../api/Services'

/**Icons */
import EditIcon from '@material-ui/icons/Edit'
import ClearIcon from '@material-ui/icons/Clear'
import AddIcon from '@material-ui/icons/Add'

function Usuarios() {
    const classes = useStyles()
    const controller = 'usuario'

    const [data, setData] = useState([])
    const [selectedId, setSelectedId] = useState('')
    const [isLoading, setIsLoading] = useState(true)
    const [reload, setReload] = useState(false)
    const [openCreateForm, setOpenCreateForm] = useState(false)
    const [openNotification, setOpenNotification] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Definición de columnas
    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            flex: 1,
            hide: true
        },
        {
            field: 'correoElectronico',
            headerName: 'Correo Electrónico',
            flex: 2
        },
        {
            field: 'nombreUsuario',
            headerName: 'Nombre de usuario',
            flex: 1
        },
        {
            field: 'tipoUsuario',
            headerName: 'Tipo de usuario',
            flex: 1
        },
        {
            field: 'asignadoTipoUsuario',
            headerName: 'Asignado a tipo de usuario',
            flex: 1,
            type: 'boolean'
        },
        {
            field: 'estado',
            headerName: 'Estado',
            flex: 1,
            type: 'boolean'
        },
        {
            field: 'fechaRegistro',
            headerName: 'Fecha de registro',
            flex: 1,
            type: 'date',
            hide: true
        },
        {
            field: 'ultimoAcceso',
            headerName: 'Último acceso',
            flex: 2,
            type: 'datetime',
            hide: true
        },
        {
            field: 'acciones',
            headerName: 'Acciones',
            flex: 1,
            renderCell: params => (
                params.getValue(params.id, 'tipoUsuario') !== 'admin' && (
                    <div className={classes.actionContainer}>
                        <Link to={`/admin/usuarios/${params.getValue(params.id, 'id')}`}>
                            <IconButton
                                className={classes.editButton}
                                size='small'>
                                <EditIcon />
                            </IconButton>
                        </Link>
                        <IconButton
                            className={classes.deleteButton}
                            size='small'
                            onClick={() => {
                                setSelectedId(params.getValue(params.id, 'id'))
                                setOpenNotification(true)
                            }}>
                            <ClearIcon />
                        </IconButton>
                    </div>
                )
            )
        }
    ]

    //Obtiene todos los usuarios
    useEffect(() => {
        GetApi(controller).then(response => {
            if (response.status === 200) {
                response.result.then(rows => {
                    let list = []
                    rows.map((values, index) => {
                        list.push({
                            ...values,
                            fechaRegistro: new Date(values.fechaRegistro),
                            ultimoAcceso: values.ultimoAcceso ? new Date(values.ultimoAcceso) : 'Sin acceso reciente'
                        })
                        return null
                    })
                    setData(list)
                    setIsLoading(false)
                    setReload(false)
                })
            }
        })

        return () => setData([])
    }, [reload])

    //Elimina registro
    const eliminar = (id) => {
        //Cierra notificación
        setOpenNotification(false)

        //Elimina el usuario
        DeleteApi(controller, id).then(response => {
            switch (response.status) {
                case 200:
                    response.result.then(text => {
                        setMessage(text)
                        setSeverity('success')
                        setOpenAlert(true)
                    })
                    break;

                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;

                default:
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })

        //Recarga la tabla
        setReload(true)
    }

    return (
        <div>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Crear
                open={openCreateForm}
                handleClose={() => setOpenCreateForm(false)}
                handleReload={() => setReload(true)} />
            <Notification
                title='Eliminar usuario'
                open={openNotification}
                onClose={() => setOpenNotification(false)}
                onAccept={() => eliminar(selectedId)}>
                <Typography>
                    ¿Está seguro de eliminar este usuario con el ID <span style={{ fontWeight: 'bold' }}>{selectedId}</span>?
                    Esta acción no se puede deshacer.
                </Typography>
                <br />
                <Typography>
                    NOTA: Algunos usuarios no pueden ser eliminados ya que están
                    relacionados con algún consultor o cliente, si desea deshabilitarlos,
                    cambie el valor de estado en el panel de modificación.
                </Typography>
            </Notification>
            <Typography variant='h4' gutterBottom>Usuarios</Typography>
            <Typography>
                Se muestra una lista con todos los usuarios registrados en el sistema,
                solo los administradores pueden manipular estos datos.
            </Typography>
            <Button
                variant='contained'
                color='primary'
                startIcon={<AddIcon />}
                className={classes.createBtn}
                onClick={() => setOpenCreateForm(true)}>
                Crear Usuario
            </Button>
            <div style={{ height: '500px' }}>
                <DataGrid
                    columns={columns}
                    rows={data}
                    disableSelectionOnClick
                    loading={isLoading}
                    pageSize={25}
                    components={{
                        LoadingOverlay: LinearProgress,
                        Toolbar: GridToolbar
                    }} />
            </div>
        </div>
    )
}

export default Usuarios
