import React, { useState, useEffect, Fragment } from 'react'
import { Redirect, Link } from 'react-router-dom'
import {
    Button, Grid, FormControl, InputLabel, Select,
    MenuItem, CircularProgress, Typography, LinearProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**API */
import { GetApi, GetByIdApi, PutApi } from '../../../api/Services'

function ModificarAcceso(props) {
    const { match: { params: { id } } } = props
    const parentPath = '/admin/accesos'
    const classes = useStyles()

    const [tiposUsuario, setTiposUsuario] = useState([])
    const [funciones, setFunciones] = useState([])
    const [inputs, setInputs] = useState({})
    const [notFound, setNotFound] = useState(false)
    const [isLoadingData, setIsLoadingData] = useState(true)
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')
    const [idUsuario, setIdUsuario] = useState(null)
    const session_id = window.localStorage.getItem('SESSION_ID')

    //Efecto que trae los tipos de usuario y la info del usuario
    useEffect(() => {
        GetByIdApi('acceso', id).then(response => {
            if (response.status === 200) {
                response.result.then(acceso => {
                    setInputs(acceso)
                    setIsLoadingData(false)
                })
            } else {
                setNotFound(true)
            }
        })

        GetApi('tiposusuario').then(response => {
            if (response.status === 200) {
                response.result.then(tipos => {
                    setTiposUsuario(tipos)
                })
            }
        })

        GetApi('funcion').then(response => {
            if (response.status === 200) {
                response.result.then(func => {
                    setFunciones(func)
                })
            }
        })

        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setIdUsuario(ses.idUsuario)
                })
            }
        })

        return () => {
            setTiposUsuario([])
            setFunciones([])
            setIdUsuario(null)
        }
    }, [id, session_id])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    //Función para crear usuario
    const update = () => {
        const data = inputs

        if (!minLengthValidation(1, data.idFuncion)) {
            setMessage('Seleccione una función.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Inicia la carga
        setIsLoading(true)

        //Modificado por
        data.modificadoPor = idUsuario

        //Guarda los datos
        PutApi('acceso', id, data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Acceso modificado correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    setTimeout(() => {
                        setNotFound(true)
                    }, 500);
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    //Si no fue encontrado el dato
    if (notFound) {
        return <Redirect to={parentPath} />
    }

    //En lo que carga la información del registro
    if (isLoadingData) {
        return <LinearProgress variant='indeterminate' />
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Typography variant='h5'>Modificar acceso</Typography>
            <div>
                <Typography variant='subtitle1'>
                    Rellene todos los campos para modificar los datos.
                </Typography>
                <form onChange={changeForm} className={classes.form}>
                    <Grid container spacing={2}>
                        <Grid item lg={6}>
                            <FormControl variant="filled" className={classes.formControl}>
                                <InputLabel id="funcionesLabel">Función</InputLabel>
                                <Select
                                    disabled
                                    name='idFuncion'
                                    labelId="funcionesLabel"
                                    onChange={changeForm}
                                    value={inputs.idFuncion}>
                                    {
                                        funciones.map((values, index) => (
                                            <MenuItem key={index} value={values.id}>
                                                {`${values.accion}${values.nombreModulo}`}
                                            </MenuItem>
                                        ))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={6}>
                            <FormControl variant="filled" className={classes.formControl}>
                                <InputLabel id="tipoUsuarioLabel">Tipo de usuario</InputLabel>
                                <Select
                                    disabled
                                    name='idTipoUsuario'
                                    labelId="tipoUsuarioLabel"
                                    onChange={changeForm}
                                    value={inputs.idTipoUsuario}>
                                    {
                                        tiposUsuario.map((values, index) => (
                                            <MenuItem key={index} value={values.tipo}>
                                                {values.tipo.toString().toUpperCase()}
                                            </MenuItem>
                                        ))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={6}>
                            <FormControl variant="filled" className={classes.formControl}>
                                <InputLabel id="estadoLabel">Estado</InputLabel>
                                <Select
                                    name='estado'
                                    labelId="estadoLabel"
                                    onChange={changeForm}
                                    value={inputs.estado}>
                                    <MenuItem value={true}>Habilitado</MenuItem>
                                    <MenuItem value={false}>Deshabilitado</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <div>
                {
                    isLoading ?
                        <CircularProgress variant='indeterminate' /> :
                        <div className={classes.buttonContainer}>
                            <Button
                                variant='contained'
                                color='primary'
                                className={classes.button}
                                onClick={update}>
                                Aceptar
                            </Button>
                            <Link to={parentPath} className={classes.link}>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    className={classes.button}>
                                    Cancelar
                                </Button>
                            </Link>
                        </div>
                }
            </div>
        </Fragment>
    )
}

export default ModificarAcceso
