import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Typography, LinearProgress, IconButton, Button } from '@material-ui/core'
import { DataGrid, GridToolbar } from '@material-ui/data-grid'
import { useStyles } from '../useStyles'

/**Components */
import Crear from './CrearAcceso'
import Notification from '../../../components/Notification'
import SnackbarAlert from '../../../components/SnackbarAlert'

/**API */
import { GetApi, DeleteApi } from '../../../api/Services'

/**Icons */
import EditIcon from '@material-ui/icons/Edit'
import ClearIcon from '@material-ui/icons/Clear'
import AddIcon from '@material-ui/icons/Add'

function Accesos() {
    const classes = useStyles()
    const controller = 'acceso'

    const [data, setData] = useState([])
    const [selectedId, setSelectedId] = useState('')
    const [isLoading, setIsLoading] = useState(true)
    const [reload, setReload] = useState(false)
    const [openCreateForm, setOpenCreateForm] = useState(false)
    const [openNotification, setOpenNotification] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')
    const [funciones, setFunciones] = useState([])

    //Definición de columnas
    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            flex: 1
        },
        {
            field: 'idFuncion',
            headerName: 'ID Función',
            flex: 1,
            hide: true
        },
        {
            field: 'funcion',
            headerName: 'Función',
            flex: 1,
            renderCell: params => funciones.map((values, index) => {
                if (values.id === params.getValue(params.id, 'idFuncion'))
                    return `${values.accion}${values.nombreModulo}`
                else
                    return ''
            })
        },
        {
            field: 'idTipoUsuario',
            headerName: 'ID Tipo de usuario',
            flex: 1
        },
        {
            field: 'estado',
            headerName: 'Estado',
            type: 'boolean',
            flex: 1
        },
        {
            field: 'modificadoPor',
            headerName: 'Modificado por',
            flex: 1,
            hide: true
        },
        {
            field: 'ultimaModificacion',
            headerName: 'Última modificación',
            type: 'datetime',
            flex: 1,
            hide: true
        },
        {
            field: 'acciones',
            headerName: 'Acciones',
            flex: 1,
            renderCell: params => (
                <div className={classes.actionContainer}>
                    <Link to={`/admin/accesos/${params.getValue(params.id, 'id')}`}>
                        <IconButton
                            className={classes.editButton}
                            size='small'>
                            <EditIcon />
                        </IconButton>
                    </Link>
                    <IconButton
                        className={classes.deleteButton}
                        size='small'
                        onClick={() => {
                            setSelectedId(params.getValue(params.id, 'id'))
                            setOpenNotification(true)
                        }}>
                        <ClearIcon />
                    </IconButton>
                </div>
            )
        }
    ]

    //Obtiene todos los usuarios
    useEffect(() => {
        GetApi(controller).then(response => {
            if (response.status === 200) {
                response.result.then(rows => {
                    let list = []
                    rows.map((values, index) => {
                        list.push({
                            ...values,
                            ultimaModificacion: new Date(values.ultimaModificacion)
                        })
                        return null
                    })
                    setData(list)
                    setIsLoading(false)
                    setReload(false)
                })
            }
        })

        GetApi('funcion').then(response => {
            if (response.status === 200) {
                response.result.then(funcion => {
                    setFunciones(funcion)
                })
            } else {
                setFunciones([])
            }
        })

        return () => {
            setData([])
            setFunciones([])
        }
    }, [reload])

    //Elimina registro
    const eliminar = (id) => {
        //Cierra notificación
        setOpenNotification(false)

        //Elimina el usuario
        DeleteApi(controller, id).then(response => {
            switch (response.status) {
                case 200:
                    response.result.then(text => {
                        setMessage(text)
                        setSeverity('success')
                        setOpenAlert(true)
                    })
                    break;

                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;

                default:
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })

        //Recarga la tabla
        setReload(true)
    }

    return (
        <div>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Crear
                open={openCreateForm}
                handleClose={() => setOpenCreateForm(false)}
                handleReload={() => setReload(true)} />
            <Notification
                title='Eliminar usuario'
                open={openNotification}
                onClose={() => setOpenNotification(false)}
                onAccept={() => eliminar(selectedId)}>
                <Typography>
                    ¿Está seguro de eliminar este consultor con el ID <span style={{ fontWeight: 'bold' }}>{selectedId}</span>?
                    Esta acción no se puede deshacer.
                </Typography>
                <br />
                <Typography>
                    NOTA: Algunos consultores no pueden ser eliminados ya que están
                    relacionados con algún proyecto, si desea deshabilitarlos,
                    cambie el valor de estado en el panel de modificación de usuarios.
                </Typography>
            </Notification>
            <Typography variant='h4' gutterBottom>Accesos</Typography>
            <Typography>
                Registro de asignación de funciones del sistema a los diferentes tipos de usuario para limitar su vista y ejecución.
            </Typography>
            <Button
                variant='contained'
                color='primary'
                startIcon={<AddIcon />}
                className={classes.createBtn}
                onClick={() => setOpenCreateForm(true)}>
                Crear Acceso
            </Button>
            <div style={{ height: '500px' }}>
                <DataGrid
                    columns={columns}
                    rows={data}
                    disableSelectionOnClick
                    loading={isLoading}
                    pageSize={25}
                    components={{
                        LoadingOverlay: LinearProgress,
                        Toolbar: GridToolbar
                    }} />
            </div>
        </div>
    )
}

export default Accesos
