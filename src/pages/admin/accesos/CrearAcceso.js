import React, { useState, useEffect, Fragment } from 'react'
import {
    Dialog, DialogTitle, DialogContent,
    DialogActions, DialogContentText,
    Button, Grid, FormControl, InputLabel, Select,
    MenuItem, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**API */
import { GetByIdApi, GetApi, PostApi } from '../../../api/Services'

function CrearAcceso(props) {
    const classes = useStyles()
    const { open, handleClose, handleReload } = props
    const [funciones, setFunciones] = useState([])
    const [tiposUsuario, setTiposUsuario] = useState([])
    const [inputs, setInputs] = useState({
        idFuncion: '',
        idTipoUsuario: 'admin',
        estado: true,
        modificadoPor: '',
        ultimaModificacion: null
    })
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')
    const [idUsuario, setIdUsuario] = useState(null)
    const session_id = window.localStorage.getItem('SESSION_ID')

    //Efecto que trae los tipos de usuario y funciones
    useEffect(() => {
        GetApi('tiposusuario').then(response => {
            if (response.status === 200) {
                response.result.then(tipos => {
                    setTiposUsuario(tipos)
                })
            }
        })

        GetApi('funcion').then(response => {
            if (response.status === 200) {
                response.result.then(f => {
                    setFunciones(f)
                })
            }
        })

        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setIdUsuario(ses.idUsuario)
                })
            }
        })

        return () => {
            setTiposUsuario([])
            setFunciones([])
            setIdUsuario(null)
        }
    }, [session_id])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    //Función para crear usuario
    const create = () => {
        const data = inputs

        if (!minLengthValidation(1, data.idFuncion)) {
            setMessage('Seleccione una función.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Inicia la carga
        setIsLoading(true)

        //Modificado por
        data.modificadoPor = idUsuario

        //Guarda los datos
        PostApi('acceso', data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Acceso concedido correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    //Cierra modal
                    handleClose()
                    handleReload()
                    //Limpia los estados
                    setInputs({
                        ...inputs,
                        modificadoPor: idUsuario,
                        estado: true
                    })
                    setIsLoading(false)
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Dialog
                open={open}
                onClose={handleClose}>
                <DialogTitle>Crear acceso</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Rellene todos los campos para dar acceso a un tipo de usuario.
                    </DialogContentText>
                    <form onChange={changeForm}>
                        <Grid container spacing={2}>
                            <Grid item lg={6}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="funcionesLabel">Función</InputLabel>
                                    <Select
                                        name='idFuncion'
                                        labelId="funcionesLabel"
                                        onChange={changeForm}
                                        value={inputs.idFuncion}>
                                        {
                                            funciones.map((values, index) => (
                                                <MenuItem key={index} value={values.id}>
                                                    {`${values.accion}${values.nombreModulo}`}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item lg={6}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="tipoUsuarioLabel">Tipo de usuario</InputLabel>
                                    <Select
                                        name='idTipoUsuario'
                                        labelId="tipoUsuarioLabel"
                                        onChange={changeForm}
                                        value={inputs.idTipoUsuario}>
                                        {
                                            tiposUsuario.map((values, index) => (
                                                <MenuItem key={index} value={values.tipo}>
                                                    {values.tipo.toString().toUpperCase()}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item lg={6}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="estadoLabel">Estado</InputLabel>
                                    <Select
                                        name='estado'
                                        labelId="estadoLabel"
                                        onChange={changeForm}
                                        value={inputs.estado}>
                                        <MenuItem value={true}>Habilitado</MenuItem>
                                        <MenuItem value={false}>Deshabilitado</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                </DialogContent>
                <DialogActions>
                    {
                        isLoading ?
                            <CircularProgress variant='indeterminate' /> :
                            <Fragment>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={create}>
                                    Aceptar
                                </Button>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    onClick={handleClose}>
                                    Cancelar
                                </Button>
                            </Fragment>
                    }
                </DialogActions>
            </Dialog>
        </Fragment>
    )
}

export default CrearAcceso
