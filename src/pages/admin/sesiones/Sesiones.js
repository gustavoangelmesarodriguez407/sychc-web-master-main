import React, { useState, useEffect } from 'react'
//import { Link } from 'react-router-dom'
import { Typography, LinearProgress, IconButton } from '@material-ui/core'
import { DataGrid, GridToolbar } from '@material-ui/data-grid'
import { useStyles } from '../useStyles'

/**Components */
import Crear from './CrearAcceso'
import Notification from '../../../components/Notification'
import SnackbarAlert from '../../../components/SnackbarAlert'

/**API */
import { GetApi, DeleteApi } from '../../../api/Services'

/**Icons */
//import EditIcon from '@material-ui/icons/Edit'
import ClearIcon from '@material-ui/icons/Clear'
//import AddIcon from '@material-ui/icons/Add'

function Sesiones() {
    const classes = useStyles()
    const controller = 'sesion'

    const [data, setData] = useState([])
    const [selectedId, setSelectedId] = useState('')
    const [isLoading, setIsLoading] = useState(true)
    const [reload, setReload] = useState(false)
    const [openCreateForm, setOpenCreateForm] = useState(false)
    const [openNotification, setOpenNotification] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Definición de columnas
    const columns = [
        {
            field: 'id',
            headerName: 'Clave (ID)',
            flex: 1
        },
        {
            field: 'idUsuario',
            headerName: 'ID Usuario',
            flex: 1
        },
        {
            field: 'direccionIP',
            headerName: 'Dirección IP',
            flex: 1
        },
        {
            field: 'fecha',
            headerName: 'Fecha',
            type: 'datetime',
            flex: 1
        },
        {
            field: 'acciones',
            headerName: 'Acciones',
            flex: 1,
            renderCell: params => (
                <div className={classes.actionContainer}>
                    <IconButton
                        className={classes.deleteButton}
                        size='small'
                        onClick={() => {
                            setSelectedId(params.getValue(params.id, 'id'))
                            setOpenNotification(true)
                        }}>
                        <ClearIcon />
                    </IconButton>
                </div>
            )
        }
    ]

    //Obtiene todos los usuarios
    useEffect(() => {
        GetApi(controller).then(response => {
            if (response.status === 200) {
                response.result.then(rows => {
                    let list = []
                    rows.map((values, index) => {
                        list.push({
                            ...values,
                            id: values.clave,
                            fecha: new Date(values.fecha)
                        })
                        return null
                    })
                    setData(list)
                    setIsLoading(false)
                    setReload(false)
                })
            }
        })

        return () => setData([])
    }, [reload])

    //Elimina registro
    const eliminar = (id) => {
        //Cierra notificación
        setOpenNotification(false)

        //Elimina el usuario
        DeleteApi(controller, id).then(response => {
            switch (response.status) {
                case 200:
                    response.result.then(text => {
                        setMessage(text)
                        setSeverity('success')
                        setOpenAlert(true)
                    })
                    break;

                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;

                default:
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })

        //Recarga la tabla
        setReload(true)
    }

    return (
        <div>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Crear
                open={openCreateForm}
                handleClose={() => setOpenCreateForm(false)}
                handleReload={() => setReload(true)} />
            <Notification
                title='Eliminar usuario'
                open={openNotification}
                onClose={() => setOpenNotification(false)}
                onAccept={() => eliminar(selectedId)}>
                <Typography>
                    ¿Está seguro de cerrar la sesión con el ID <span style={{ fontWeight: 'bold' }}>{selectedId}</span>?
                    Esta acción no se puede deshacer.
                </Typography>
            </Notification>
            <Typography variant='h4' gutterBottom>Sesiones</Typography>
            <Typography>
                Registro de las sesiones iniciadas y en ejecución de todos los usuarios,
                la sesión se dispone por usuario, su dirección IP (si es que permitió el rastreo)
                y la fecha y hora.
            </Typography>
            <div style={{ marginTop: 20, height: '500px' }}>
                <DataGrid
                    columns={columns}
                    rows={data}
                    disableSelectionOnClick
                    loading={isLoading}
                    pageSize={25}
                    components={{
                        LoadingOverlay: LinearProgress,
                        Toolbar: GridToolbar
                    }} />
            </div>
        </div>
    )
}

export default Sesiones
