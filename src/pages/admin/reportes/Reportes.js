import React, { useState } from 'react'
import {
    Typography, FormControl,
    InputLabel, Select, MenuItem
} from '@material-ui/core'
import { useStyles } from '../useStyles'


/**Compnents */
import ReportesProyecto from './ReportesProyecto'
import ReportesConsultor from './ReportesConsultor'
import ReportesCliente from './ReportesCliente'

function Reportes() {
    const classes = useStyles()
    const [moduloSeleccionado, setModuloSeleccionado] = useState('proyecto')

    const Modulo = () => {
        switch (moduloSeleccionado) {
            case 'proyecto':
                return <ReportesProyecto />
            case 'consultor':
                return <ReportesConsultor />
            case 'cliente':
                return <ReportesCliente />
            default:
                return <ReportesProyecto />
        }
    }

    return (
        <div>
            <Typography variant='h4' gutterBottom>
                Reportes
            </Typography>
            <Typography>
                Modifique los filtros para obtener un reporte específico,
                al seleccionar un módulo, más opciones se irán desplegando.
                Dejar vacíos los filtros para omitirlos.
            </Typography>
            <FormControl
                size='small'
                variant='filled'
                className={classes.formControl}
                style={{ marginTop: 16 }}>
                <InputLabel id='moduloLabel'>Módulo</InputLabel>
                <Select
                    labelId='moduloLabel'
                    onChange={(e) => setModuloSeleccionado(e.target.value)}
                    value={moduloSeleccionado}>
                    <MenuItem value='proyecto'>Proyecto</MenuItem>
                    <MenuItem value='consultor'>Consultor</MenuItem>
                    <MenuItem value='cliente'>Cliente</MenuItem>
                </Select>
            </FormControl>
            <Modulo />
        </div>
    )
}

export default Reportes
