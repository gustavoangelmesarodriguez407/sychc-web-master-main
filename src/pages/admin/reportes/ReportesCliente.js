import React, { useState, useEffect, Fragment } from 'react'
import {
    FormControl, InputLabel, Select, MenuItem,
    Typography, Grid, Button, Divider, TextField, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'
import ReportePlantillaCliente from '../../../components/plantillas_reportes/ReportePlantillaCliente'

/**APIs */
import { GetApi } from '../../../api/Services'

/**Icons */
import SearchIcon from '@material-ui/icons/Search'
import FilterListIcon from '@material-ui/icons/FilterList'

function ReportesCliente() {
    const classes = useStyles()
    const [filtros, setFiltros] = useState({
        clienteSeleccionado: '',
        proyectoSeleccionado: '',
        consultorSeleccionado: '',
        fecha1: '',
        fecha2: ''
    })
    const [clientes, setClientes] = useState([])
    const [proyectosCliente, setProyectosCliente] = useState([])
    const [consultoresProyecto, setConsultoresProyecto] = useState([])

    //Resultados de búsqueda
    const [resultadosReporte, setResultadosReporte] = useState(null)
    const [reporte, setReporte] = useState({
        cliente: '',
        proyecto: '',
        consultor: '',
        periodo: '',
        listaProyectos: [],
        totalHoras: 0,
        totalHorasFacturables: 0,
        totalHorasNoFacturables: 0,
        clienteArray: []
    })

    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae los clientes
    useEffect(() => {
        GetApi('cliente').then(response => {
            if (response.status === 200) {
                response.result.then(datos => {
                    setClientes(datos)
                })
            } else {
                response.result.then(msg => {
                    setMessage(msg)
                    setSeverity('error')
                    setOpenAlert(true)
                })
            }
        })
        return () => {
            setClientes([])
        }
    }, [])

    //Efecto que trae los proyectos del cliente seleccionado
    useEffect(() => {
        if (filtros.clienteSeleccionado.trim().length > 0) {
            GetApi(`proyecto/cliente/${filtros.clienteSeleccionado}`)
                .then(response => {
                    if (response.status === 200) {
                        response.result.then(prt => {
                            setProyectosCliente(prt)
                        })
                    } else {
                        response.result.then(msg => {
                            setMessage(msg)
                            setSeverity('error')
                            setOpenAlert(true)
                        })
                    }
                })
        }

        return () => {
            setProyectosCliente([])
        }
    }, [filtros.clienteSeleccionado])

    //Efecto que trae los consultores del proyecto
    useEffect(() => {
        if (filtros.proyectoSeleccionado.trim().length > 0) {
            //Trae los consultores del proyecto
            GetApi(`consultores_proyecto/lista-consultores/${filtros.proyectoSeleccionado}`).then(response => {
                if (response.status === 200) {
                    response.result.then(cop => {
                        setConsultoresProyecto(cop)
                    })
                }
            })
        }

        return () => {
            setConsultoresProyecto([])
        }
    }, [filtros.proyectoSeleccionado])

    //Función que busca el reporte
    const buscarReporte = () => {
        const data = filtros
        const controller = `reporte/cliente?idCliente=${data.clienteSeleccionado}&idProyecto=${data.proyectoSeleccionado}&idConsultor=${data.consultorSeleccionado}&fecha1=${data.fecha1}&fecha2=${data.fecha2}`

        //Inicia la carga
        setIsLoading(true)

        //Trae los datos del reporte
        GetApi(controller).then(response => {
            if (response.status === 200) {
                response.result.then(rp => {
                    //Valida si encontró resultados
                    if (rp.length > 0)
                        setResultadosReporte(true)
                    else {
                        setResultadosReporte(false)
                        return
                    }

                    //Genera el reporte de horas
                    generarReporte(rp)
                })
            } else {
                response.result.then(msg => {
                    setMessage(msg)
                    setSeverity('error')
                    setOpenAlert(true)
                })
            }
        })

        //Para la carga
        setIsLoading(false)
    }

    //Función que genera el reporte
    const generarReporte = (resultados) => {
        let cliente = resultados[0].nombreCliente
        let proyecto = filtros.proyectoSeleccionado === '' ? 'Todos los proyectos' : resultados[0].nombreProyecto
        let consultor = filtros.consultorSeleccionado === '' ? 'Todos los consultores' : resultados[0].nombreConsultor
        let periodo =
            filtros.fecha1 === '' && filtros.fecha2 === '' ?
                'Mostrando resumen de trabajo' :
                filtros.fecha1 === filtros.fecha2 ?
                    `Mostrando resumen de trabajo del ${filtros.fecha1}.`
                    : `Mostrando resumen de trabajo del ${filtros.fecha1} al ${filtros.fecha2}.`

        let totalHoras = 0
        let totalHorasFacturables = 0
        let totalHorasNoFacturables = 0
        const totalesPorCliente = {};
        //Suma de las horas
        resultados.forEach(r => {
            //Suma las horas
            totalHoras += r.horasTrabajadas

            //suma horas factuables y no facturables
            if (r.horasFacturables)
                totalHorasFacturables += r.horasTrabajadas
            else
                totalHorasNoFacturables += r.horasTrabajadas
            const claveUnica = `${r.nombreProyecto}-${r.nombreConsultor}`;

            // Inicializa el objeto para la clave única si no existe
            if (!totalesPorCliente[claveUnica]) {
                totalesPorCliente[claveUnica] = {
                    nombreProyecto: r.nombreProyecto,
                    nombreConsultor: r.nombreConsultor,
                    totalHoras: 0,
                    totalHorasFacturables: 0,
                    totalHorasNoFacturables: 0
                };
            }

            // Suma las horas trabajadas totales
            totalesPorCliente[claveUnica].totalHoras += r.horasTrabajadas;

            // Suma horas facturables y no facturables
            if (r.horasFacturables) {
                totalesPorCliente[claveUnica].totalHorasFacturables += r.horasTrabajadas;
            } else {
                totalesPorCliente[claveUnica].totalHorasNoFacturables += r.horasTrabajadas;
            }
        });

        // Transforma el objeto 'totalesPorCliente' en un array
        const mappedData = Object.values(totalesPorCliente);

        // Establece los clientes del consultor con los totales calculados
        //setClientesConsultor(mappedData);

        setReporte({
            ...reporte,
            cliente: cliente,
            proyecto: proyecto,
            consultor: consultor,
            periodo: periodo,
            listaProyectos: proyectosCliente,
            totalHoras: totalHoras,
            totalHorasFacturables: totalHorasFacturables,
            totalHorasNoFacturables: totalHorasNoFacturables,
            clienteArray: mappedData
        })

        //Alerta de información
        setMessage('Reporte generado correctamente.')
        setSeverity('success')
        setOpenAlert(true)
    }

    //Función que limpia los filtros
    const limpiarFiltros = () => {
        setFiltros({
            ...filtros,
            clienteSeleccionado: '',
            proyectoSeleccionado: '',
            consultorSeleccionado: '',
            fecha1: '',
            fecha2: ''
        })
    }

    return (
        <div>
            <SnackbarAlert open={openAlert} onClose={() => setOpenAlert(false)} severity={severity}>
                {message}
            </SnackbarAlert>
            <div className={classes.filtrosContainer}>
                <Grid container spacing={2}>
                    <Grid item xl={6} lg={6} md={6} sm={6} xs={12}>
                        <FormControl size='small' variant='filled' className={classes.formControl}>
                            <InputLabel id='clienteLabel'>Cliente</InputLabel>
                            <Select
                                labelId='clienteLabel'
                                value={filtros.clienteSeleccionado}
                                onChange={(e) => setFiltros({ ...filtros, clienteSeleccionado: e.target.value })}>
                                {
                                    clientes.map((value, index) => (
                                        <MenuItem value={value.id} key={index}>
                                            {value.nombre}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <FormControl size='small' variant='filled' className={classes.formControl}>
                            <InputLabel id='proyectoLabel'>Proyecto</InputLabel>
                            <Select
                                labelId='proyectoLabel'
                                value={filtros.proyectoSeleccionado}
                                onChange={(e) => setFiltros({ ...filtros, proyectoSeleccionado: e.target.value })}>
                                <MenuItem value=''>Todos los proyectos</MenuItem>
                                <Divider />
                                {
                                    proyectosCliente.map((value, index) => (
                                        <MenuItem value={value.id} key={index}>
                                            {value.nombre}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <FormControl size='small' variant='filled' className={classes.formControl}>
                            <InputLabel id='consultorLabel'>Consultor</InputLabel>
                            <Select
                                labelId='consultorLabel'
                                value={filtros.consultorSeleccionado}
                                onChange={(e) => setFiltros({ ...filtros, consultorSeleccionado: e.target.value })}>
                                <MenuItem value=''>Todos los consultores</MenuItem>
                                <Divider />
                                {
                                    consultoresProyecto.map((value, index) => (
                                        <MenuItem value={value.idConsultor} key={index}>
                                            {`${value.nombresConsultor} ${value.apellidosConsultor}`}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <Typography variant='caption'>Primer periodo de actividad</Typography>
                        <TextField
                            type='date'
                            size='small'
                            value={filtros.fecha1}
                            onChange={(e) => setFiltros({ ...filtros, fecha1: e.target.value })}
                            className={classes.formControl} />
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <Typography variant='caption'>Segundo periodo de actividad</Typography>
                        <TextField
                            type='date'
                            size='small'
                            value={filtros.fecha2}
                            onChange={(e) => setFiltros({ ...filtros, fecha2: e.target.value })}
                            className={classes.formControl} />
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        {
                            isLoading ?
                                <CircularProgress variant='indeterminate' /> :
                                <Button
                                    variant='contained'
                                    color='primary'
                                    startIcon={<SearchIcon />}
                                    fullWidth
                                    onClick={buscarReporte}>
                                    Buscar
                                </Button>
                        }
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <Button
                            variant='contained'
                            color='secondary'
                            startIcon={<FilterListIcon />}
                            fullWidth
                            onClick={limpiarFiltros}>
                            Limpiar filtros
                        </Button>
                    </Grid>
                </Grid>
            </div>
            {
                resultadosReporte != null && (
                    resultadosReporte ?
                        (
                            <Fragment>
                                <Divider className={classes.divider} />
                                <ReportePlantillaCliente datos={reporte} />
                            </Fragment>
                        ) :
                        (
                            <div>
                                <Typography variant='h5'>
                                    Sin resultados
                                </Typography>
                                <Typography>
                                    No se encontraron actividades que coincidan con los filtros ingresados.
                                </Typography>
                            </div>
                        )
                )
            }
        </div>
    )
}

export default ReportesCliente
