/*eslint array-callback-return: 0 */
/*eslint no-loop-func: 0*/
/*eslint-env es6*/

import React, { useState, useEffect, Fragment } from 'react'
import {
    FormControl, InputLabel, Select, MenuItem,
    Typography, Grid, Button, Divider, TextField, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'
import ReportePlantillaProyecto from '../../../components/plantillas_reportes/ReportePlantillaProyecto'

/**APIs */
import { GetApi } from '../../../api/Services'

/**Icons */
import SearchIcon from '@material-ui/icons/Search'
import FilterListIcon from '@material-ui/icons/FilterList'

function ReportesProyecto(props) {
    const classes = useStyles()

    //Estados de proyecto
    const [filtrosProyecto, setFiltrosProyecto] = useState({
        clienteSeleccionado: '',
        proyectoSeleccionado: '',
        etapaSeleccionada: '',
        consultorSeleccionado: '',
        fecha1: '',
        fecha2: ''
    })
    const [clientes, setClientes] = useState([])
    const [proyectos, setProyectos] = useState([])
    const [etapasProyecto, setEtapasProyecto] = useState([])
    const [consultoresProyecto, setConsultoresProyecto] = useState([])
    const [reporteDeProyecto, setReporteDeProyecto] = useState({
        proyecto: '',
        consultor: '',
        etapa: '',
        cliente: '',
        periodo: '',
        actividades: [],
        tarifas: [],
        totalHoras: 0,
        totalHorasFacturables: 0,
        totalHorasNoFacturables: 0,
        subtotal: 0
    })

    //Resultados de búsqueda
    const [resultadosReporte, setResultadosReporte] = useState(null)

    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae los clientes y proyectos
    useEffect(() => {
        GetApi('cliente').then(response => {
            if (response.status === 200) {
                response.result.then(datos => {
                    setClientes(datos)
                })
            } else {
                setMessage('No hay clientes.')
                setSeverity('error')
                setOpenAlert(true)
            }
        })

        return () => {
            setClientes([])
        }
    }, [])

    //Efecto que trae los proyectos del cliente seleccionado
    useEffect(() => {
        if (filtrosProyecto.clienteSeleccionado.trim().length > 0) {
            GetApi(`proyecto/cliente/${filtrosProyecto.clienteSeleccionado}`)
                .then(response => {
                    if (response.status === 200) {
                        response.result.then(prt => {
                            setProyectos(prt)
                        })
                    } else {
                        response.result.then(msg => {
                            setMessage(msg)
                            setSeverity('error')
                            setOpenAlert(true)
                        })
                    }
                })
        }

        return () => {
            setProyectos([])
        }
    }, [filtrosProyecto.clienteSeleccionado])

    //Efecto que trae las etapas del proyecto seleccionado
    useEffect(() => {
        if (filtrosProyecto.proyectoSeleccionado.trim().length > 0) {
            //Trae las etapas del proyecto
            GetApi(`etapas_proyecto/lista-etapas/${filtrosProyecto.proyectoSeleccionado}`).then(response => {
                if (response.status === 200) {
                    response.result.then(etpy => {
                        setEtapasProyecto(etpy)
                    })
                }
            })

            //Trae los consultores del proyecto
            GetApi(`consultores_proyecto/lista-consultores/${filtrosProyecto.proyectoSeleccionado}`).then(response => {
                if (response.status === 200) {
                    response.result.then(cop => {
                        setConsultoresProyecto(cop)
                    })
                }
            })
        }

        return () => {
            setEtapasProyecto([])
            setConsultoresProyecto([])
        }
    }, [filtrosProyecto.proyectoSeleccionado])

    //Reporte de proyecto
    const buscarReporte = () => {
        const data = filtrosProyecto
        const controller = `reporte/proyecto?idCliente=${data.clienteSeleccionado}&idProyecto=${data.proyectoSeleccionado}&idEtapa=${data.etapaSeleccionada}&idConsultor=${data.consultorSeleccionado}&fecha1=${data.fecha1}&fecha2=${data.fecha2}`

        //Inicia la carga
        setIsLoading(true)

        //Trae los datos del reporte
        GetApi(controller).then(response => {
            if (response.status === 200) {
                response.result.then(rp => {
                    //Valida si encontró resultados
                    if (rp.length > 0)
                        setResultadosReporte(true)
                    else {
                        setResultadosReporte(false)
                        return
                    }

                    //Genera el reporte de horas
                    generarReporte(rp)
                })
            } else {
                response.result.then(msg => {
                    setMessage(msg)
                    setSeverity('error')
                    setOpenAlert(true)
                })
            }
        })

        //Para la carga
        setIsLoading(false)
    }

    //Función que genera el reporte de horas
    const generarReporte = (resultados) => {
        //Variables de reporte
        let proyecto = resultados[0].nombreProyecto
        let cliente = resultados[0].nombreCliente
        let consultor = filtrosProyecto.consultorSeleccionado === '' ? 'Todos los consultores' : resultados[0].nombreConsultor
        let etapa = filtrosProyecto.etapaSeleccionada === '' ? 'Todas las etapas' : resultados[0].nombreEtapa
        let periodo =
            filtrosProyecto.fecha1 === '' && filtrosProyecto.fecha2 === '' ?
                'Mostrando todas las actividades' :
                filtrosProyecto.fecha1 === filtrosProyecto.fecha2 ?
                    `Mostrando actividades del ${filtrosProyecto.fecha1}.`
                    : `Mostrando actividades del ${filtrosProyecto.fecha1} al ${filtrosProyecto.fecha2}.`
        let totalHoras = 0
        let totalHorasFacturables = 0
        let totalHorasNoFacturables = 0

        //Variables de tarifas
        let tarifas = []
        let subtotal = 0

        //Suma de las horas
        resultados.forEach(r => {
            //Suma las horas
            totalHoras += r.horasTrabajadas

            //suma horas factuables y no facturables
            if (r.horasFacturables)
                totalHorasFacturables += r.horasTrabajadas
            else
                totalHorasNoFacturables += r.horasTrabajadas
        })

        //Suma las horas y costos subtotales de los consultores
        consultoresProyecto.forEach(cp => {
            let horasFacturablesTrabajadas = 0

            resultados.forEach(r => {
                if (cp.idConsultor === r.idConsultor)
                    if (r.horasFacturables)
                        horasFacturablesTrabajadas += r.horasTrabajadas
            })

            //Subtotal
            subtotal += (horasFacturablesTrabajadas * cp.costo)

            tarifas.push({
                consultor: `${cp.nombresConsultor} ${cp.apellidosConsultor}`,
                costo: cp.costo,
                horasTrabajadas: horasFacturablesTrabajadas
            })
        })

        //Guarda los datos del reporte
        setReporteDeProyecto({
            ...reporteDeProyecto,
            proyecto: proyecto,
            consultor: consultor,
            etapa: etapa,
            cliente: cliente,
            periodo: periodo,
            tarifas: tarifas,
            actividades: resultados,
            totalHoras: totalHoras,
            totalHorasFacturables: totalHorasFacturables,
            totalHorasNoFacturables: totalHorasNoFacturables,
            subtotal: subtotal
        })

        //Alerta de información
        setMessage('Reporte generado correctamente.')
        setSeverity('success')
        setOpenAlert(true)
    }

    const limpiarFiltros = () => {
        setFiltrosProyecto({
            ...filtrosProyecto,
            clienteSeleccionado: '',
            proyectoSeleccionado: '',
            etapaSeleccionada: '',
            consultorSeleccionado: '',
            fecha1: '',
            fecha2: ''
        })
    }

    return (
        <div>
            <SnackbarAlert open={openAlert} onClose={() => setOpenAlert(false)} severity={severity}>
                {message}
            </SnackbarAlert>
            <div className={classes.filtrosContainer}>
                <Grid container spacing={2}>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <FormControl size='small' variant='filled' className={classes.formControl}>
                            <InputLabel id='clienteLabel'>Cliente</InputLabel>
                            <Select
                                labelId='clienteLabel'
                                value={filtrosProyecto.clienteSeleccionado}
                                onChange={(e) => setFiltrosProyecto({ ...filtrosProyecto, clienteSeleccionado: e.target.value })}>
                                {
                                    clientes.map((value, index) => (
                                        <MenuItem value={value.id} key={index}>
                                            {value.nombre}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <FormControl size='small' variant='filled' className={classes.formControl}>
                            <InputLabel id='proyectoLabel'>Proyecto</InputLabel>
                            <Select
                                labelId='proyectoLabel'
                                value={filtrosProyecto.proyectoSeleccionado}
                                onChange={(e) => setFiltrosProyecto({ ...filtrosProyecto, proyectoSeleccionado: e.target.value })}>
                                {
                                    proyectos.map((value, index) => (
                                        <MenuItem value={value.id} key={index}>
                                            {value.nombre}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <FormControl size='small' variant='filled' className={classes.formControl}>
                            <InputLabel id='etapaLabel'>Etapa</InputLabel>
                            <Select
                                labelId='etapaLabel'
                                onChange={(e) => setFiltrosProyecto({ ...filtrosProyecto, etapaSeleccionada: e.target.value })}
                                value={filtrosProyecto.etapaSeleccionada}>
                                <MenuItem value=''>Todas las etapas</MenuItem>
                                <Divider />
                                {
                                    etapasProyecto.map((value, index) => (
                                        <MenuItem value={value.idEtapa} key={index}>
                                            {value.nombre}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <FormControl size='small' variant='filled' className={classes.formControl}>
                            <InputLabel id='consultorLabel'>Consultor</InputLabel>
                            <Select
                                labelId='consultorLabel'
                                onChange={(e) => setFiltrosProyecto({ ...filtrosProyecto, consultorSeleccionado: e.target.value })}
                                value={filtrosProyecto.consultorSeleccionado}>
                                <MenuItem value=''>Todos los consultores</MenuItem>
                                <Divider />
                                {
                                    consultoresProyecto.map((value, index) => (
                                        <MenuItem value={value.idConsultor} key={index}>
                                            {`${value.nombresConsultor} ${value.apellidosConsultor}`}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <Typography variant='caption'>Primer periodo de actividad</Typography>
                        <TextField
                            type='date'
                            size='small'
                            value={filtrosProyecto.fecha1}
                            onChange={(e) => setFiltrosProyecto({ ...filtrosProyecto, fecha1: e.target.value })}
                            className={classes.formControl} />
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <Typography variant='caption'>Segundo periodo de actividad</Typography>
                        <TextField
                            type='date'
                            size='small'
                            value={filtrosProyecto.fecha2}
                            onChange={(e) => setFiltrosProyecto({ ...filtrosProyecto, fecha2: e.target.value })}
                            className={classes.formControl} />
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        {
                            isLoading ?
                                <CircularProgress variant='indeterminate' /> :
                                <Button
                                    variant='contained'
                                    color='primary'
                                    startIcon={<SearchIcon />}
                                    fullWidth
                                    onClick={buscarReporte}>
                                    Buscar
                                </Button>
                        }
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <Button
                            variant='contained'
                            color='secondary'
                            startIcon={<FilterListIcon />}
                            fullWidth
                            onClick={limpiarFiltros}>
                            Limpiar filtros
                        </Button>
                    </Grid>
                </Grid>
            </div>
            {
                resultadosReporte != null && (
                    resultadosReporte ?
                        (
                            <Fragment>
                                <Divider className={classes.divider} />
                                <ReportePlantillaProyecto datos={reporteDeProyecto} />
                            </Fragment>
                        ) :
                        (
                            <div>
                                <Typography variant='h5'>
                                    Sin resultados
                                </Typography>
                                <Typography>
                                    No se encontraron actividades que coincidan con los filtros ingresados.
                                </Typography>
                            </div>
                        )
                )
            }
        </div>
    )
}

export default ReportesProyecto
