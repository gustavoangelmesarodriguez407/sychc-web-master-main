import React, { useState, useEffect, Fragment } from 'react'
import {
    FormControl, InputLabel, Select, MenuItem,
    Typography, Grid, Button, Divider, TextField, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'
import ReportePlantillaConsultor from '../../../components/plantillas_reportes/ReportePlantillaConsultor'

/**APIs */
import { GetApi } from '../../../api/Services'

/**Icons */
import SearchIcon from '@material-ui/icons/Search'
import FilterListIcon from '@material-ui/icons/FilterList'

function ReportesConsultor() {
    const classes = useStyles()
    const [filtros, setFiltros] = useState({
        consultorSeleccionado: '',
        clienteSeleccionado: '',
        proyectoSeleccionado: '',
        fecha1: '',
        fecha2: ''
    })
    const [consultores, setConsultores] = useState([])
    const [clientes, setClientes] = useState([])
    const [proyectosConsultor, setProyectosConsultor] = useState([])
    const [clientesconsultor, setClientesConsultor] = useState([])

    //Resultados de búsqueda
    const [resultadosReporte, setResultadosReporte] = useState(null)
    const [reporte, setReporte] = useState({
        consultor: '',
        cliente: '',
        proyecto: '',
        periodo: '',
        totalHoras: 0,
        totalHorasFacturables: 0,
        totalHorasNoFacturables: 0,
        listaProyectos: [],
        clienteArray: []
    })

    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae los consultores y clientes
    useEffect(() => {
        GetApi('consultor').then(response => {
            if (response.status === 200) {
                response.result.then(cons => {
                    setConsultores(cons)
                })
            } else {
                response.result.then(msg => {
                    setMessage(msg)
                    setSeverity('error')
                    setOpenAlert(true)
                })
            }
        })

        GetApi('cliente').then(response => {
            if (response.status === 200) {
                response.result.then(cln => {
                    setClientes(cln)
                })
            } else {
                response.result.then(msg => {
                    setMessage(msg)
                    setSeverity('error')
                    setOpenAlert(true)
                })
            }
        })

        return () => {
            setConsultores([])
            setClientes([])
        }
    }, [])

    //Efecto que trae los proyectos del consultor seleccionado
    useEffect(() => {
        if (filtros.consultorSeleccionado.length > 0) {
            GetApi(`consultores_proyecto/lista-proyectos-consultor/${filtros.consultorSeleccionado}`)
                .then(response => {
                    if (response.status === 200) {
                        response.result.then(proys => {
                            setProyectosConsultor(proys)
                        })
                    } else {
                        setProyectosConsultor([])
                    }
                })
        }

        return () => {
            setProyectosConsultor([])
        }
    }, [filtros.consultorSeleccionado])

    //Función para buscar el reporte con filtros o sin filtros
    const buscarReporte = () => {
        const data = filtros
        const controller = `reporte/consultor?idConsultor=${data.consultorSeleccionado}&idCliente=${data.clienteSeleccionado}&idProyecto=${data.proyectoSeleccionado}&fecha1=${data.fecha1}&fecha2=${data.fecha2}`

        //Inicia la carga
        setIsLoading(true)

        //Trae los datos del reporte
        GetApi(controller).then(response => {
            if (response.status === 200) {
                response.result.then(rp => {
                    //Valida si encontró resultados
                    if (rp.length > 0)
                        setResultadosReporte(true)
                    else {
                        setResultadosReporte(false)
                        return
                    }

                    //Genera el reporte de horas
                    generarReporte(rp)
                })
            } else {
                response.result.then(msg => {
                    setMessage(msg)
                    setSeverity('error')
                    setOpenAlert(true)
                })
            }
        })

        //Para la carga
        setIsLoading(false)
    }

    //Función que genera el cuerpo del reporte
    const generarReporte = (resultados) => {
        let consultor = resultados[0].nombreConsultor
        let cliente = filtros.clienteSeleccionado === '' ? 'Todos los clientes' : resultados[0].nombreCliente
        let proyecto = filtros.proyectoSeleccionado === '' ? 'Todos los proyectos' : resultados[0].nombreProyecto
        let periodo =
            filtros.fecha1 === '' && filtros.fecha2 === '' ?
                'Mostrando resumen de trabajo' :
                filtros.fecha1 === filtros.fecha2 ?
                    `Mostrando resumen de trabajo del ${filtros.fecha1}.`
                    : `Mostrando resumen de trabajo del ${filtros.fecha1} al ${filtros.fecha2}.`

        let totalHoras = 0
        let totalHorasFacturables = 0
        let totalHorasNoFacturables = 0
        const totalesPorCliente = {};

        //Suma de las horas
        resultados.forEach(r => {
            //Suma las horas
            totalHoras += r.horasTrabajadas

            //suma horas factuables y no facturables
            if (r.horasFacturables)
                totalHorasFacturables += r.horasTrabajadas
            else
                totalHorasNoFacturables += r.horasTrabajadas
            // Inicializa el objeto para el cliente si no existe
            if (!totalesPorCliente[r.nombreCliente]) {
                totalesPorCliente[r.nombreCliente] = {
                    nombreCliente: r.nombreCliente,
                    totalHoras: 0,
                    totalHorasFacturables: 0,
                    totalHorasNoFacturables: 0
                };
            }

            // Suma las horas trabajadas totales
            totalesPorCliente[r.nombreCliente].totalHoras += r.horasTrabajadas;

            // Suma horas facturables y no facturables
            if (r.horasFacturables) {
                totalesPorCliente[r.nombreCliente].totalHorasFacturables += r.horasTrabajadas;
            } else {
                totalesPorCliente[r.nombreCliente].totalHorasNoFacturables += r.horasTrabajadas;
            }
        });

        // Transforma el objeto 'totalesPorCliente' en un array
        const mappedData = Object.values(totalesPorCliente);

        // Establece los clientes del consultor con los totales calculados
        //setClientesConsultor(mappedData);


        //Gyarda los datos del reporte para mostrarlos
        setReporte({
            ...reporte,
            consultor: consultor,
            cliente: cliente,
            proyecto: proyecto,
            periodo: periodo,
            listaProyectos: proyectosConsultor,
            totalHoras: totalHoras,
            totalHorasFacturables: totalHorasFacturables,
            totalHorasNoFacturables: totalHorasNoFacturables,
            clienteArray: mappedData
        })

        //Alerta de información
        setMessage('Reporte generado correctamente.')
        setSeverity('success')
        setOpenAlert(true)
    }

    //Función para limpiar filtros
    const limpiarFiltros = () => {
        setFiltros({
            ...filtros,
            consultorSeleccionado: '',
            proyectoSeleccionado: '',
            clienteSeleccionado: '',
            fecha1: '',
            fecha2: ''
        })
    }

    return (
        <div>
            <SnackbarAlert open={openAlert} onClose={() => setOpenAlert(false)} severity={severity}>
                {message}
            </SnackbarAlert>
            <div className={classes.filtrosContainer}>
                <Grid container spacing={2}>
                    <Grid item xl={6} lg={6} md={6} sm={6} xs={12}>
                        <FormControl size='small' variant='filled' className={classes.formControl}>
                            <InputLabel id='consultorLabel'>Consultor</InputLabel>
                            <Select
                                labelId='consultorLabel'
                                value={filtros.consultorSeleccionado}
                                onChange={(e) => setFiltros({ ...filtros, consultorSeleccionado: e.target.value })}>
                                {
                                    consultores.map((value, index) => (
                                        <MenuItem value={value.id} key={index}>
                                            {`${value.nombres} ${value.apellidos}`}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <FormControl size='small' variant='filled' className={classes.formControl}>
                            <InputLabel id='clienteLabel'>Cliente</InputLabel>
                            <Select
                                labelId='clienteLabel'
                                value={filtros.clienteSeleccionado}
                                onChange={(e) => setFiltros({ ...filtros, clienteSeleccionado: e.target.value })}>
                                <MenuItem value=''>Todos los clientes</MenuItem>
                                <Divider />
                                {
                                    clientes.map((value, index) => (
                                        <MenuItem value={value.id} key={index}>
                                            {value.nombre}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <FormControl size='small' variant='filled' className={classes.formControl}>
                            <InputLabel id='proyectoLabel'>Proyecto</InputLabel>
                            <Select
                                labelId='proyectoLabel'
                                value={filtros.proyectoSeleccionado}
                                onChange={(e) => setFiltros({ ...filtros, proyectoSeleccionado: e.target.value })}>
                                <MenuItem value=''>Todos los proyectos</MenuItem>
                                <Divider />
                                {
                                    proyectosConsultor.map((value, index) => {
                                        if (value.idCliente === filtros.clienteSeleccionado) {
                                            return (
                                                <MenuItem value={value.idProyecto} key={index}>
                                                    {`${value.nombreProyecto}`}
                                                </MenuItem>
                                            )
                                        } else {
                                            return null
                                        }
                                    })
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <Typography variant='caption'>Primer periodo de actividad</Typography>
                        <TextField
                            type='date'
                            size='small'
                            value={filtros.fecha1}
                            onChange={(e) => setFiltros({ ...filtros, fecha1: e.target.value })}
                            className={classes.formControl} />
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <Typography variant='caption'>Segundo periodo de actividad</Typography>
                        <TextField
                            type='date'
                            size='small'
                            value={filtros.fecha2}
                            onChange={(e) => setFiltros({ ...filtros, fecha2: e.target.value })}
                            className={classes.formControl} />
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        {
                            isLoading ?
                                <CircularProgress variant='indeterminate' /> :
                                <Button
                                    variant='contained'
                                    color='primary'
                                    startIcon={<SearchIcon />}
                                    fullWidth
                                    onClick={buscarReporte}>
                                    Buscar
                                </Button>
                        }
                    </Grid>
                    <Grid item xl={3} lg={3} md={3} sm={6} xs={12}>
                        <Button
                            variant='contained'
                            color='secondary'
                            startIcon={<FilterListIcon />}
                            fullWidth
                            onClick={limpiarFiltros}>
                            Limpiar filtros
                        </Button>
                    </Grid>
                </Grid>
            </div>
            {
                resultadosReporte != null && (
                    resultadosReporte ?
                        (
                            <Fragment>
                                <Divider className={classes.divider} />
                                <ReportePlantillaConsultor datos={reporte} />
                            </Fragment>
                        ) :
                        (
                            <div>
                                <Typography variant='h5'>
                                    Sin resultados
                                </Typography>
                                <Typography>
                                    No se encontraron actividades que coincidan con los filtros ingresados.
                                </Typography>
                            </div>
                        )
                )
            }
        </div>
    )
}

export default ReportesConsultor
