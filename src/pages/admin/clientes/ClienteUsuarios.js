import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import {
    Typography, Grid, List, IconButton, ListItemSecondaryAction,
    ListItemText, ListItem, ListItemAvatar, Avatar, ListItemIcon,
    FormControl, InputLabel, Select, MenuItem, Button,
    LinearProgress, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**APis */
import { GetByIdApi, GetApi, PostApi, DeleteApi } from '../../../api/Services'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**Icon */
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd'
import ClearIcon from '@material-ui/icons/Clear'
import AddIcon from '@material-ui/icons/Add'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'

function ClienteUsuarios(props) {
    const classes = useStyles()
    const { match: { params: { id } } } = props
    const [cliente, setCliente] = useState({})
    const [usuarios, setUsuarios] = useState([])
    const [usuariosCliente, setUsuariosCliente] = useState([])
    const [inputs, setInputs] = useState({
        idUsuario: '',
        idCliente: id
    })
    const [isLoading, setIsLoading] = useState(true)
    const [isLoadingUpload, setIsLoadingUpload] = useState(false)
    const [reloadData, setReloadData] = useState(false)
    const [notFound, setNotFound] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae todos los datos
    useEffect(() => {
        //Datos del consultor
        GetByIdApi('cliente', id).then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setCliente(dato)
                })
            } else {
                setNotFound(true)
            }
        })

        //Datos de toos los perfules
        GetApi('usuario').then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setUsuarios(dato)
                })
            }
        })

        //Para la carga
        setIsLoading(false)

        return () => {
            setCliente({})
            setUsuarios([])
        }
    }, [id, isLoading])

    //Efecto que reacciona a los cambios de los datos principales
    useEffect(() => {
        setReloadData(false)

        //Perfiles del consultor
        GetApi(`clientes_usuario/lista-usuarios/${id}`).then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setUsuariosCliente(dato)
                    setIsLoading(false)
                })
            }
        })

        return () => {
            setUsuariosCliente([])
        }
    }, [id, reloadData])

    //Función para guardar lso datos
    const changeForm = (e) => {
        if (e.target.type === 'number') {
            setInputs({
                ...inputs,
                [e.target.name]: Number(e.target.value)
            })
        } else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            })
        }
    }

    //Función para añadir un perfil
    const create = () => {
        const data = inputs

        if (!minLengthValidation(1, data.idUsuario)) {
            setMessage('Seleccione un usuario.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Carga de los datos
        setIsLoadingUpload(true)

        //Añade los datos
        PostApi('clientes_usuario', data).then(response => {
            if (response.status === 200) {
                setMessage('Usuario añadido correctamente.')
                setSeverity('success')
                setOpenAlert(true)

                //Recarfa los daotos
                setReloadData(true)
            } else {
                response.result.then(text => {
                    setMessage(text)
                    setSeverity('error')
                    setOpenAlert(true)
                })
            }
        })

        //Detiene carga
        setIsLoadingUpload(false)
        setIsLoading(true)
    }

    //Función para eliminar elemento
    const deleteElement = (id) => {
        DeleteApi('clientes_usuario', id)
            .then(response => {
                if (response.status === 200) {
                    response.result.then(text => {
                        setMessage(text)
                        setOpenAlert(true)
                        setSeverity('success')

                        //Inicia la recarga de los datos
                        setReloadData(true)
                    })
                }
            })

        //Para la carga
        setReloadData(false)
        setIsLoading(true)
    }


    //En lo que carga la info
    if (isLoading) {
        return <LinearProgress variant='indeterminate' />
    }

    //si no fue encontrado
    if (notFound) {
        return <Redirect to='/admin/clientes' />
    }

    return (
        <div>
            <SnackbarAlert open={openAlert} onClose={() => setOpenAlert(false)} severity={severity}>
                {message}
            </SnackbarAlert>
            <Button color='primary' startIcon={<ArrowBackIosIcon />} onClick={() => props.history.goBack()}>
                Volver
            </Button>
            <Typography variant='h5'>Gestión de usuarios de cliente</Typography>
            <Typography variant='caption'>
                {`${cliente.nombre}`}
            </Typography>
            <Grid container spacing={2} className={classes.gridContainer}>
                <Grid item lg={6}>
                    <Typography variant='h6'>Usuarios</Typography>
                    <List className={classes.listContent}>
                        {
                            usuariosCliente.length > 0 ?
                                usuariosCliente.map((value, index) => (
                                    <ListItem key={index}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AssignmentIndIcon />
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={value.nombreUsuario}
                                            secondary={`${value.correoElectronico}`} />
                                        <ListItemSecondaryAction>
                                            <IconButton onClick={() => deleteElement(value.id)}>
                                                <ClearIcon />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                )) :
                                <ListItem>
                                    <ListItemIcon>
                                        <ClearIcon />
                                    </ListItemIcon>
                                    <ListItemText
                                        primary={'Sin usuarios'} />
                                </ListItem>
                        }
                    </List>
                </Grid>
                <Grid item lg={6}>
                    <Typography variant='h6'>Agregar usuario</Typography>
                    <form onChange={changeForm} className={classes.form}>
                        <Grid container spacing={2}>
                            <Grid item lg={12}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id='usuarioLabel'>Seleccione un usuario tipo cliente</InputLabel>
                                    <Select
                                        name='idUsuario'
                                        labelId='usuarioLabel'
                                        onChange={changeForm}
                                        value={inputs.idUsuario}>
                                        {
                                            usuarios.map((value, index) => (
                                                value.tipoUsuario === 'cliente'
                                                && !value.asignadoTipoUsuario && (
                                                    <MenuItem value={value.id} key={index}>
                                                        {value.correoElectronico}
                                                    </MenuItem>
                                                )
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                    {
                        isLoadingUpload ?
                            <CircularProgress variant='indeterminate' /> :
                            <Button
                                color='primary'
                                variant='contained'
                                startIcon={<AddIcon />}
                                onClick={create}>
                                Añadir usuario
                            </Button>
                    }
                </Grid>
            </Grid>
        </div>
    )
}

export default ClienteUsuarios
