import React, { useState, Fragment } from 'react'
import {
    Dialog, DialogTitle, DialogContent,
    DialogActions, DialogContentText, TextField, FormControl,
    Button, Grid, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**API */
import { PostApi, UploadFileApi } from '../../../api/Services'

function CrearCliente(props) {
    const classes = useStyles()
    const { open, handleClose, handleReload } = props
    const [inputs, setInputs] = useState({
        nombre: '',
        rfc: '',
        razonSocial: '',
        direccionFiscal: '',
        imagenURl: ''
    })
    const [imagen, setImagen] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        if (e.target.type === 'file') {
            setImagen(e.target.files[0])
        } else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            })
        }
    }

    //Función para crear usuario
    const create = () => {
        const data = inputs

        if (!minLengthValidation(3, data.nombre)) {
            setMessage('Ingrese un nombre válido.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(12, data.rfc)) {
            setMessage('El RFC debe tener al menos 12 caracteres.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.razonSocial)) {
            setMessage('Ingrese una razón social válida.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.direccionFiscal)) {
            setMessage('Ingrese una dirección fiscal válida.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (imagen) {
            if (!imagen.type.includes("image")) {
                setMessage('Seleccione una imagen en formato .jpg o .png')
                setSeverity('error')
                setOpenAlert(true)
                return
            }
        } else {
            setMessage('Seleccione una imagen.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Inicia la carga
        setIsLoading(true)

        //Sube la imagen
        UploadFileApi(imagen).then(response => {
            if (response.status === 200) {
                response.result.then(imageName => {
                    //Guarda el nombre de la imagen
                    data.imagenURl = imageName
                    createCliente(data)
                })
            } else {
                setMessage('Error al subir la imagen.')
                setSeverity('error')
                setOpenAlert(true)
                setIsLoading(false)
                return
            }
        })
    }

    //Función que crea el cliente
    const createCliente = (data) => {
        //Guarda los datos
        PostApi('cliente', data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Cliente añadido correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    //Cierra modal
                    handleClose()
                    handleReload()
                    //Limpia los estados
                    setInputs({
                        ...inputs,
                        nombre: '',
                        rfc: '',
                        razonSocial: '',
                        direccionFiscal: ''
                    })
                    setImagen(null)
                    setIsLoading(false)
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Dialog
                open={open}
                onClose={handleClose}>
                <DialogTitle>Crear cliente</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Rellene todos los campos para crear un nuevo cliente.
                    </DialogContentText>
                    <form onChange={changeForm}>
                        <Grid container spacing={2}>
                            <Grid item lg={6}>
                                <TextField
                                    name='nombre'
                                    variant='filled'
                                    label='Nombre'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <TextField
                                    name='rfc'
                                    variant='filled'
                                    label='RFC'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <TextField
                                    name='razonSocial'
                                    variant='filled'
                                    label='Razón social'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <TextField
                                    name='direccionFiscal'
                                    variant='filled'
                                    label='Dirección fiscal'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={12}>
                                <FormControl>
                                    <label
                                        htmlFor='actividadArchivo'
                                        className={classes.archivoBtn}>
                                        {imagen ? imagen.name : 'Elegir imagen del cliente.'}
                                    </label>
                                    <input
                                        id='actividadArchivo'
                                        type='file'
                                        accept='image/*'
                                        multiple={false}
                                        style={{ opacity: 0 }} />
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                </DialogContent>
                <DialogActions>
                    {
                        isLoading ?
                            <CircularProgress variant='indeterminate' /> :
                            <Fragment>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={create}>
                                    Aceptar
                                </Button>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    onClick={handleClose}>
                                    Cancelar
                                </Button>
                            </Fragment>
                    }
                </DialogActions>
            </Dialog>
        </Fragment>
    )
}

export default CrearCliente
