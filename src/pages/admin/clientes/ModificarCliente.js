import React, { useState, useEffect, Fragment } from 'react'
import { Redirect, Link } from 'react-router-dom'
import {
    TextField, Button, Grid, CircularProgress, Typography,
    FormControl,
    LinearProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'
import { BASE_PATH, API } from '../../../helpers/ApiConfig'

/**API */
import { GetByIdApi, PutApi, UploadFileApi } from '../../../api/Services'

function ModificarCliente(props) {
    const { match: { params: { id } } } = props
    const parentPath = '/admin/clientes'
    const classes = useStyles()

    const [inputs, setInputs] = useState({})
    const [imagen, setImagen] = useState(null)
    const [notFound, setNotFound] = useState(false)
    const [isLoadingData, setIsLoadingData] = useState(true)
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae los tipos de usuario y la info del usuario
    useEffect(() => {
        GetByIdApi('cliente', id).then(response => {
            if (response.status === 200) {
                response.result.then(usuario => {
                    setInputs(usuario)
                    setIsLoadingData(false)
                })
            } else {
                setNotFound(true)
            }
        })

        return () => setInputs([])
    }, [id])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        if (e.target.type === 'file') {
            setImagen(e.target.files[0])
        } else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            })
        }
    }

    //Función para crear usuario
    const update = () => {
        const data = inputs

        if (!minLengthValidation(3, data.nombre)) {
            setMessage('Ingrese un nombre válido.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(12, data.rfc)) {
            setMessage('El RFC debe tener al menos 12 caracteres.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.razonSocial)) {
            setMessage('Ingrese una razón social válida.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.direccionFiscal)) {
            setMessage('Ingrese una dirección fiscal válida.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (imagen) {
            if (!imagen.type.includes("image")) {
                setMessage('Seleccione una imagen en formato .jpg o .png')
                setSeverity('error')
                setOpenAlert(true)
                return
            }
        }

        //Inicia la carga
        setIsLoading(true)

        //Trata de subir la imagen
        if (imagen) {
            UploadFileApi(imagen).then(response => {
                if (response.status === 200) {
                    response.result.then(imageName => {
                        //Modifca el nombre de la imagen
                        data.imagenURL = imageName
                        updateCliente(data)
                    })
                } else {
                    //Guarda el cliente sin cambiar la imagen
                    setMessage('Error al subir la imagen.')
                    setSeverity('error')
                    setOpenAlert(true)
                    setIsLoading(false)
                }
            })
        } else {
            //Guarda el cliente sin cambiar la imagen
            updateCliente(data)
        }
    }

    const updateCliente = (data) => {
        //Guarda los datos
        PutApi('cliente', id, data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Cliente modificado correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    setTimeout(() => {
                        setNotFound(true)
                    }, 500);
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    //Si no fue encontrado el dato
    if (notFound) {
        return <Redirect to={parentPath} />
    }

    //En lo que carga la información del registro
    if (isLoadingData) {
        return <LinearProgress variant='indeterminate' />
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Typography variant='h5'>Modificar cliente</Typography>
            <div>
                <Typography variant='subtitle1'>
                    Rellene todos los campos para modificar los datos.
                </Typography>
                <img
                    src={`${BASE_PATH}/${API}/archivo/${inputs.imagenURL}`}
                    alt={inputs.imagenURl}
                    className={classes.image} />
                <form onChange={changeForm} className={classes.form}>
                    <Grid container spacing={2}>
                        <Grid item lg={6}>
                            <TextField
                                name='nombre'
                                variant='filled'
                                label='Nombre'
                                defaultValue={inputs.nombre}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='rfc'
                                variant='filled'
                                label='RFC'
                                defaultValue={inputs.rfc}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='razonSocial'
                                variant='filled'
                                label='Razón social'
                                defaultValue={inputs.razonSocial}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='direccionFiscal'
                                variant='filled'
                                label='Dirección fiscal'
                                defaultValue={inputs.direccionFiscal}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={12}>
                            <FormControl>
                                <label
                                    htmlFor='actividadArchivo'
                                    className={classes.archivoBtn}>
                                    {imagen ? imagen.name : 'Cambiar imagen del cliente.'}
                                </label>
                                <input
                                    id='actividadArchivo'
                                    type='file'
                                    accept='image/*'
                                    multiple={false}
                                    style={{ opacity: 0 }} />
                            </FormControl>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <div>
                {
                    isLoading ?
                        <CircularProgress variant='indeterminate' /> :
                        <div className={classes.buttonContainer}>
                            <Button
                                variant='contained'
                                color='primary'
                                className={classes.button}
                                onClick={update}>
                                Aceptar
                            </Button>
                            <Link to={parentPath} className={classes.link}>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    className={classes.button}>
                                    Cancelar
                                </Button>
                            </Link>
                        </div>
                }
            </div>
        </Fragment>
    )
}

export default ModificarCliente
