import React, { useState, useEffect, Fragment } from 'react'
import { Redirect, Link } from 'react-router-dom'
import {
    TextField, Button, Grid, FormControl, InputLabel, Select,
    MenuItem, CircularProgress, Typography, LinearProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { BASE_PATH, API } from '../../../helpers/ApiConfig'
import { minLengthValidation } from '../../../helpers/Validations'
import dateISOFormatter from '../../../helpers/DateFormatter'

/**API */
import { GetApi, GetByIdApi, PutApi, UploadFileApi } from '../../../api/Services'

function ModificarProyecto(props) {
    const { match: { params: { id } } } = props
    const parentPath = '/admin/proyectos'
    const classes = useStyles()

    const [clientes, setClientes] = useState([])
    const [inputs, setInputs] = useState({})
    const [etapasProyecto, setEtapasProyecto] = useState([])
    const [imagen, setImagen] = useState(null)
    const [notFound, setNotFound] = useState(false)
    const [isLoadingData, setIsLoadingData] = useState(true)
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')
    const [idUsuario, setidUsuario] = useState(null)
    const session_id = window.localStorage.getItem('SESSION_ID')

    //Efecto que trae los tipos de usuario y la info del usuario
    useEffect(() => {
        GetByIdApi('proyecto', id).then(response => {
            if (response.status === 200) {
                response.result.then(usuario => {
                    setInputs(usuario)
                    setIsLoadingData(false)
                })
            } else {
                setNotFound(true)
            }
        })

        //Busca las etapas del proyecto
        GetApi(`etapas_proyecto/lista-etapas/${id}`).then(response => {
            if (response.status === 200) {
                response.result.then(etps => {
                    setEtapasProyecto(etps)
                })
            } else {
                setEtapasProyecto([])
            }
        })

        GetApi('cliente').then(response => {
            if (response.status === 200) {
                response.result.then(datos => {
                    setClientes(datos)
                })
            }
        })

        //Info de la sesion e Id de usuario
        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setidUsuario(ses.idUsuario)
                })
            }
        })

        return () => {
            setClientes([])
            setEtapasProyecto([])
        }
    }, [id, session_id])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        if (e.target.type === 'number') {
            setInputs({
                ...inputs,
                [e.target.name]: Number(e.target.value)
            })
        } else if (e.target.type === 'file') {
            setImagen(e.target.files[0])
        } else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            })
        }
    }

    //Función para crear usuario
    const update = () => {
        const data = inputs

        if (!minLengthValidation(3, data.nombre.trim())) {
            setMessage('Ingrese un nombre de más de 3 caracteres.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.descripcion.trim())) {
            setMessage('Ingrese una descripción.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.idCliente.trim())) {
            setMessage('Seleccione un cliente.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.fechaInicio.trim())) {
            setMessage('Ingrese una fecha de inicio.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.fechaFinal.trim())) {
            setMessage('Ingrese una fecha final.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.moneda.trim())) {
            setMessage('Ingrese un código de moneda.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.horasEstimadas <= 0) {
            setMessage('Las horas estimadas deben ser mayor a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.horasTotales < 0) {
            setMessage('Las horas estimadas deben ser mayor o igual a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.precioEstimado <= 0) {
            setMessage('El precio estimado debe ser mayor a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.precioReal < 0) {
            setMessage('El precio real debe ser mayor o igual a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.costoEstimado <= 0) {
            setMessage('El costo estimado debe ser mayor a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.costoReal < 0) {
            setMessage('El costo real debe ser mayor o igual a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Si hay imagen, valida el formato
        if (imagen) {
            //Valida la extensión
            if (!imagen.type.includes("image")) {
                setMessage('Seleccione una imagen .jpg o .png')
                setSeverity('error')
                setOpenAlert(true)
                return
            }
        }

        //Modificado por
        data.modificadoPor = idUsuario

        //Inicia la carga
        setIsLoading(true)

        //Si hay imagen, sube el archivo
        if (imagen) {
            //Sube la imagen
            UploadFileApi(imagen).then(response => {
                if (response.status === 200) {
                    response.result.then(imagenName => {
                        //Guarda el proyecto con el nombre de la imagen
                        data.imagenURl = imagenName;
                        updateProyecto(data)
                    })
                } else {
                    setMessage('Error al subir la imagen.')
                    setSeverity('error')
                    setOpenAlert(true)
                    setIsLoading(false)
                }
            })
        } else {
            //Guarda el poryecto sin cambios en la imaen
            updateProyecto(data)
        }
    }

    const updateProyecto = (data) => {
        //Guarda los datos
        PutApi('proyecto', id, data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Proyecto modificado correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    setTimeout(() => {
                        setNotFound(true)
                    }, 500);
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    //Si no fue encontrado el dato
    if (notFound) {
        return <Redirect to={parentPath} />
    }

    //En lo que carga la información del registro
    if (isLoadingData) {
        return <LinearProgress variant='indeterminate' />
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Typography variant='h5'>Modificar proyecto</Typography>
            <div>
                <Typography variant='subtitle1'>
                    Rellene todos los campos para modificar los datos.
                </Typography>
                <img
                    src={`${BASE_PATH}/${API}/archivo/${inputs.imagenURL}`}
                    alt={inputs.imagenURl}
                    className={classes.image} />
                <form onChange={changeForm} className={classes.form}>
                    <Grid container spacing={2}>
                        <Grid item lg={12}>
                            <TextField
                                name='nombre'
                                variant='filled'
                                label='Nombre del proyecto'
                                defaultValue={inputs.nombre}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={12}>
                            <TextField
                                name='descripcion'
                                variant='filled'
                                label='Descripción'
                                defaultValue={inputs.descripcion}
                                rows={5}
                                multiline
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={12}>
                            <FormControl variant="filled" className={classes.formControl}>
                                <InputLabel id="clientesLabel">Cliente</InputLabel>
                                <Select
                                    name='idCliente'
                                    labelId="clientesLabel"
                                    onChange={changeForm}
                                    value={inputs.idCliente}>
                                    {
                                        clientes.map((values, index) => (
                                            <MenuItem key={index} value={values.id}>
                                                {values.nombre}
                                            </MenuItem>
                                        ))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={6}>
                            <Typography>Fecha de inicio</Typography>
                            <TextField
                                name='fechaInicio'
                                variant='standard'
                                type='date'
                                defaultValue={dateISOFormatter(inputs.fechaInicio)}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <Typography>Fecha de término</Typography>
                            <TextField
                                name='fechaFinal'
                                variant='standard'
                                type='date'
                                defaultValue={dateISOFormatter(inputs.fechaFinal)}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='horasEstimadas'
                                type='number'
                                variant='filled'
                                label='Horas estimadas'
                                defaultValue={inputs.horasEstimadas}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='horasTotales'
                                type='number'
                                variant='filled'
                                label='Horas totales'
                                defaultValue={inputs.horasTotales}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <FormControl variant="filled" className={classes.formControl}>
                                <InputLabel id="monedaLabel">Moneda</InputLabel>
                                <Select
                                    name='moneda'
                                    labelId="monedaLabel"
                                    onChange={changeForm}
                                    value={inputs.moneda}>
                                    <MenuItem value='USD'>USD</MenuItem>
                                    <MenuItem value='MXN'>MXN</MenuItem>
                                    <MenuItem value='EUR'>EUR</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={6}>
                            <FormControl variant="filled" className={classes.formControl}>
                                <InputLabel id="estadoLabel">Estado actual</InputLabel>
                                <Select
                                    name='estadoActual'
                                    labelId="estadoLabel"
                                    onChange={changeForm}
                                    value={inputs.estadoActual}>
                                    <MenuItem value='Iniciado'>Iniciado</MenuItem>
                                    <MenuItem value='En Proceso'>En Proceso</MenuItem>
                                    <MenuItem value='Terminado'>Terminado</MenuItem>
                                    <MenuItem value='Cancelado'>Cancelado</MenuItem>
                                    <MenuItem value='En Espera'>En Espera</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={6}>
                            <FormControl variant="filled" className={classes.formControl}>
                                <InputLabel id="etapaLabel">Etapa actual</InputLabel>
                                <Select
                                    name='etapaActual'
                                    labelId="etapaLabel"
                                    onChange={changeForm}
                                    value={inputs.etapaActual}>
                                    {
                                        etapasProyecto.map((values, index) => (
                                            <MenuItem key={index} value={values.idEtapa}>
                                                {values.nombre}
                                            </MenuItem>
                                        ))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='precioEstimado'
                                type='number'
                                variant='filled'
                                label='Precio estimado'
                                defaultValue={inputs.precioEstimado}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='precioReal'
                                type='number'
                                variant='filled'
                                label='Precio real'
                                defaultValue={inputs.precioReal}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='costoEstimado'
                                type='number'
                                variant='filled'
                                label='Costo estimado'
                                defaultValue={inputs.costoEstimado}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={6}>
                            <TextField
                                name='costoReal'
                                type='number'
                                variant='filled'
                                label='Costo real'
                                defaultValue={inputs.costoReal}
                                className={classes.formControl} />
                        </Grid>
                        <Grid item lg={12}>
                            <FormControl>
                                <label
                                    htmlFor='actividadArchivo'
                                    className={classes.archivoBtn}>
                                    {imagen ? imagen.name : 'Cambiar imagen del proyecto'}
                                </label>
                                <input
                                    id='actividadArchivo'
                                    type='file'
                                    accept='image/*'
                                    multiple={false}
                                    style={{ opacity: 0 }} />
                            </FormControl>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <div>
                {
                    isLoading ?
                        <CircularProgress variant='indeterminate' /> :
                        <div className={classes.buttonContainer}>
                            <Button
                                variant='contained'
                                color='primary'
                                className={classes.button}
                                onClick={update}>
                                Aceptar
                            </Button>
                            <Link to={parentPath} className={classes.link}>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    className={classes.button}>
                                    Cancelar
                                </Button>
                            </Link>
                        </div>
                }
            </div>
        </Fragment>
    )
}

export default ModificarProyecto
