import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import {
    Typography, Grid, List, IconButton, ListItemSecondaryAction,
    ListItemText, ListItem, ListItemAvatar, Avatar, ListItemIcon,
    FormControl, InputLabel, Select, MenuItem, Button,
    LinearProgress, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**APis */
import { GetByIdApi, GetApi, PostApi, DeleteApi } from '../../../api/Services'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**Icon */
import TimelineIcon from '@material-ui/icons/Timeline'
import ClearIcon from '@material-ui/icons/Clear'
import AddIcon from '@material-ui/icons/Add'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'

function ConsultoresProyecto(props) {
    const classes = useStyles()
    const { match: { params: { id } } } = props
    const [proyecto, setProyecto] = useState({})
    const [consultores, setConsultores] = useState([])
    const [consultoresProyecto, setConsultoresProyecto] = useState([])
    const [perfilesConsultor, setPerfilesConsultor] = useState([])
    const [inputs, setInputs] = useState({
        idConsultor: '',
        idPerfil: '',
        idProyecto: id
    })
    const [isLoading, setIsLoading] = useState(true)
    const [isLoadingUpload, setIsLoadingUpload] = useState(false)
    const [reloadData, setReloadData] = useState(false)
    const [notFound, setNotFound] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae todos los datos ala verga
    useEffect(() => {
        //Datos del consultor
        GetByIdApi('proyecto', id).then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setProyecto(dato)
                })
            } else {
                setNotFound(true)
            }
        })

        //Datos de todos los consultores
        GetApi('consultor').then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setConsultores(dato)
                })
            }
        })

        //Para la carga
        setIsLoading(false)

        return () => {
            setConsultores([])
            setProyecto([])
        }
    }, [id, isLoading])

    //Efecto que reacciona a los cambios de los datos principales
    useEffect(() => {
        setReloadData(false)

        //Perfiles del consultor
        GetApi(`consultores_proyecto/lista-consultores/${id}`).then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setConsultoresProyecto(dato)
                    setIsLoading(false)
                })
            }
        })

        return () => {
            setConsultoresProyecto([])
        }
    }, [id, reloadData])

    //Efecto que trae los perfiles del consultor asignado
    useEffect(() => {
        if (inputs.idConsultor.length > 0) {
            GetApi(`perfiles_consultor/lista-perfiles/${inputs.idConsultor}`)
                .then(response => {
                    if (response.status === 200) {
                        response.result.then(pc => {
                            setPerfilesConsultor(pc)
                        })
                    } else {
                        setPerfilesConsultor([])
                    }
                })
        }

        return () => {
            setPerfilesConsultor([])
        }
    }, [inputs.idConsultor])

    //Función para guardar lso datos
    const changeForm = (e) => {
        if (e.target.type === 'number') {
            setInputs({
                ...inputs,
                [e.target.name]: Number(e.target.value)
            })
        } else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            })
        }
    }

    //Función para añadir un perfil
    const create = () => {
        const data = inputs

        if (!minLengthValidation(1, data.idConsultor)) {
            setMessage('Seleccione un consultor.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(1, data.idPerfil)) {
            setMessage('Seleccione un perfil.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Carga de los datos
        setIsLoadingUpload(true)

        //Añade los datos
        PostApi('consultores_proyecto', data).then(response => {
            if (response.status === 200) {
                setMessage('Consultor asignado correctamente.')
                setSeverity('success')
                setOpenAlert(true)

                //Recarfa los daotos
                setReloadData(true)
            } else {
                response.result.then(text => {
                    setMessage(text)
                    setSeverity('error')
                    setOpenAlert(true)
                })
            }
        })

        //Detiene carga
        setIsLoadingUpload(false)
    }

    //Función para eliminar elemento
    const deleteElement = (id) => {
        DeleteApi('consultores_proyecto', id)
            .then(response => {
                if (response.status === 200) {
                    response.result.then(text => {
                        setMessage(text)
                        setOpenAlert(true)
                        setSeverity('success')

                        //Inicia la recarga de los datos
                        setReloadData(true)
                    })
                }
            })

        //Para la carga
        setReloadData(false)
    }


    //En lo que carga la info
    if (isLoading) {
        return <LinearProgress variant='indeterminate' />
    }

    //si no fue encontrado
    if (notFound) {
        return <Redirect to='/admin/consultores' />
    }

    return (
        <div>
            <SnackbarAlert open={openAlert} onClose={() => setOpenAlert(false)} severity={severity}>
                {message}
            </SnackbarAlert>
            <Button color='primary' startIcon={<ArrowBackIosIcon />} onClick={() => props.history.goBack()}>
                Volver
            </Button>
            <Typography variant='h5'>Gestión de consultores de proyecto</Typography>
            <Typography variant='caption'>
                {proyecto.nombre}
            </Typography>
            <Grid container spacing={2} className={classes.gridContainer}>
                <Grid item lg={6}>
                    <Typography variant='h6'>Consultores asignados</Typography>
                    <List className={classes.listContent}>
                        {
                            consultoresProyecto.length > 0 ?
                                consultoresProyecto.map((value, index) => (
                                    <ListItem key={index}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <TimelineIcon />
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={`${value.nombresConsultor} ${value.apellidosConsultor}`}
                                            secondary={value.nombrePerfil} />
                                        <ListItemSecondaryAction>
                                            <IconButton onClick={() => deleteElement(value.id)}>
                                                <ClearIcon />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                )) :
                                <ListItem>
                                    <ListItemIcon>
                                        <ClearIcon />
                                    </ListItemIcon>
                                    <ListItemText
                                        primary={'Sin consultores'} />
                                </ListItem>
                        }
                    </List>
                </Grid>
                <Grid item lg={6}>
                    <Typography variant='h6'>Agregar consultor</Typography>
                    <form onChange={changeForm} className={classes.form}>
                        <Grid container spacing={2}>
                            <Grid item lg={6}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id='consultorLabel'>Seleccione un consultor</InputLabel>
                                    <Select
                                        name='idConsultor'
                                        labelId='consultorLabel'
                                        onChange={changeForm}
                                        value={inputs.idConsultor}>
                                        {
                                            consultores.map((value, index) => (
                                                <MenuItem value={value.id} key={index}>
                                                    {`${value.nombres} ${value.apellidos}`}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item lg={6}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id='consultorLabel'>Seleccione un perfil</InputLabel>
                                    <Select
                                        name='idPerfil'
                                        labelId='consultorLabel'
                                        onChange={changeForm}
                                        value={inputs.idPerfil}>
                                        {
                                            perfilesConsultor.map((value, index) => (
                                                <MenuItem value={value.idPerfil} key={index}>
                                                    {`${value.nombre} - ${value.nivel}`}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                    {
                        isLoadingUpload ?
                            <CircularProgress variant='indeterminate' /> :
                            <Button
                                color='primary'
                                variant='contained'
                                startIcon={<AddIcon />}
                                onClick={create}>
                                Añadir consultor
                            </Button>
                    }
                </Grid>
            </Grid>
        </div>
    )
}

export default ConsultoresProyecto
