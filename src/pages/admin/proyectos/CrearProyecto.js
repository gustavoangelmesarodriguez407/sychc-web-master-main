import React, { useState, useEffect, Fragment } from 'react'
import {
    Dialog, DialogTitle, DialogContent, Typography,
    DialogActions, DialogContentText, TextField,
    Button, Grid, FormControl, InputLabel, Select,
    MenuItem, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**API */
import { GetApi, GetByIdApi, PostApi, UploadFileApi } from '../../../api/Services'

function CrearProyecto(props) {
    const classes = useStyles()
    const { open, handleClose, handleReload } = props
    const [clientes, setClientes] = useState([])
    const [inputs, setInputs] = useState({
        nombre: '',
        descripcion: '',
        idCliente: '',
        fechaInicio: '',
        fechaFinal: '',
        horasEstimadas: 0,
        horasTotales: 0,
        etapaActual: null,
        estadoActual: 'Iniciado',
        moneda: 'USD',
        precioEstimado: 0,
        precioReal: 0,
        costoEstimado: 0,
        costoReal: 0,
        imagenURl: '',
        modificadoPor: '',
        ultimaModificacion: null
    })
    const [imagen, setImagen] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')
    const [idUsuario, setidUsuario] = useState(null)
    const session_id = window.localStorage.getItem('SESSION_ID')

    //Trae todos los clientes
    useEffect(() => {
        GetApi('cliente').then(response => {
            if (response.status === 200) {
                response.result.then(datos => {
                    setClientes(datos)
                })
            } else {
                setClientes([])
            }
        })

        //Info de la sesion e Id de usuario
        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setidUsuario(ses.idUsuario)
                })
            }
        })
        return () => setClientes([])
    }, [session_id])


    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        if (e.target.type === 'number') {
            setInputs({
                ...inputs,
                [e.target.name]: Number(e.target.value)
            })
        } else if (e.target.type === 'file') {
            setImagen(e.target.files[0])
        } else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            })
        }
    }

    //Función para crear usuario
    const create = () => {
        const data = inputs

        if (!minLengthValidation(3, data.nombre.trim())) {
            setMessage('Ingrese un nombre de más de 3 caracteres.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.descripcion.trim())) {
            setMessage('Ingrese una descripción.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.idCliente.trim())) {
            setMessage('Seleccione un cliente.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.fechaInicio.trim())) {
            setMessage('Ingrese una fecha de inicio.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.fechaFinal.trim())) {
            setMessage('Ingrese una fecha final.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(3, data.moneda.trim())) {
            setMessage('Ingrese un código de moneda.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.horasEstimadas <= 0) {
            setMessage('Las horas estimadas deben ser mayor a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.precioEstimado <= 0) {
            setMessage('El precio estimado debe ser mayor a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.costoEstimado <= 0) {
            setMessage('El costo estimado debe ser mayor a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (imagen) {
            //Valida la extensión
            if (!imagen.type.includes("image")) {
                setMessage('Seleccione una imagen .jpg o .png')
                setSeverity('error')
                setOpenAlert(true)
                return
            }
        } else {
            setMessage('Seleccione una imagen para el proyecto.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Modidicado por
        data.modificadoPor = idUsuario

        //Inicia la carga
        setIsLoading(true)

        //Sube la imagen
        UploadFileApi(imagen).then(response => {
            if (response.status === 200) {
                response.result.then(imagenName => {
                    //Guarda el proyecto con el nombre de la imagen
                    data.imagenURl = imagenName;
                    createProyecto(data)
                })
            } else {
                //Para la carga
                setIsLoading(false)

                setMessage('Error al subir la imagen.')
                setSeverity('error')
                setOpenAlert(true)
                return
            }
        })
    }

    const createProyecto = (data) => {
        //Guarda los datos
        PostApi('proyecto', data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Proyecto añadido correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    //Cierra modal
                    handleClose()
                    handleReload()
                    //Limpia los estados
                    setInputs({
                        ...inputs,
                        nombre: '',
                        descripcion: '',
                        idCliente: '',
                        fechaInicio: '',
                        fechaFinal: '',
                        horasEstimadas: 0,
                        horasTotales: 0,
                        etapaActual: null,
                        estadoActual: 'Iniciado',
                        moneda: '',
                        precioEstimado: 0,
                        precioReal: 0,
                        costoEstimado: 0,
                        costoReal: 0,
                        imagenURl: '',
                        modificadoPor: idUsuario,
                        ultimaModificacion: null
                    })
                    setImagen(null)
                    setIsLoading(false)
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Dialog
                open={open}
                onClose={handleClose}>
                <DialogTitle>Crear proyecto</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Rellene todos los campos para crear un nuevo proyecto.
                    </DialogContentText>
                    <form onChange={changeForm}>
                        <Grid container spacing={2}>
                            <Grid item lg={12}>
                                <TextField
                                    name='nombre'
                                    variant='filled'
                                    label='Nombre del proyecto'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={12}>
                                <TextField
                                    name='descripcion'
                                    variant='filled'
                                    label='Descripción'
                                    rows={5}
                                    multiline
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={12}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="clientesLabel">Cliente</InputLabel>
                                    <Select
                                        name='idCliente'
                                        labelId="clientesLabel"
                                        onChange={changeForm}
                                        value={inputs.idCliente}>
                                        {
                                            clientes.map((values, index) => (
                                                <MenuItem key={index} value={values.id}>
                                                    {values.nombre}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item lg={6}>
                                <Typography>Fecha de inicio</Typography>
                                <TextField
                                    name='fechaInicio'
                                    variant='standard'
                                    type='date'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <Typography>Fecha de término</Typography>
                                <TextField
                                    name='fechaFinal'
                                    variant='standard'
                                    type='date'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <TextField
                                    name='horasEstimadas'
                                    type='number'
                                    variant='filled'
                                    label='Horas estimadas'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id="monedaLabel">Moneda</InputLabel>
                                    <Select
                                        name='moneda'
                                        labelId="monedaLabel"
                                        onChange={changeForm}
                                        value={inputs.moneda}>
                                        <MenuItem value='USD'>USD</MenuItem>
                                        <MenuItem value='MXN'>MXN</MenuItem>
                                        <MenuItem value='EUR'>EUR</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item lg={6}>
                                <TextField
                                    name='precioEstimado'
                                    type='number'
                                    variant='filled'
                                    label='Precio estimado'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={6}>
                                <TextField
                                    name='costoEstimado'
                                    type='number'
                                    variant='filled'
                                    label='Costo estimado'
                                    className={classes.formControl} />
                            </Grid>
                            <Grid item lg={12}>
                                <FormControl>
                                    <label
                                        htmlFor='actividadArchivo'
                                        className={classes.archivoBtn}>
                                        {imagen ? imagen.name : 'Elegir imagen del proyecto'}
                                    </label>
                                    <input
                                        id='actividadArchivo'
                                        type='file'
                                        accept='image/*'
                                        multiple={false}
                                        style={{ opacity: 0 }} />
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                </DialogContent>
                <DialogActions>
                    {
                        isLoading ?
                            <CircularProgress variant='indeterminate' /> :
                            <Fragment>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={create}>
                                    Aceptar
                                </Button>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    onClick={handleClose}>
                                    Cancelar
                                </Button>
                            </Fragment>
                    }
                </DialogActions>
            </Dialog>
        </Fragment>
    )
}

export default CrearProyecto
