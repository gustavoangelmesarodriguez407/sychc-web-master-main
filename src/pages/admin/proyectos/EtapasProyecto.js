import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import {
    Typography, Grid, List, IconButton, ListItemSecondaryAction,
    ListItemText, ListItem, ListItemAvatar, Avatar, ListItemIcon,
    FormControl, InputLabel, Select, MenuItem, Button,
    LinearProgress, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**APis */
import { GetByIdApi, GetApi, PostApi, DeleteApi } from '../../../api/Services'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**Icon */
import CategoryIcon from '@material-ui/icons/Category'
import ClearIcon from '@material-ui/icons/Clear'
import AddIcon from '@material-ui/icons/Add'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'

function EtapasProyecto(props) {
    const classes = useStyles()
    const { match: { params: { id } } } = props
    const [proyecto, setProyecto] = useState({})
    const [etapas, setEtapas] = useState([])
    const [etapasProyecto, setEtapasProyecto] = useState([])
    const [inputs, setInputs] = useState({
        idEtapa: '',
        idProyecto: id
    })
    const [isLoading, setIsLoading] = useState(true)
    const [isLoadingUpload, setIsLoadingUpload] = useState(false)
    const [reloadData, setReloadData] = useState(false)
    const [notFound, setNotFound] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae todos los datos ala verga
    useEffect(() => {
        //Datos del proyecto
        GetByIdApi('proyecto', id).then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setProyecto(dato)
                })
            } else {
                setNotFound(true)
            }
        })

        //Datos de toos los perfules
        GetApi('etapa').then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setEtapas(dato)
                })
            }
        })

        //Para la carga
        setIsLoading(false)

        return () => {
            setProyecto({})
            setEtapas([])
        }
    }, [id, isLoading])

    //Efecto que reacciona a los cambios de los datos principales
    useEffect(() => {
        setReloadData(false)

        //Perfiles del consultor
        GetApi(`etapas_proyecto/lista-etapas/${id}`).then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setEtapasProyecto(dato)
                })
            }
        })

        return () => {
            setEtapasProyecto([])
        }
    }, [id, reloadData])

    //Función para guardar lso datos
    const changeForm = (e) => {
        if (e.target.type === 'number') {
            setInputs({
                ...inputs,
                [e.target.name]: Number(e.target.value)
            })
        } else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            })
        }
    }

    //Función para añadir un perfil
    const create = () => {
        const data = inputs

        if (!minLengthValidation(1, data.idEtapa)) {
            setMessage('Seleccione una etapa.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Carga de los datos
        setIsLoadingUpload(true)

        //Añade los datos
        PostApi('etapas_proyecto', data).then(response => {
            if (response.status === 200) {
                setMessage('Etapa añadida correctamente.')
                setSeverity('success')
                setOpenAlert(true)
                setReloadData(true)
            } else {
                response.result.then(text => {
                    setMessage(text)
                    setSeverity('error')
                    setOpenAlert(true)
                })
            }
        })

        //Detiene carga
        setIsLoadingUpload(false)
    }

    //Función para eliminar elemento
    const deleteElement = (id) => {
        DeleteApi('etapas_proyecto', id)
            .then(response => {
                if (response.status === 200) {
                    response.result.then(text => {
                        setMessage(text)
                        setOpenAlert(true)
                        setSeverity('success')

                        //Inicia la recarga de los datos
                        setReloadData(true)
                    })
                }
            })

        //Para la carga
        setReloadData(false)
    }


    //En lo que carga la info
    if (isLoading) {
        return <LinearProgress variant='indeterminate' />
    }

    //si no fue encontrado
    if (notFound) {
        return <Redirect to='/admin/proyectos' />
    }

    return (
        <div>
            <SnackbarAlert open={openAlert} onClose={() => setOpenAlert(false)} severity={severity}>
                {message}
            </SnackbarAlert>
            <Button color='primary' startIcon={<ArrowBackIosIcon />} onClick={() => props.history.goBack()}>
                Volver
            </Button>
            <Typography variant='h5'>Gestión de etapas del proyecto</Typography>
            <Typography variant='caption'>
                {`${proyecto.nombre}`}
            </Typography>
            <Grid container spacing={2} className={classes.gridContainer}>
                <Grid item lg={6}>
                    <Typography variant='h6'>Etapas</Typography>
                    <List className={classes.listContent}>
                        {
                            etapasProyecto.length > 0 ?
                                etapasProyecto.map((value, index) => (
                                    <ListItem key={index}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <CategoryIcon />
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={value.nombre} />
                                        <ListItemSecondaryAction>
                                            <IconButton onClick={() => deleteElement(value.id)}>
                                                <ClearIcon />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                )) :
                                <ListItem>
                                    <ListItemIcon>
                                        <ClearIcon />
                                    </ListItemIcon>
                                    <ListItemText
                                        primary={'Sin etapas'} />
                                </ListItem>
                        }
                    </List>
                </Grid>
                <Grid item lg={6}>
                    <Typography variant='h6'>Agregar etapa</Typography>
                    <form onChange={changeForm} className={classes.form}>
                        <Grid container spacing={2}>
                            <Grid item lg={12}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id='etapaLabel'>Seleccione una etapa</InputLabel>
                                    <Select
                                        name='idEtapa'
                                        labelId='etapaLabel'
                                        onChange={changeForm}
                                        value={inputs.idEtapa}>
                                        {
                                            etapas.map((value, index) => (
                                                <MenuItem value={value.id} key={index}>
                                                    {`${value.nombre}`}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                    {
                        isLoadingUpload ?
                            <CircularProgress variant='indeterminate' /> :
                            <Button
                                color='primary'
                                variant='contained'
                                startIcon={<AddIcon />}
                                onClick={create}>
                                Añadir etapa
                            </Button>
                    }
                </Grid>
            </Grid>
        </div>
    )
}

export default EtapasProyecto
