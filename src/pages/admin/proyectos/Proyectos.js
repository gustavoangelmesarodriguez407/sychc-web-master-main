import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import {
    Typography, LinearProgress, IconButton, Button,
    Tooltip
} from '@material-ui/core'
import { DataGrid, GridToolbar } from '@material-ui/data-grid'
import { useStyles } from '../useStyles'

/**Components */
import Crear from './CrearProyecto'
import Notification from '../../../components/Notification'
import SnackbarAlert from '../../../components/SnackbarAlert'

/**API */
import { GetApi, DeleteApi } from '../../../api/Services'

/**Helpers */
import { BASE_PATH, API } from '../../../helpers/ApiConfig'

/**Icons */
import VisibilityIcon from '@material-ui/icons/Visibility'
import EditIcon from '@material-ui/icons/Edit'
import ClearIcon from '@material-ui/icons/Clear'
import AddIcon from '@material-ui/icons/Add'
import TimelineIcon from '@material-ui/icons/Timeline'
import CategoryIcon from '@material-ui/icons/Category'

function Proyectos() {
    const classes = useStyles()
    const controller = 'proyecto'

    const [data, setData] = useState([])
    const [selectedId, setSelectedId] = useState('')
    const [isLoading, setIsLoading] = useState(true)
    const [reload, setReload] = useState(false)
    const [openCreateForm, setOpenCreateForm] = useState(false)
    const [openNotification, setOpenNotification] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Definición de columnas
    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            flex: 1,
            hide: true
        },
        {
            field: 'nombre',
            headerName: 'Nombre',
            flex: 2,
        },
        {
            field: 'descripcion',
            headerName: 'Descripción',
            flex: 1,
            hide: true
        },
        {
            field: 'idCliente',
            headerName: 'ID del cliente',
            flex: 1,
            hide: true
        },
        {
            field: 'fechaInicio',
            headerName: 'Fecha Inicio',
            type: 'date',
            flex: 1,
            hide: true
        },
        {
            field: 'fechaFinal',
            headerName: 'Fecha Término',
            type: 'date',
            flex: 1,
            hide: true
        },
        {
            field: 'horasEstimadas',
            headerName: 'Horas estimadas',
            type: 'number',
            flex: 1,
            hide: true
        },
        {
            field: 'horasTotales',
            headerName: 'Horas totales',
            type: 'number',
            flex: 1,
            hide: true
        },
        {
            field: 'etapaActual',
            headerName: 'Etapa actual',
            flex: 1,
            hide: true
        },
        {
            field: 'estadoActual',
            headerName: 'Estado actual',
            flex: 1,
            hide: false
        },
        {
            field: 'moneda',
            headerName: 'Moneda',
            flex: 1,
            hide: true
        },
        {
            field: 'precioEstimado',
            headerName: 'Precio estimado',
            type: 'number',
            flex: 1,
            valueFormatter: params => {
                const value = Number(params.value).toFixed(2)
                return value
            },
            hide: true
        },
        {
            field: 'precioReal',
            headerName: 'Precio real',
            type: 'number',
            flex: 1,
            valueFormatter: params => {
                const value = Number(params.value).toFixed(2)
                return value
            },
            hide: true
        },
        {
            field: 'costoEstimado',
            headerName: 'Costo estimado',
            type: 'number',
            flex: 1,
            valueFormatter: params => {
                const value = Number(params.value).toFixed(2)
                return value
            },
            hide: true
        },
        {
            field: 'costoReal',
            headerName: 'Costo real',
            type: 'number',
            flex: 1,
            valueFormatter: params => {
                const value = Number(params.value).toFixed(2)
                return value
            },
            hide: true
        },
        {
            field: 'imagenURL',
            headerName: 'Imagen',
            hide: false,
            flex: 1,
            renderCell: params => (
                <div className={classes.actionContainer}>
                    <img
                        src={`${BASE_PATH}/${API}/archivo/${params.value}`}
                        style={{ width: 75, height: 50, borderRadius: 6 }}
                        alt={params.value}
                    />
                </div>
            )
        },
        {
            field: 'modificadoPor',
            headerName: 'Modificado por',
            flex: 1,
            hide: true
        },
        {
            field: 'ultimaModificacion',
            headerName: 'Última modificación',
            type: 'datetime',
            flex: 1,
            hide: true
        },
        {
            field: 'acciones',
            headerName: 'Acciones',
            flex: 1,
            renderCell: params => (
                <div className={classes.actionContainer}>
                    <Link to={`/admin/proyectos/panel/${params.getValue(params.id, 'id')}`}>
                        <Tooltip title='Ver en el panel'>
                            <IconButton
                                className={classes.verButton}
                                size='small'>
                                <VisibilityIcon />
                            </IconButton>
                        </Tooltip>
                    </Link>
                    <Link to={`/admin/proyectos/${params.getValue(params.id, 'id')}`}>
                        <Tooltip title='Editar'>
                            <IconButton
                                className={classes.editButton}
                                size='small'>
                                <EditIcon />
                            </IconButton>
                        </Tooltip>
                    </Link>
                    <Link to={`/admin/etapas-proyecto/${params.getValue(params.id, 'id')}`}>
                        <Tooltip title='Gestionar etapas'>
                            <IconButton
                                className={classes.etapasButton}
                                size='small'>
                                <CategoryIcon />
                            </IconButton>
                        </Tooltip>
                    </Link>
                    <Link to={`/admin/consultores-proyecto/${params.getValue(params.id, 'id')}`}>
                        <Tooltip title='Gestionar consultores'>
                            <IconButton
                                className={classes.consultoresButton}
                                size='small'>
                                <TimelineIcon />
                            </IconButton>
                        </Tooltip>
                    </Link>
                    <Tooltip title='Eliminar'>
                        <IconButton
                            className={classes.deleteButton}
                            size='small'
                            onClick={() => {
                                setSelectedId(params.getValue(params.id, 'id'))
                                setOpenNotification(true)
                            }}>
                            <ClearIcon />
                        </IconButton>
                    </Tooltip>
                </div>
            )
        }
    ]

    //Obtiene todos los usuarios
    useEffect(() => {
        GetApi(controller).then(response => {
            if (response.status === 200) {
                response.result.then(rows => {
                    let list = []
                    rows.map((values, index) => {
                        list.push({
                            ...values,
                            fechaInicio: new Date(values.fechaInicio),
                            fechaFinal: new Date(values.fechaFinal),
                            ultimaModificacion: new Date(values.ultimaModificacion)
                        })
                        return null
                    })
                    setData(list)
                    setIsLoading(false)
                    setReload(false)
                })
            }
        })

        return () => setData([])
    }, [reload])

    //Elimina registro
    const eliminar = (id) => {
        //Cierra notificación
        setOpenNotification(false)

        //Elimina el usuario
        DeleteApi(controller, id).then(response => {
            switch (response.status) {
                case 200:
                    response.result.then(text => {
                        setMessage(text)
                        setSeverity('success')
                        setOpenAlert(true)
                    })
                    break;

                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;

                default:
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })

        //Recarga la tabla
        setReload(true)
    }

    return (
        <div>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Crear
                open={openCreateForm}
                handleClose={() => setOpenCreateForm(false)}
                handleReload={() => setReload(true)} />
            <Notification
                title='Eliminar proyecto'
                open={openNotification}
                onClose={() => setOpenNotification(false)}
                onAccept={() => eliminar(selectedId)}>
                <Typography>
                    ¿Está seguro de eliminar este proyecto con el ID <span style={{ fontWeight: 'bold' }}>{selectedId}</span>?
                    Esta acción no se puede deshacer.
                </Typography>
                <br />
                <Typography>
                    NOTA: Algunos proyectos no pueden ser eliminados ya que están
                    relacionados con alguna actividad, si desea deshabilitarlos,
                    cambie el valor de estado actual a "Cancelado".
                </Typography>
            </Notification>
            <Typography variant='h4' gutterBottom>Proyectos</Typography>
            <Typography>
                Se muestra una lista de todos los proyectos del sistema. después de crear un proyecto,
                se necesitará de la asignación de etapas y consultores.
            </Typography>
            <Button
                variant='contained'
                color='primary'
                startIcon={<AddIcon />}
                className={classes.createBtn}
                onClick={() => setOpenCreateForm(true)}>
                Crear Proyecto
            </Button>
            <div style={{ height: '500px' }}>
                <DataGrid
                    columns={columns}
                    rows={data}
                    disableSelectionOnClick
                    loading={isLoading}
                    pageSize={25}
                    components={{
                        LoadingOverlay: LinearProgress,
                        Toolbar: GridToolbar
                    }} />
            </div>
        </div>
    )
}

export default Proyectos
