import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import {
    Typography, Grid, List, IconButton, ListItemSecondaryAction,
    ListItemText, ListItem, ListItemAvatar, Avatar, ListItemIcon,
    FormControl, InputLabel, Select, MenuItem, TextField, Button,
    LinearProgress, CircularProgress
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**APis */
import { GetByIdApi, GetApi, PostApi, DeleteApi } from '../../../api/Services'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'

/**Icon */
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd'
import ClearIcon from '@material-ui/icons/Clear'
import AddIcon from '@material-ui/icons/Add'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'

function PerfilesConsultor(props) {
    const classes = useStyles()
    const { match: { params: { id } } } = props
    const [consultor, setConsultor] = useState({})
    const [perfiles, setPerfiles] = useState([])
    const [perfilesConsultor, setPerfilesConsultor] = useState([])
    const [inputs, setInputs] = useState({
        idConsultor: id,
        idPerfil: '',
        aniosExperiencia: 1
    })
    const [isLoading, setIsLoading] = useState(true)
    const [isLoadingUpload, setIsLoadingUpload] = useState(false)
    const [reloadData, setReloadData] = useState(false)
    const [notFound, setNotFound] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae todos los datos ala verga
    useEffect(() => {
        //Datos del consultor
        GetByIdApi('consultor', id).then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setConsultor(dato)
                })
            } else {
                setNotFound(true)
            }
        })

        //Datos de toos los perfules
        GetApi('perfil').then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setPerfiles(dato)
                })
            }
        })

        //Para la carga
        setIsLoading(false)

        return () => {
            setConsultor({})
            setPerfiles([])
        }
    }, [id, isLoading])

    //Efecto que reacciona a los cambios de los datos principales
    useEffect(() => {
        setReloadData(false)

        //Perfiles del consultor
        GetApi(`perfiles_consultor/lista-perfiles/${id}`).then(response => {
            if (response.status === 200) {
                response.result.then(dato => {
                    setPerfilesConsultor(dato)
                    setIsLoading(false)
                })
            }
        })

        return () => {
            setPerfilesConsultor([])
        }
    }, [id, reloadData])

    //Función para guardar lso datos
    const changeForm = (e) => {
        if (e.target.type === 'number') {
            setInputs({
                ...inputs,
                [e.target.name]: Number(e.target.value)
            })
        } else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            })
        }
    }

    //Función para añadir un perfil
    const create = () => {
        const data = inputs

        if (!minLengthValidation(1, data.idPerfil)) {
            setMessage('Seleccione un perfil.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (data.aniosExperiencia <= 0) {
            setMessage('Los años de experiencia deben ser mayores a 0.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Carga de los datos
        setIsLoadingUpload(true)

        //Añade los datos
        PostApi('perfiles_consultor', data).then(response => {
            if (response.status === 200) {
                setMessage('Perfil añadido correctamente.')
                setSeverity('success')
                setOpenAlert(true)

                //Recarfa los daotos
                setReloadData(true)
            } else {
                response.result.then(text => {
                    setMessage(text)
                    setSeverity('error')
                    setOpenAlert(true)
                })
            }
        })

        //Detiene carga
        setIsLoadingUpload(false)
    }

    //Función para eliminar elemento
    const deleteElement = (id) => {
        DeleteApi('perfiles_consultor', id)
            .then(response => {
                if (response.status === 200) {
                    response.result.then(text => {
                        setMessage(text)
                        setOpenAlert(true)
                        setSeverity('success')

                        //Inicia la recarga de los datos
                        setReloadData(true)
                    })
                }
            })

        //Para la carga
        setReloadData(false)
    }


    //En lo que carga la info
    if (isLoading) {
        return <LinearProgress variant='indeterminate' />
    }

    //si no fue encontrado
    if (notFound) {
        return <Redirect to='/admin/consultores' />
    }

    return (
        <div>
            <SnackbarAlert open={openAlert} onClose={() => setOpenAlert(false)} severity={severity}>
                {message}
            </SnackbarAlert>
            <Button color='primary' startIcon={<ArrowBackIosIcon />} onClick={() => props.history.goBack()}>
                Volver
            </Button>
            <Typography variant='h5'>Gestión de perfiles de consultor</Typography>
            <Typography variant='caption'>
                {`${consultor.nombres} ${consultor.apellidos}`}
            </Typography>
            <Grid container spacing={2} className={classes.gridContainer}>
                <Grid item lg={6}>
                    <Typography variant='h6'>Perfiles</Typography>
                    <List className={classes.listContent}>
                        {
                            perfilesConsultor.length > 0 ?
                                perfilesConsultor.map((value, index) => (
                                    <ListItem key={index}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AssignmentIndIcon />
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={value.nombre}
                                            secondary={`${value.nivel} - ${value.aniosExperiencia} Años de experiencia.`} />
                                        <ListItemSecondaryAction>
                                            <IconButton onClick={() => deleteElement(value.id)}>
                                                <ClearIcon />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                )) :
                                <ListItem>
                                    <ListItemIcon>
                                        <ClearIcon />
                                    </ListItemIcon>
                                    <ListItemText
                                        primary={'Sin perfiles'} />
                                </ListItem>
                        }
                    </List>
                </Grid>
                <Grid item lg={6}>
                    <Typography variant='h6'>Agregar perfil</Typography>
                    <form onChange={changeForm} className={classes.form}>
                        <Grid container spacing={2}>
                            <Grid item lg={6}>
                                <FormControl variant="filled" className={classes.formControl}>
                                    <InputLabel id='perfilLabel'>Seleccione un perfil</InputLabel>
                                    <Select
                                        name='idPerfil'
                                        labelId='perfilLabel'
                                        onChange={changeForm}
                                        value={inputs.idPerfil}>
                                        {
                                            perfiles.map((value, index) => (
                                                <MenuItem value={value.id} key={index}>
                                                    {`${value.nombre} - ${value.nivel}`}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item lg={6}>
                                <TextField
                                    name='aniosExperiencia'
                                    type='number'
                                    label='Años de experiencia'
                                    variant='filled'
                                    defaultValue={inputs.aniosExperiencia}
                                    className={classes.formControl} />
                            </Grid>
                        </Grid>
                    </form>
                    {
                        isLoadingUpload ?
                            <CircularProgress variant='indeterminate' /> :
                            <Button
                                color='primary'
                                variant='contained'
                                startIcon={<AddIcon />}
                                onClick={create}>
                                Añadir perfil
                            </Button>
                    }
                </Grid>
            </Grid>
        </div>
    )
}

export default PerfilesConsultor
