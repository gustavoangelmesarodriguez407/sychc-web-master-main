import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles(theme => ({
    actionContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%'
    },
    createBtn: {
        marginBlock: 20,
        paddingInline: 50
    },
    verButton: {
        backgroundColor: theme.palette.success.main,
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: theme.palette.success.dark
        }
    },
    editButton: {
        backgroundColor: theme.palette.info.main,
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: theme.palette.info.dark
        }
    },
    deleteButton: {
        backgroundColor: theme.palette.error.main,
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: theme.palette.error.dark
        }
    },
    consultoresButton: {
        backgroundColor: theme.palette.primary.main,
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: theme.palette.primary.dark
        }
    },
    etapasButton: {
        backgroundColor: '#DE8C35',
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: '#BA6F1E'
        }
    },
    perfilesButton: {
        backgroundColor: '#DE3552',
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: '#BA1E38'
        }
    },
    usuariosButton: {
        backgroundColor: '#DE358C',
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: '#BA1E6F'
        }
    },
    formControl: {
        width: '100%'
    },
    form: {
        marginBlock: 20
    },
    link: {
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'none'
        }
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        marginRight: theme.spacing(2)
    },
    listContent: {
        maxWidth: 400
    },
    gridContainer: {
        marginBlock: theme.spacing(3)
    },
    archivoBtn: {
        width: '100%',
        borderRadius: 5,
        padding: theme.spacing(2),
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        '&:hover': {
            backgroundColor: theme.palette.primary.dark,
            cursor: 'pointer'
        }
    },
    image: {
        width: '100%',
        height: 140,
        objectFit: 'cover',
        borderRadius: 6,
        marginTop: theme.spacing(2)
    },
    filtrosContainer: {
        marginBlock: theme.spacing(2)
    },
    divider: {
        marginBlock: theme.spacing(2.5)
    }
}))