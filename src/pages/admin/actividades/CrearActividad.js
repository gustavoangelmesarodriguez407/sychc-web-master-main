import React, { useState, useEffect, Fragment } from 'react'
import {
    Dialog, DialogTitle, DialogContent,
    DialogActions, DialogContentText, TextField,
    Button, Grid, FormControl, InputLabel, Select,
    FormControlLabel, Switch,
    MenuItem, CircularProgress, Typography
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import SnackbarAlert from '../../../components/SnackbarAlert'

/**Helpers */
import { minLengthValidation } from '../../../helpers/Validations'
import DateFormatter from '../../../helpers/DateFormatter'

/**API */
import { GetByIdApi, PostApi, UploadFileApi, GetApi } from '../../../api/Services'

function CrearActividad(props) {
    const classes = useStyles()
    const { open, handleClose, handleReload, idProyecto, etapas,
        nombreProyecto } = props
    const session_id = window.localStorage.getItem('SESSION_ID')
    const [categorias, setCategorias] = useState([])
    const [inputs, setInputs] = useState({
        idProyecto: idProyecto,
        idConsultor: '',
        descripcion: '',
        fecha: null,
        fechaInicio: DateFormatter(new Date().setDate(new Date().getDate() - 1)),
        fechaFinal: DateFormatter(new Date()),
        etapa: '',
        archivoURL: '',
        horasTrabajadas: 1,
        horasFacturables: false,
        categoria: '',
    })
    const [archivo, setArchivo] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Efecto que trae el id de consultor
    useEffect(() => {
        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setInputs({ ...inputs, idConsultor: ses.idUsuario })
                })
            }
        })
        GetApi(`categoria/`).then(response => {
            if (response.status === 200) {
                response.result.then(etps => {
                    setCategorias(etps)
                })
            } else {
                setCategorias([])
            }
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [session_id])

    //Función para guardar los datos en el estado
    const changeForm = (e) => {
        if (e.target.type === 'number') {
            setInputs({
                ...inputs,
                [e.target.name]: Number(e.target.value)
            })
        } else if (e.target.type === 'checkbox') {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.checked
            })
        } else if (e.target.type === 'file') {
            setArchivo(e.target.files[0])
        } else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            })
        }
    }

    //Función para crear usuario
    const validate = () => {
        const data = inputs

        if (!minLengthValidation(1, data.descripcion)) {
            setMessage('Ingrese una descripción.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        if (!minLengthValidation(1, data.etapa)) {
            setMessage('Seleccione una etapa.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }
        if (!minLengthValidation(1, data.categoria)) {
            setMessage('Seleccione una categoría.')
            setSeverity('error')
            setOpenAlert(true)
            return
        }
        if (data.horasTrabajadas <= 0 || data.horasTrabajadas > 168) {
            setMessage('Las horas trabajadas deben ser entre 1 y 168 horas (7 días).')
            setSeverity('error')
            setOpenAlert(true)
            return
        }

        //Inicia la carga
        setIsLoading(true)

        /**SUBIR ARCHIVO */
        //Si hay archivo seleccionado, lo intenta subir
        if (archivo) {
            UploadFileApi(archivo).then(response => {
                if (response.status === 200) {
                    response.result.then(fileName => {
                        data.archivoURL = fileName

                        //Guarda la actividad con el archivo
                        createActivity(data)
                    })
                } else {
                    setMessage('Error al subir el archivo, intente de nuevo.')
                    setSeverity('error')
                    setOpenAlert(true)
                    return
                }
            })
        } else {
            //Guarda la actividad sin el archivo
            createActivity(data)
        }
    }

    const createActivity = (data) => {
        //Guarda los datos
        PostApi('actividad', data).then(response => {
            switch (response.status) {
                case 200:
                    setMessage('Actividad creada correctamente.')
                    setSeverity('success')
                    setOpenAlert(true)

                    //Cierra modal
                    handleClose()
                    handleReload()

                    //Limpia los estados
                    setInputs({
                        ...inputs,
                        idProyecto: idProyecto,
                        idConsultor: '',
                        descripcion: '',
                        fecha: null,
                        fechaInicio: DateFormatter(new Date()),
                        fechaFinal: DateFormatter(new Date().setDate(new Date().getDate() + 1)),
                        etapa: '',
                        archivoURL: '',
                        horasTrabajadas: 1,
                        horasFacturables: true,
                        categoria: '',
                    })

                    setIsLoading(false)
                    break;
                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setIsLoading(false)
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;
                default:
                    setIsLoading(false)
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })
    }

    return (
        <Fragment>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Dialog
                open={open}
                onClose={handleClose}>
                <DialogTitle>Crear Actividad</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Rellene todos los campos para crear una actividad
                        sobre la consultoría de este proyecto.
                    </DialogContentText>
                    <form onChange={changeForm}>
                        <Grid container spacing={2}>
                            <Grid item lg={12}>
                                <Typography>{nombreProyecto}</Typography>
                            </Grid>
                            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                                <TextField
                                    name='descripcion'
                                    variant='filled'
                                    multiline
                                    rows={6}
                                    label='Descripción de la actividad'
                                    style={{ width: '100%' }} />
                            </Grid>
                            <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
                                <Typography variant='caption'>Fecha de inicio de actividad</Typography>
                                <TextField
                                    type='date'
                                    size='small'
                                    name='fechaInicio'
                                    value={inputs.fechaInicio}
                                    style={{ width: '100%' }} />
                            </Grid>
                            <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
                                <Typography variant='caption'>Fecha final de actividad</Typography>
                                <TextField
                                    type='date'
                                    size='small'
                                    name='fechaFinal'
                                    value={inputs.fechaFinal}
                                    style={{ width: '100%' }} />
                            </Grid>
                            <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
                                <FormControl variant="filled" style={{ width: '100%' }}>
                                    <InputLabel id="etapaLabel">Etapa</InputLabel>
                                    <Select
                                        name='etapa'
                                        labelId="etapaLabel"
                                        onChange={changeForm}
                                        value={inputs.etapa}>
                                        {
                                            etapas.map((values, index) => (
                                                <MenuItem key={index} value={values.idEtapa}>
                                                    {values.nombre}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
                                <FormControl variant="filled" style={{ width: '100%' }}>
                                    <InputLabel id="categoriasLabel">categorias</InputLabel>
                                    <Select
                                        name='categoria'
                                        labelId="categoriasLabel"
                                        onChange={changeForm}
                                        value={inputs.categoria}>
                                        {
                                            categorias.map((values, index) => (
                                                <MenuItem key={index} value={values.id}>
                                                    {values.nombre}
                                                </MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
                                <TextField
                                    name='horasTrabajadas'
                                    variant='filled'
                                    type='number'
                                    label='Horas trabajadas'
                                    defaultValue={inputs.horasTrabajadas}
                                    style={{ width: '100%' }} />
                            </Grid>
                            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                                <FormControlLabel
                                    control={
                                        <Switch
                                            checked={inputs.horasFacturables}
                                            onChange={changeForm}
                                            name="horasFacturables"
                                            color="primary"
                                        />
                                    }
                                    label="Horas Facturables"
                                />
                            </Grid>
                            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                                <FormControl>
                                    <label
                                        htmlFor='actividadArchivo'
                                        className={classes.archivoBtn}>
                                        {archivo ? archivo.name : 'Elegir archivo'}
                                    </label>
                                    <input
                                        id='actividadArchivo'
                                        type='file'
                                        multiple={false}
                                        style={{ opacity: 0 }} />
                                </FormControl>
                            </Grid>
                        </Grid>
                    </form>
                </DialogContent>
                <DialogActions>
                    {
                        isLoading ?
                            <CircularProgress variant='indeterminate' /> :
                            <Fragment>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={validate}>
                                    Aceptar
                                </Button>
                                <Button
                                    variant='contained'
                                    color='secondary'
                                    onClick={handleClose}>
                                    Cancelar
                                </Button>
                            </Fragment>
                    }
                </DialogActions>
            </Dialog>
        </Fragment>
    )
}

export default CrearActividad
