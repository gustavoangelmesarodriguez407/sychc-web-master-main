import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Typography, LinearProgress, IconButton, /* Button */ } from '@material-ui/core'
import { DataGrid, GridToolbar } from '@material-ui/data-grid'
import { useStyles } from '../useStyles'

/**Components */
import Notification from '../../../components/Notification'
import SnackbarAlert from '../../../components/SnackbarAlert'

/**API */
import { GetApi, DeleteApi } from '../../../api/Services'

/**Icons */
import EditIcon from '@material-ui/icons/Edit'
import ClearIcon from '@material-ui/icons/Clear'
//import AddIcon from '@material-ui/icons/Add'

function Actividad() {
    const classes = useStyles()
    const controller = 'actividad'

    const [data, setData] = useState([])
    const [selectedId, setSelectedId] = useState('')
    const [isLoading, setIsLoading] = useState(true)
    const [reload, setReload] = useState(false)
    //const [openCreateForm, setOpenCreateForm] = useState(false)
    const [openNotification, setOpenNotification] = useState(false)
    const [openAlert, setOpenAlert] = useState(false)
    const [message, setMessage] = useState('')
    const [severity, setSeverity] = useState('error')

    //Definición de columnas
    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            width: 100,
            hide: true
        },
        {
            field: 'idProyecto',
            headerName: 'ID Proyecto',
            width: 180,
            hide: true
        },
        {
            field: 'idConsultor',
            headerName: 'ID Consultor',
            width: 180,
            hide: true
        },
        {
            field: 'fecha',
            headerName: 'Realización',
            type: 'date',
            width: 140
        },
        {
            field: 'fechaInicio',
            headerName: 'Inicio',
            type: 'date',
            width: 140
        },
        {
            field: 'fechaFinal',
            headerName: 'Término',
            type: 'date',
            width: 140
        },
        {
            field: 'descripcion',
            headerName: 'Descripción',
            width: 180
        },
        {
            field: 'etapa',
            headerName: 'ID Etapa',
            width: 140,
            hide: true
        },
        {
            field: 'horasTrabajadas',
            headerName: 'Horas trabajadas',
            type: 'number',
            width: 140
        },
        {
            field: 'horasFacturables',
            headerName: 'Horas facturables',
            type: 'boolean',
            width: 180
        },
        {
            field: 'categoriaNombre',
            headerName: 'Categoría',
            width: 180
        },
        {
            field: 'acciones',
            headerName: 'Acciones',
            width: 150,
            renderCell: params => (
                <div className={classes.actionContainer}>
                    <Link to={`/admin/actividades/${params.getValue(params.id, 'id')}`}>
                        <IconButton
                            className={classes.editButton}
                            size='small'>
                            <EditIcon />
                        </IconButton>
                    </Link>
                    <IconButton
                        className={classes.deleteButton}
                        size='small'
                        onClick={() => {
                            setSelectedId(params.getValue(params.id, 'id'))
                            setOpenNotification(true)
                        }}>
                        <ClearIcon />
                    </IconButton>
                </div>
            )
        }
    ]

    //Obtiene todos los usuarios
    useEffect(() => {
        GetApi(controller).then(response => {
            if (response.status === 200) {
                response.result.then(rows => {
                    let list = []
                    rows.map((values, index) => {
                        list.push({  
                            ...values,
                            fecha: new Date(values.fecha),
                            fechaInicio: new Date(values.fechaInicio),
                            fechaFinal: new Date(values.fechaFinal)
                        })
                        return null
                    })
                    setData(list)
                    setIsLoading(false)
                    setReload(false)
                })
            }
        })

        return () => setData([])
    }, [reload])

    //Elimina registro
    const eliminar = (id) => {
        //Cierra notificación
        setOpenNotification(false)

        //Elimina el usuario
        DeleteApi(controller, id).then(response => {
            switch (response.status) {
                case 200:
                    response.result.then(text => {
                        setMessage(text)
                        setSeverity('success')
                        setOpenAlert(true)
                    })
                    break;

                case 400:
                case 404:
                case 500:
                    response.result.then(text => {
                        setMessage(text)
                        setSeverity('error')
                        setOpenAlert(true)
                    })
                    break;

                default:
                    setMessage('Error del servidor.')
                    setSeverity('error')
                    setOpenAlert(true)
                    break;
            }
        })

        //Recarga la tabla
        setReload(true)
    }

    return (
        <div>
            <SnackbarAlert
                open={openAlert}
                onClose={() => setOpenAlert(false)}
                severity={severity}>
                {message}
            </SnackbarAlert>
            <Notification
                title='Eliminar usuario'
                open={openNotification}
                onClose={() => setOpenNotification(false)}
                onAccept={() => eliminar(selectedId)}>
                <Typography>
                    ¿Está seguro de eliminar esta actividad con el ID <span style={{ fontWeight: 'bold' }}>{selectedId}</span>?
                    Esta acción no se puede deshacer.
                </Typography>
                <br />
                <Typography>
                    NOTA: Las actividades se usan para calcular los precios reales de los
                    proyectos, si elimina esta actividad, puede que se pierdan elementos
                    al concluir un proyecto.
                </Typography>
            </Notification>
            <Typography variant='h4' gutterBottom>Actividades</Typography>
            <Typography>
                Registro de todas las actividades relacionadas a un proyecto y realizadas por consultores
                a lo largo del proceso de consultoría.
            </Typography>
            {/* <Button
                variant='contained'
                color='primary'
                startIcon={<AddIcon />}
                className={classes.createBtn}
                onClick={() => setOpenCreateForm(true)}>
                Crear Actividad
            </Button> */}
            <div style={{ height: '500px', marginTop: 32 }}>
                <DataGrid
                    columns={columns}
                    rows={data}
                    disableSelectionOnClick
                    loading={isLoading}
                    pageSize={25}
                    components={{
                        LoadingOverlay: LinearProgress,
                        Toolbar: GridToolbar
                    }} />
            </div>
        </div>
    )
}

export default Actividad
