import React, { useState, useEffect } from 'react'
import {
    Typography, Button, LinearProgress, Divider,
    Link, Grid
} from '@material-ui/core'
import theme from '../../../styles/Theme'
import { useStyles } from '../useStyles'

/**GHelpers */
import DateFormatter, { datetimeISOFormatter } from '../../../helpers/DateFormatter'
import { BASE_PATH, API } from '../../../helpers/ApiConfig'

/**API */
import { GetByIdApi } from '../../../api/Services'

/**Icons */
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import EditIcon from '@material-ui/icons/Edit'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import EventIcon from '@material-ui/icons/Event';
import EventAvailableIcon from '@material-ui/icons/EventAvailable'
import CategoryIcon from '@material-ui/icons/Category'
import ScheduleIcon from '@material-ui/icons/Schedule'
import MoneyOffIcon from '@material-ui/icons/MoneyOff';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import AttachFileIcon from '@material-ui/icons/AttachFile'
import ModificarActividad from './ModificarActividad'
import BookIcon from '@material-ui/icons/Book'

function VerActividad(props) {
    const { match: { params: { id } } } = props
    const classes = useStyles()
    const session_id = window.localStorage.getItem('SESSION_ID')

    const [idUsuario, setIdUsuario] = useState(null)
    const [tipoUsuario, setTipoUsuario] = useState(null)
    const [actividad, setActividad] = useState({})
    const [proyecto, setProyecto] = useState({})
    const [etapa, setEtapa] = useState({})
    const [categoria, setCategoria] = useState({})
    const [consultor, setConsultor] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    const [notFound, setNotFound] = useState(false)
    const [reload, setReload] = useState(false)
    const [openUpdateForm, setOpenUpdateForm] = useState(false)

    console.log(actividad);
    //Efecto que trae la info de todo la actividad, proyecto y etapa
    useEffect(() => {

        //Trae info de la sesión e id de usuario para poder editar
        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setIdUsuario(ses.idUsuario)
                    setTipoUsuario(ses.tipoUsuario)
                })
            } else {
                setNotFound(true)
            }
        })

        //Trae info de la actividad
        GetByIdApi('actividad', id).then(responseAct => {
            if (responseAct.status === 200) {
                responseAct.result.then(act => {
                    setActividad(act)

                    //Busca el proyecto
                    GetByIdApi('proyecto', act.idProyecto).then(responseProy => {
                        if (responseProy.status === 200) {
                            responseProy.result.then(proy => {
                                setProyecto(proy)
                            })
                        }
                    })

                    //Busca la etapa
                    GetByIdApi('etapa', act.etapa).then(responseEtp => {
                        if (responseEtp.status === 200) {
                            responseEtp.result.then(etp => {
                                setEtapa(etp)
                            })
                        }
                    })
                    //Busca la categoria
                    GetByIdApi('categoria', act.categoria).then(responseEtp => {
                        if (responseEtp.status === 200) {
                            responseEtp.result.then(etp => {
                                setCategoria(etp)
                            })
                        }
                    })

                    //Busca el consultor
                    GetByIdApi('consultor', act.idConsultor).then(responseCons => {
                        if (responseCons.status === 200) {
                            responseCons.result.then(cons => {
                                setConsultor(cons)
                            })
                        }
                    })

                    //Para la carga
                    setIsLoading(false)
                })
            } else {
                setNotFound(true)
            }
        })

        //Para la recarga
        setReload(false)

        return () => {
            setActividad([])
            setProyecto([])
            setEtapa([])
            setConsultor([])
            setCategoria([])
        }
    }, [id, session_id, reload])

    //Si no encunetra la actividad
    if (notFound) {
        props.history.goBack()
    }

    //Si no encunetra la actividad
    if (isLoading) {
        return <LinearProgress variant='indeterminate' />
    }

    return (
        <div>
            <ModificarActividad
                open={openUpdateForm}
                handleClose={() => setOpenUpdateForm(false)}
                handleReload={() => setReload(true)}
                idActividad={id} />
            <Button color='primary' startIcon={<ArrowBackIosIcon />} onClick={() => props.history.goBack()}>
                Volver
            </Button>
            <Typography variant='h4'>Actividad</Typography>
            <Typography variant='caption'>
                {proyecto.nombre}
            </Typography>
            <Grid container spacing={2}>
                <Grid item xl={8} lg={8} md={8} sm={12} xs={12}>
                    <Divider className={classes.divider} />

                    <div className={classes.infoContainer}>
                        <Typography variant='h6' gutterBottom>
                            Descripción de la actividad
                        </Typography>
                        <Typography variant='body1'>
                            {actividad.descripcion}
                        </Typography>
                    </div>
                </Grid>
                <Grid item xl={4} lg={4} md={4} sm={12} xs={12}>
                    <div className={classes.infoLabel}>
                        <EditIcon className={classes.infoIcon} />
                        <Typography>
                            <span style={{ fontWeight: 'bold' }}> Elaborado por: </span>
                            {`${consultor.nombres} ${consultor.apellidos}`}
                        </Typography>
                    </div>
                    <div className={classes.infoLabel}>
                        <EventIcon className={classes.infoIcon} />
                        <Typography>
                            <span style={{ fontWeight: 'bold' }}> Fecha de creación: </span>
                            {datetimeISOFormatter(actividad.fecha)}
                        </Typography>
                    </div>
                    <div className={classes.infoLabel}>
                        <CalendarTodayIcon className={classes.infoIcon} />
                        <Typography>
                            <span style={{ fontWeight: 'bold' }}> Fecha de inicio: </span>
                            {DateFormatter(actividad.fechaInicio)}
                        </Typography>
                    </div>
                    <div className={classes.infoLabel}>
                        <EventAvailableIcon className={classes.infoIcon} />
                        <Typography>
                            <span style={{ fontWeight: 'bold' }}> Fecha de término: </span>
                            {DateFormatter(actividad.fechaFinal)}
                        </Typography>
                    </div>
                    <div className={classes.infoLabel}>
                        <CategoryIcon className={classes.infoIcon} />
                        <Typography>
                            <span style={{ fontWeight: 'bold' }}> Etapa de elaboración: </span>
                            {etapa.nombre}
                        </Typography>
                    </div>
                    <div className={classes.infoLabel}>
                        <BookIcon className={classes.infoIcon} />
                        <Typography>
                            <span style={{ fontWeight: 'bold' }}> Categoría: </span>
                            {categoria.nombre}
                        </Typography>
                    </div>
                    <div className={classes.infoLabel}>
                        <ScheduleIcon className={classes.infoIcon} />
                        <Typography>
                            <span style={{ fontWeight: 'bold' }}> Horas trabajadas: </span>
                            {actividad.horasTrabajadas}
                        </Typography>
                    </div>
                    <div className={classes.infoLabel}>
                        {
                            actividad.horasFacturables ?
                                <AttachMoneyIcon style={{ marginRight: theme.spacing(1), color: theme.palette.success.main }} /> :
                                <MoneyOffIcon style={{ marginRight: theme.spacing(1), color: theme.palette.error.main }} />
                        }
                        <Typography>
                            {actividad.horasFacturables ? 'Horas facturables' : 'Horas no facturables'}
                        </Typography>
                    </div>
                    {
                        actividad.archivoURL && actividad.archivoURL.length > 0 && (
                            <div className={classes.infoLabel}>
                                <AttachFileIcon className={classes.infoIcon} />
                                <Typography>
                                    <span style={{ fontWeight: 'bold' }}> Archivo adjunto: </span>
                                    <Link target='_blank' href={`${BASE_PATH}/${API}/archivo/${actividad.archivoURL}`}>Ver archivo</Link>
                                </Typography>
                            </div>
                        )
                    }
                    {
                        (actividad.idConsultor === idUsuario ||
                            tipoUsuario === 'admin' || tipoUsuario === 'gerente') && (
                            <div className={classes.infoLabel}>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    startIcon={<EditIcon />}
                                    className={classes.editarBtn}
                                    onClick={() => setOpenUpdateForm(true)}>
                                    Editar actividad
                                </Button>
                            </div>
                        )
                    }
                </Grid>
            </Grid>
        </div>
    )
}

export default VerActividad
