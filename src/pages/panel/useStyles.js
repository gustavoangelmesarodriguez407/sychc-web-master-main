import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles(theme => ({
    projectContainer: {
        width: '100%',
        marginBlock: theme.spacing(2)
    },
    divider: {
        marginBlock: theme.spacing(2)
    },
    paperInfo: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    subtitlePaper: {
        marginBottom: theme.spacing(2)
    },
    infoContainer: {
        marginBottom: theme.spacing(3)
    },
    infoLabel: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginBlock: theme.spacing(1)
    },
    infoIcon: {
        marginRight: theme.spacing(1),
        color: theme.palette.primary.main
    },
    optionBtn: {
        width: '100%',
        marginBlock: theme.spacing(1),
    },
    editarBtn: {
        marginBlock: theme.spacing(1)
    },
    actividadTitle: {
        textAlign: 'center',
        fontSize: '1.3em',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(1)
    },
    actividadList: {
        height: 500,
    },
    link: {
        color: theme.palette.primary.main,
        textDecoration: 'none',
        '&:hover': {
            color: theme.palette.primary.light,
        }
    },
    image: {
        width: '100%',
        height: 140,
        objectFit: 'cover',
        borderRadius: 6,
        marginBottom: theme.spacing(1)
    },

    /**Estilos de resumen */
    grid: {
        marginBlock: theme.spacing(2)
    },
    resumenContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar: {
        width: '9em',
        height: '9em',
        marginBottom: theme.spacing(1)
    },
    imagenCliente: {
        width: '200px',
        height: '200px',
        alignSelf: 'center',
        borderRadius: 5,
        marginBlock: theme.spacing(2)
    },
    actionContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%'
    },
    createBtn: {
        marginBlock: 20,
        paddingInline: 50
    },
    verButton: {
        backgroundColor: theme.palette.success.main,
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: theme.palette.success.dark
        }
    },
    editButton: {
        backgroundColor: theme.palette.info.main,
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: theme.palette.info.dark
        }
    },
    deleteButton: {
        backgroundColor: theme.palette.error.main,
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: theme.palette.error.dark
        }
    },
    consultoresButton: {
        backgroundColor: theme.palette.primary.main,
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: theme.palette.primary.dark
        }
    },
    etapasButton: {
        backgroundColor: '#DE8C35',
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: '#BA6F1E'
        }
    },
    perfilesButton: {
        backgroundColor: '#DE3552',
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: '#BA1E38'
        }
    },
    usuariosButton: {
        backgroundColor: '#DE358C',
        marginRight: 10,
        color: '#FFFFFF',
        '&:hover': {
            backgroundColor: '#BA1E6F'
        }
    },
    formControl: {
        width: '100%'
    },
    form: {
        marginBlock: 20
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        marginRight: theme.spacing(2)
    },
    listContent: {
        maxWidth: 400
    },
    gridContainer: {
        marginBlock: theme.spacing(3)
    },
    archivoBtn: {
        width: '100%',
        borderRadius: 5,
        padding: theme.spacing(2),
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        '&:hover': {
            backgroundColor: theme.palette.primary.dark,
            cursor: 'pointer'
        }
    },
    filtrosContainer: {
        marginBlock: theme.spacing(2)
    },
}))