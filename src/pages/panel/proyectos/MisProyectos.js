import React, { useState, useEffect } from 'react'
import {
    Typography, LinearProgress,
    Grid
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Apis */
import { GetApi, GetByIdApi } from '../../../api/Services'
import TarjetaProyecto from '../../../components/TarjetaProyecto'

function MisProyectos() {
    const classes = useStyles()

    const [proyectos, setProyectos] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const session_id = window.localStorage.getItem('SESSION_ID')

    useEffect(() => {
        //Dependiendo del tipo usuario, busca en proyectos
        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(sesion => {
                    switch (sesion.tipoUsuario) {
                        case 'consultor':
                            GetApi(`consultores_proyecto/lista-proyectos-consultor/${sesion.idUsuario}`).then(response => {
                                if (response.status === 200) {
                                    response.result.then(cp => {
                                        setProyectos(cp)
                                    })
                                } else {
                                    setProyectos(null)
                                }
                            })
                            break;

                        case 'cliente':
                            GetApi(`clientes_usuario/info-cliente/${sesion.idUsuario}`).then(response => {
                                if (response.status === 200) {
                                    response.result.then(cliente => {
                                        GetApi(`proyecto/cliente/${cliente.idCliente}`).then(proys => {
                                            if (proys.status === 200) {
                                                proys.result.then(pys => {
                                                    let proyectosVistas = []

                                                    pys.forEach(element => {
                                                        proyectosVistas.push({
                                                            idProyecto: element.id,
                                                            nombreProyecto: element.nombre,
                                                            estadoActual: element.estadoActual,
                                                            imagenURL: element.imagenURL
                                                        })
                                                    });

                                                    setProyectos(proyectosVistas)
                                                })
                                            } else {
                                                setProyectos(null)
                                            }
                                        })
                                    })
                                } else {
                                    setProyectos(null)
                                }
                            })
                            break;

                        default:
                            break;
                    }
                })
            }
        })

        //Para la carga
        setIsLoading(false)

        return () => {
            setProyectos(null)
        }
    }, [session_id])

    //En lo que cargan los proyectos
    if (isLoading) {
        return <LinearProgress variant='indeterminate' />
    }

    return (
        <div>
            {
                !proyectos ?
                    <div>
                        <Typography variant='h4' gutterBottom>
                            No hay proyectos
                        </Typography>
                        <Typography>
                            Contacte a un administrador o un gerente para que se le asigne uno
                            o más proyectos.
                        </Typography>
                    </div> :
                    <div>
                        <Typography variant='h4' gutterBottom>Mis proyectos</Typography>
                        <div className={classes.projectContainer}>
                            <Grid container spacing={2}>
                                {
                                    proyectos.map((value, index) => (
                                        <Grid key={index} item xl={3} lg={3} md={4} sm={12} xs={12}>
                                            <TarjetaProyecto
                                                id={value.idProyecto}
                                                nombre={value.nombreProyecto}
                                                cliente={value.nombreCliente ? value.nombreCliente : ''}
                                                estadoActual={value.estadoActual}
                                                imagenProyecto={value.imagenURL} />
                                        </Grid>
                                    ))
                                }
                            </Grid>
                        </div>
                    </div>
            }
        </div>
    )
}

export default MisProyectos
