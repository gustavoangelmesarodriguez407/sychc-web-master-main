import React, { useState, useEffect } from 'react'
import { Redirect, Link } from 'react-router-dom'
import {
    Typography, Grid, Paper, Divider,
    Button, List, ListItem, ListItemText,
    ListItemIcon, ListItemAvatar, Avatar, LinearProgress
} from '@material-ui/core'
import { DataGrid, GridToolbar } from '@material-ui/data-grid'
import { useStyles } from '../useStyles'

/**COmponents */
import CrearActividad from '../actividades/CrearActividad'
import CalendarioActividades from '../../../components/graficas/admin/CalendarioActividades'

/**APis */
import { GetApi, GetByIdApi } from '../../../api/Services'

/**Helpse */
import dateISOFormatter, { datetimeISOFormatter } from '../../../helpers/DateFormatter'
import { BASE_PATH, API } from '../../../helpers/ApiConfig'

/**Icons */
import Brightness1Icon from '@material-ui/icons/Brightness1'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'
import EventAvailableIcon from '@material-ui/icons/EventAvailable'
import ScheduleIcon from '@material-ui/icons/Schedule'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'
import CategoryIcon from '@material-ui/icons/Category'
import BookIcon from '@material-ui/icons/Book'
import ClearIcon from '@material-ui/icons/Clear'
import TimelineIcon from '@material-ui/icons/Timeline'

function VerProyecto(props) {
    const classes = useStyles()
    const { match: { params: { id } } } = props
    const session_id = window.localStorage.getItem('SESSION_ID')

    const [tipoUsuario, setTipoUsuario] = useState(null)
    const [proyecto, setProyecto] = useState(null)
    const [etapasProyecto, setEtapasProyecto] = useState([])
    const [consultoresProyecto, setConsultoresProyecto] = useState([])
    const [actividadesProyecto, setActividadesProyecto] = useState([])
    const [openCreateForm, setOpenCreateForm] = useState(false)
    const [isLoading, setIsLoading] = useState(true)
    const [reload, setReload] = useState(false)
    const [notFound, setNotFound] = useState(false)

    const actividadesColumns = [
        { field: 'id', headerName: 'ID', hide: true, },
        { field: 'nombreCategoria', headerName: 'Categoría',width: 125 },
        { field: 'idConsultor', headerName: 'ID de consultor',width: 200, hide: true },
        { field: 'descripcion', headerName: 'Descripción',width: 300},
        { field: 'fechaInicio', headerName: 'Fecha Inicio', type: 'date',width: 175 },
        { field: 'fechaFinal', headerName: 'Fecha Final', type: 'date',width: 150 },
        {
            field: 'detalles',
            headerName: 'Detalles',
             width: 150,
            renderCell: params => (
                <Link className={classes.link} to={`/${tipoUsuario === 'admin' || tipoUsuario === 'gerente' ? 'admin' : 'panel'}/ver-actividad/${params.getValue(params.id, 'id')}`}>
                    <Button variant='contained' color='primary' size='small'>
                        Ver actividad
                    </Button>
                </Link>
            )
        }
    ]

    //Efecto que trae la info del prpyecto
    useEffect(() => {
        //Trae tipo de usuario de la sesion
        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setTipoUsuario(ses.tipoUsuario)
                })
            } else {
                setNotFound(true)
            }
        })

        GetByIdApi('proyecto', id).then(response => {
            if (response.status === 200) {
                response.result.then(proyt => {
                    setProyecto(proyt)
                    //document.title = `${proyt.nombre} - SyCHC`
                    setIsLoading(false)
                })
            } else {
                setNotFound(true)
            }
        })

        //Busca las etapas del proyecto
        GetApi(`etapas_proyecto/lista-etapas/${id}`).then(response => {
            if (response.status === 200) {
                response.result.then(etps => {
                    setEtapasProyecto(etps)
                })
            } else {
                setEtapasProyecto([])
            }
        })

        //Buscar los consultores del proyecto
        GetApi(`consultores_proyecto/lista-consultores/${id}`).then(response => {
            if (response.status === 200) {
                response.result.then(etps => {
                    setConsultoresProyecto(etps)
                })
            } else {
                setConsultoresProyecto([])
            }
        })

        //Buscar los actividades del proyecto
        GetApi(`actividad/lista-actividades/${id}`).then(response => {
            if (response.status === 200) {
                response.result.then(acts => {
                    let a = []

                    acts.forEach(element => {
                        a.push({ ...element,fechaInicio: datetimeISOFormatter(element.fechaInicio), 
                            fechaFinal: datetimeISOFormatter(element.fechaFinal) })
                    });

                    setActividadesProyecto(a)
                })
            } else {
                setActividadesProyecto([])
            }
        })

        //Para la recarfa
        setReload(false)

        return () => {
            setEtapasProyecto([])
            setProyecto([])
            setConsultoresProyecto([])
            setActividadesProyecto([])
        }
    }, [id, session_id, reload])

    //Si no fue encontrado
    if (notFound) {
        return <Redirect to='/panel/mis-proyectos' />
    }

    //En lo que carga la info
    if (isLoading) {
        return <LinearProgress variant='indeterminate' />
    }

    return (
        <div>
            <CrearActividad
                open={openCreateForm}
                handleClose={() => setOpenCreateForm(false)}
                handleReload={() => setReload(true)}
                idProyecto={id}
                nombreProyecto={proyecto.nombre}
                etapas={etapasProyecto} />
            <img
                src={`${BASE_PATH}/${API}/archivo/${proyecto.imagenURL}`}
                alt={proyecto.imagenURl}
                className={classes.image} />
            <Typography variant='h3' gutterBottom>
                {proyecto.nombre}
            </Typography>

            <div className={classes.infoLabel}>
                <Brightness1Icon style={{
                    color:
                        proyecto.estadoActual === 'Iniciado' ?
                            '#36A5DD' :
                            proyecto.estadoActual === 'En Proceso' ?
                                '#DE5235' :
                                proyecto.estadoActual === 'Terminado' ?
                                    '#36DD36' :
                                    proyecto.estadoActual === 'Cancelado' ?
                                        '#DE3552' : '#8A36DD'
                }}
                    className={classes.infoIcon} />
                <Typography>
                    {proyecto.estadoActual}
                </Typography>
            </div>

            <Grid container spacing={2}>
                <Grid item xl={8} lg={8} md={8} sm={12} xs={12}>
                    <div className={classes.infoContainer}>
                        <Typography variant='h6'>
                            Descripción
                        </Typography>
                        <Typography variant='body1'>
                            {proyecto.descripcion}
                        </Typography>
                    </div>

                    <Divider className={classes.divider} />

                    <div className={classes.infoContainer}>
                        <Typography variant='h6'>
                            Información específica
                        </Typography>
                        <div className={classes.infoLabel}>
                            <CalendarTodayIcon className={classes.infoIcon} />
                            <Typography>
                                <span style={{ fontWeight: 'bold' }}> Fecha de inicio: </span>
                                {dateISOFormatter(proyecto.fechaInicio)}
                            </Typography>
                        </div>
                        <div className={classes.infoLabel}>
                            <EventAvailableIcon className={classes.infoIcon} />
                            <Typography>
                                <span style={{ fontWeight: 'bold' }}> Fecha de término: </span>
                                {dateISOFormatter(proyecto.fechaFinal)}
                            </Typography>
                        </div>
                        <div className={classes.infoLabel}>
                            <ScheduleIcon className={classes.infoIcon} />
                            <Typography>
                                <span style={{ fontWeight: 'bold' }}> Horas estimadas de trabajo: </span>
                                {`${proyecto.horasEstimadas} hrs.`}
                            </Typography>
                        </div>
                        <div className={classes.infoLabel}>
                            <AttachMoneyIcon className={classes.infoIcon} />
                            <Typography>
                                <span style={{ fontWeight: 'bold' }}> Precio estimado del proyecto: </span>
                                {`$${proyecto.precioEstimado} ${proyecto.moneda}`}
                            </Typography>
                        </div>
                    </div>

                    <Divider className={classes.divider} />

                    <div className={classes.infoContainer}>
                        <Typography variant='h6'>
                            Seguimiento de actividades y progreso
                        </Typography>

                        <div className={classes.infoLabel}>
                            <CategoryIcon className={classes.infoIcon} />
                            <Typography>
                                <span style={{ fontWeight: 'bold' }}> Etapa actual: </span>
                                {
                                    proyecto.etapaActual ? etapasProyecto.map(value => {
                                        if (value.idEtapa === proyecto.etapaActual)
                                            return value.nombre
                                        else
                                            return null
                                    }) : 'Sin etapa actual asignada.'
                                }
                            </Typography>
                        </div>

                        <div>
                            <Typography
                                className={classes.actividadTitle}>
                                Calendario de progreso
                            </Typography>
                            <CalendarioActividades idProyecto={id} />
                        </div>

                        <div>
                            <Typography
                                className={classes.actividadTitle}>
                                Lista de actividades
                            </Typography>
                            <div className={classes.actividadList}>
                                <DataGrid 
                                    columns={actividadesColumns}
                                    rows={actividadesProyecto}
                                    disableSelectionOnClick
                                    loading={isLoading}
                                    pageSize={25}
                                    components={{
                                        Toolbar: GridToolbar
                                    }} />
                            </div>
                        </div>
                    </div>
                </Grid>

                <Grid item xl={4} lg={4} md={4} sm={12} xs={12}>

                    {
                        //Solo los consultores pueden crear actividades
                        tipoUsuario === 'consultor' && (
                            <Paper className={classes.paperInfo} elevation={2}>
                                <Typography variant='h6' className={classes.subtitlePaper}>
                                    Opciones de proyecto
                                </Typography>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    className={classes.optionBtn}
                                    startIcon={<BookIcon />}
                                    onClick={() => setOpenCreateForm(true)}>
                                    Crear actividad
                                </Button>
                            </Paper>
                        )
                    }

                    <Paper className={classes.paperInfo} elevation={2}>
                        <Typography variant='h6' className={classes.subtitlePaper}>
                            Etapas
                        </Typography>
                        <List>
                            {
                                etapasProyecto.length > 0 ?
                                    etapasProyecto.map((value, index) => (
                                        <ListItem key={index}>
                                            <ListItemAvatar>
                                                <Avatar>
                                                    <CategoryIcon />
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText primary={value.nombre} />
                                        </ListItem>
                                    )) :
                                    <ListItem>
                                        <ListItemIcon>
                                            <ClearIcon />
                                        </ListItemIcon>
                                        <ListItemText primary='Sin etapas' />
                                    </ListItem>
                            }
                        </List>
                    </Paper>

                    <Paper className={classes.paperInfo} elevation={2}>
                        <Typography variant='h6' className={classes.subtitlePaper}>
                            Consultores asignados
                        </Typography>
                        <List>
                            {
                                consultoresProyecto.length > 0 ?
                                    consultoresProyecto.map((value, index) => (
                                        <ListItem key={index}>
                                            <ListItemAvatar>
                                                <Avatar>
                                                    <TimelineIcon />
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary={`${value.nombresConsultor} ${value.apellidosConsultor}`}
                                                secondary={value.nombrePerfil} />
                                        </ListItem>
                                    )) :
                                    <ListItem>
                                        <ListItemIcon>
                                            <ClearIcon />
                                        </ListItemIcon>
                                        <ListItemText primary='Sin consultores' />
                                    </ListItem>
                            }
                        </List>
                    </Paper>

                </Grid>
            </Grid>
        </div>
    )
}

export default VerProyecto
