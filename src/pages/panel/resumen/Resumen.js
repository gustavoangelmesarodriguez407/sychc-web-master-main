import React, { Fragment, useEffect, useState } from 'react'
import {
    Typography, Grid, Avatar, LinearProgress, Divider, List,
    ListItemAvatar, ListItemText, ListItem, ListItemIcon
} from '@material-ui/core'
import { useStyles } from '../useStyles'

/**Components */
import CalendarioActividadesConsultor from '../../../components/graficas/panel/CalendarioActividadesConsultor'

/**APis */
import { GetByIdApi, GetApi } from '../../../api/Services'

/**Helpers */
import { BASE_PATH, API } from '../../../helpers/ApiConfig'

/**Icons */
import TimelineIcon from '@material-ui/icons/Timeline'
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle'
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd'
import ClearIcon from '@material-ui/icons/Clear'

function Resumen() {
    const classes = useStyles()
    const [sesionData, setSesionData] = useState(null)
    const [perfilesConsultor, setPerfilesConsultor] = useState([])
    const [infoCliente, setInfoCliente] = useState(null)
    const session_id = window.localStorage.getItem('SESSION_ID')

    //Efecto que trae los datos de la sesión y tipo de usuario
    useEffect(() => {
        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setSesionData(ses)

                    if (ses.tipoUsuario === 'consultor') {
                        //Perfiles del consultor
                        GetApi(`perfiles_consultor/lista-perfiles/${ses.idUsuario}`).then(response => {
                            if (response.status === 200) {
                                response.result.then(dato => {
                                    setPerfilesConsultor(dato)
                                })
                            }
                        })
                    } else {
                        //Info de la empresa-cliente
                        GetApi(`clientes_usuario/info-cliente/${ses.idUsuario}`).then(response => {
                            if (response.status === 200) {
                                response.result.then(dato => {
                                    setInfoCliente(dato)
                                })
                            }
                        })
                    }
                })
            }
        })

        return () => {
            setSesionData([])
        }
    }, [session_id])

    const InicioConsultor = (props) => (
        <Fragment>
            <Grid container spacing={2} className={classes.grid}>
                <Grid item xl={4} lg={4} md={6} sm={12} xs={12}>
                    <div className={classes.resumenContainer}>
                        <Avatar className={classes.avatar}>
                            <TimelineIcon style={{ fontSize: '4em' }} />
                        </Avatar>
                        <Typography variant='caption'>Bienvenido</Typography>
                        <Typography variant='h4' gutterBottom>{props.datos.nombreUsuario}</Typography>
                        <Typography variant='h6'>{props.datos.correoElectronico}</Typography>
                        <Typography>{props.datos.tipoUsuario.toString().toUpperCase()}</Typography>
                    </div>
                    <Divider className={classes.divider} />
                    <Typography variant='h5' gutterBottom>Mis perfiles</Typography>
                    <List>
                        {
                            props.perfiles.length > 0 ?
                                props.perfiles.map((value, index) => {
                                    if (value.idConsultor === props.datos.idUsuario) {
                                        return (
                                            <ListItem key={index}>
                                                <ListItemAvatar>
                                                    <Avatar>
                                                        <AssignmentIndIcon />
                                                    </Avatar>
                                                </ListItemAvatar>
                                                <ListItemText
                                                    primary={value.nombre}
                                                    secondary={`${value.nivel} - ${value.aniosExperiencia} Años de experiencia.`} />
                                            </ListItem>
                                        )
                                    } else {
                                        return null
                                    }
                                }) :
                                <ListItem>
                                    <ListItemIcon>
                                        <ClearIcon />
                                    </ListItemIcon>
                                    <ListItemText
                                        primary={'Sin perfiles'} />
                                </ListItem>
                        }
                    </List>
                </Grid>
                <Grid item xl={8} lg={8} md={6} sm={12} xs={12}>
                    <Typography variant='h5' gutterBottom>Mi actividad</Typography>
                    <CalendarioActividadesConsultor idConsultor={props.datos.idUsuario} />
                </Grid>
            </Grid>
        </Fragment>
    )

    const InicioCliente = (props) => (
        <Fragment>
            <Grid container spacing={2} className={classes.grid}>
                <Grid item xl={4} lg={4} md={6} sm={12} xs={12}>
                    <div className={classes.resumenContainer}>
                        <Avatar className={classes.avatar}>
                            <PersonPinCircleIcon style={{ fontSize: '4em' }} />
                        </Avatar>
                        <Typography variant='caption'>Bienvenido</Typography>
                        <Typography variant='h4' gutterBottom>{props.datos.nombreUsuario}</Typography>
                        <Typography variant='h6'>{props.datos.correoElectronico}</Typography>
                        <Typography>{props.datos.tipoUsuario.toString().toUpperCase()}</Typography>
                    </div>
                </Grid>
                <Grid item xl={8} lg={8} md={6} sm={12} xs={12}>
                    <div className={classes.resumenContainer}>
                        <Typography variant='h5' gutterBottom>Perteneces a</Typography>
                        {
                            props.cliente ?
                                <Fragment>
                                    <img
                                        src={`${BASE_PATH}/${API}/archivo/${props.cliente.imagenURL}`}
                                        alt={props.cliente.imagenURL}
                                        className={classes.imagenCliente} />
                                    <Typography variant='h6'>{props.cliente.nombreCliente}</Typography>
                                    <Typography variant='caption'>{props.cliente.razonSocial}</Typography>
                                </Fragment> :
                                <LinearProgress variant='indeterminate' />
                        }
                    </div>
                </Grid>
            </Grid>
        </Fragment>
    )

    return (
        <div>
            <Typography variant='h4' gutterBottom>
                Inicio
            </Typography>
            {
                !sesionData ?
                    <LinearProgress variant='indeterminate' /> :
                    sesionData.tipoUsuario === 'consultor' ?
                        <InicioConsultor datos={sesionData} perfiles={perfilesConsultor} /> :
                        <InicioCliente datos={sesionData} cliente={infoCliente} />
            }
        </div>
    )
}

export default Resumen
