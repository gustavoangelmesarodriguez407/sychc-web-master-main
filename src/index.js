import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import theme from './styles/Theme'
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <App />,
  document.getElementById('root'),
  () => {
    if (theme.palette.type === 'light') {
      document.body.style.backgroundColor = '#FAFAFA'
      document.body.style.color = '#000'
    } else {
      document.body.style.backgroundColor = '#2F2F2F'
      document.body.style.color = '#FFF'
      
      
    }
  }
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
