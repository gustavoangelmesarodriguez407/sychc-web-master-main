import React from 'react'
import {
    Dialog, DialogTitle, DialogContent, DialogActions,
    Button
} from '@material-ui/core'

function Notification(props) {
    const { children, title, open, onClose, onAccept } = props

    return (
        <Dialog
            open={open}
            onClose={onClose}>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                {children}
            </DialogContent>
            <DialogActions>
                <Button color="primary" variant='contained' onClick={onAccept}>Aceptar</Button>
                <Button color="secondary" variant='contained' onClick={onClose}>Cancelar</Button>
            </DialogActions>
        </Dialog>
    )
}

export default Notification
