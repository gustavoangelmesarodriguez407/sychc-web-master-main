import React, { useState, useEffect } from 'react'
import { LinearProgress, Typography } from '@material-ui/core'
import { ResponsiveCalendar } from '@nivo/calendar'
import theme from '../../../styles/Theme'
import tints from '../../../styles/Tints'

/**APis */
import { GetApi } from '../../../api/Services'

/**Helpser */
import DateFormatter from '../../../helpers/DateFormatter'

function CalendarioActividadesConsultor(props) {
    const [datos, setDatos] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    //Efecto que trae los datos de la grafica
    useEffect(() => {
        GetApi(`consultor/grafica-calendario-consultor/${props.idConsultor}`).then(response => {
            if (response.status === 200) {
                response.result.then(datos => {
                    if (datos.length > 0) {
                        let act = []

                        //Guardo los datos en un arreglo para la grafica
                        for (let i = 0; i < datos.length; i++) {
                            act.push({ value: datos[i].cantidad, day: DateFormatter(datos[i].fechaActividadCorta) })
                        }

                        setDatos(act)
                        setIsLoading(false)
                    } else {
                        setIsLoading(false)
                    }
                })
            } else {
                setIsLoading(false)
            }
        })

        return () => {
            setDatos([])
        }
    }, [props.idConsultor])

    if (!isLoading && datos.length === 0) {
        return <Typography variant='h5'>Sin actividades</Typography>
    }

    return (
        <div>
            {
                isLoading ?
                    <LinearProgress variant='indeterminate' /> :
                    <div style={{ width: '100%', height: '300px', maxHeight: '380px', color: '#000' }}>
                        <ResponsiveCalendar
                            data={datos}
                            colors={tints}
                            theme={{
                                textColor: theme.palette.text.primary
                            }}
                            margin={{ top: 20, right: 20, bottom: 20, left: 20 }}
                            from={`${new Date().getFullYear()}-01-01`}
                            to={`${new Date().getFullYear()}-12-31`}
                            emptyColor={theme.palette.divider}
                            yearSpacing={35}
                            monthBorderColor={theme.palette.background.default}
                            dayBorderWidth={2}
                            dayBorderColor={theme.palette.background.default}
                            legends={[
                                {
                                    anchor: 'bottom-right',
                                    direction: 'row',
                                    translateY: 36,
                                    itemCount: 4,
                                    itemWidth: 42,
                                    itemHeight: 36,
                                    itemsSpacing: 14,
                                    itemDirection: 'right-to-left'
                                }
                            ]} />
                    </div>
            }
        </div>
    )
}

export default CalendarioActividadesConsultor
