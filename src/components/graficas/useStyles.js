import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2)
    },
    gridProyectos: {
        marginTop: theme.spacing(1)
    },
    estadoActualLabel: {
        textTransform: 'uppercase',
        fontWeight: 'bold'
    },
    graficaContainer: {
        height: '435px',
        color: '#000'
    }
}))