import React, { useState, useEffect } from 'react'
import { LinearProgress, Typography } from '@material-ui/core'
import { ResponsiveCalendar } from '@nivo/calendar'
import theme from '../../../styles/Theme'
import tints from '../../../styles/Tints'

/**APis */
import { GetApi, GetByIdApi } from '../../../api/Services'

/**Helpser */
import DateFormatter from '../../../helpers/DateFormatter'

function CalendarioActividades(props) {
    const [datos, setDatos] = useState([])
    const [proyecto, setProyecto] = useState({})
    const [isLoading, setIsLoading] = useState(true)

    //Efecto que trae los datos de la grafica
    useEffect(() => {
        GetByIdApi('proyecto', props.idProyecto).then(response => {
            if (response.status === 200) {
                response.result.then(pry => {
                    setProyecto(pry)
                })
            } else {
                setIsLoading(false)
            }
        })

        GetApi(`proyecto/grafica-calendario-proyecto/${props.idProyecto}`).then(response => {
            if (response.status === 200) {
                response.result.then(datos => {
                    if (datos.length > 0) {
                        let act = []

                        //Guardo los datos en un arreglo para la grafica
                        for (let i = 0; i < datos.length; i++) {
                            act.push({ value: datos[i].cantidad, day: DateFormatter(datos[i].fechaActividadCorta) })
                        }

                        setDatos(act)
                        setIsLoading(false)
                    } else {
                        setIsLoading(false)
                    }
                })
            } else {
                setIsLoading(false)
            }
        })

        return () => {
            setDatos([])
        }
    }, [props.idProyecto])

    if (!isLoading && datos.length === 0) {
        return <Typography variant='h5'>Sin actividades</Typography>
    }

    return (
        <div>
            {
                isLoading ?
                    <LinearProgress variant='indeterminate' /> :
                    <div style={{ height: '250px', maxHeight: '380px', color: '#000' }}>
                        <ResponsiveCalendar
                            data={datos}
                            colors={tints}
                            theme={{
                                textColor: theme.palette.text.primary
                            }}
                            margin={{ top: 20, right: 20, bottom: 20, left: 20 }}
                            from={DateFormatter(proyecto.fechaInicio)}
                            to={DateFormatter(proyecto.fechaFinal)}
                            emptyColor={theme.palette.divider}
                            yearSpacing={35}
                            monthBorderColor={theme.palette.background.default}
                            dayBorderWidth={2}
                            dayBorderColor={theme.palette.background.default}
                            legends={[
                                {
                                    anchor: 'bottom-right',
                                    direction: 'row',
                                    translateY: 36,
                                    itemCount: 4,
                                    itemWidth: 42,
                                    itemHeight: 36,
                                    itemsSpacing: 14,
                                    itemDirection: 'right-to-left'
                                }
                            ]} />
                    </div>
            }
        </div>
    )
}

export default CalendarioActividades
