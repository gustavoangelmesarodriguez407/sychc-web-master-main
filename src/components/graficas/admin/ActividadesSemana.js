import React, { useState, useEffect, Fragment } from 'react'
import { Paper, Typography, LinearProgress } from '@material-ui/core'
import { useStyles } from '../useStyles'
import theme from '../../../styles/Theme'
import { ResponsiveLine } from '@nivo/line'

/**APis */
import { GetApi } from '../../../api/Services'

/**Helpser */
import DateFormatter from '../../../helpers/DateFormatter'

function ActividadesSemana() {
    const classes = useStyles()
    const [datos, setDatos] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    //Efecto que trae los datos de la grafica
    useEffect(() => {
        GetApi('reporte/grafica-semana').then(response => {
            if (response.status === 200) {
                response.result.then(datos => {
                    if (datos.length > 0) {
                        let act = []

                        //Guardo los datos en un arreglo para la grafica
                        for (let i = 0; i < datos.length; i++) {
                            act.push({ x: DateFormatter(datos[i].fechaActividadCorta), y: datos[i].cantidad })
                        }

                        //Guardo en el estado
                        setDatos([{
                            id: 'Flujo',
                            color: theme.palette.primary.main,
                            data: act
                        }])
                        setIsLoading(false)
                    }
                })
            }
        })
        return () => {
            setDatos([])
        }
    }, [])


    return (
        <Paper className={classes.paper}>
            <Typography variant='h5' gutterBottom>
                Flujo de actividades de la semana
            </Typography>
            {
                isLoading ?
                    <LinearProgress variant='indeterminate' /> :
                    <Fragment>
                        <div className={classes.graficaContainer}>
                            <ResponsiveLine
                                data={datos}
                                colors={[theme.palette.primary.main]}
                                margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
                                curve='linear'
                                theme={{
                                    textColor: theme.palette.text.primary
                                }}
                                xScale={{ type: 'point' }}
                                yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: true, reverse: false }}
                                axisTop={null}
                                axisRight={null}
                                axisBottom={{
                                    orient: 'bottom',
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: 'Semana',
                                    legendOffset: 36,
                                    legendPosition: 'middle'
                                }}
                                axisLeft={{
                                    orient: 'left',
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: 'Cantidad de actividades',
                                    legendOffset: -40,
                                    legendPosition: 'middle'
                                }}
                                pointSize={10}
                                pointColor={{ from: 'color', modifiers: [] }}
                                pointBorderWidth={2}
                                pointBorderColor={{ from: 'serieColor' }}
                                pointLabelYOffset={-12}
                                useMesh={true}
                                legends={[
                                    {
                                        anchor: 'bottom-right',
                                        direction: 'column',
                                        justify: false,
                                        translateX: 100,
                                        translateY: 0,
                                        itemsSpacing: 0,
                                        itemDirection: 'left-to-right',
                                        itemWidth: 80,
                                        itemHeight: 20,
                                        itemOpacity: 0.75,
                                        symbolSize: 12,
                                        symbolShape: 'circle',
                                        symbolBorderColor: 'rgba(0, 0, 0, .5)',
                                        effects: [
                                            {
                                                on: 'hover',
                                                style: {
                                                    itemBackground: 'rgba(0, 0, 0, .03)',
                                                    itemOpacity: 1
                                                }
                                            }
                                        ]
                                    }
                                ]} />
                        </div>
                    </Fragment>
            }
        </Paper>
    )
}

export default ActividadesSemana
