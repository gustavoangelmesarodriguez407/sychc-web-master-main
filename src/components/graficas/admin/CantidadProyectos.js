import React, { useState, useEffect, Fragment } from 'react'
import { Paper, Typography, Grid, LinearProgress } from '@material-ui/core'
import { useStyles } from '../useStyles'

/**APis */
import { GetApi } from '../../../api/Services'

/**Icons */
import Brightness1Icon from '@material-ui/icons/Brightness1'

function CantidadProyectos() {
    const classes = useStyles()
    const [isLoading, setIsLoading] = useState(true)
    const [proyectos, setProyectos] = useState({
        total: 0,
        iniciados: 0,
        enProceso: 0,
        terminados: 0,
        cancelados: 0,
        enEspera: 0
    })

    //Efecto que trae info de los proyectos
    useEffect(() => {
        GetApi('proyecto/grafica-cantidad').then(response => {
            if (response.status === 200) {
                response.result.then(datos => {
                    if (datos.length > 0) {
                        //Guardo el arreglo de la respuesta
                        let pr = datos

                        //Variables para acumular
                        let iniciados = 0
                        let enProceso = 0
                        let terminados = 0
                        let cancelados = 0
                        let enEspera = 0
                        let total = 0

                        pr.forEach(element => {
                            //suma el total de los ptoyectos
                            total += element.cantidad

                            //Divide cantidad en estados acuatles
                            switch (element.estadoActual) {
                                case 'Iniciado':
                                    iniciados += element.cantidad
                                    break;
                                case 'En Proceso':
                                    enProceso += element.cantidad
                                    break;
                                case 'Terminado':
                                    terminados += element.cantidad
                                    break;
                                case 'Cancelado':
                                    cancelados += element.cantidad
                                    break;
                                case 'En Espera':
                                    enEspera += element.cantidad
                                    break;
                                default:
                                    break;
                            }
                        });

                        //Guarda el esatdo
                        setProyectos({
                            ...proyectos,
                            total: total,
                            iniciados: iniciados,
                            enProceso: enProceso,
                            terminados: terminados,
                            cancelados: cancelados,
                            enEspera: enEspera
                        })

                        //Para la carga
                        setIsLoading(false)
                    } else {
                        setIsLoading(false)
                    }
                })
            } else {
                setIsLoading(false)
            }
        })

        return () => {
            setProyectos({})
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div>
            <Paper className={classes.paper}>
                <Typography variant='h5' gutterBottom>
                    Cantidad de proyectos
                </Typography>
                {
                    isLoading ?
                        <LinearProgress variant='indeterminate' /> :
                        <Fragment>
                            <Typography variant='overline'>
                                {proyectos.total} proyectos registrados
                            </Typography>
                            <div>
                                <Grid container spacing={2} className={classes.gridProyectos}>
                                    <Grid item xl={4} lg={4} md={6} sm={6} xs={6}>
                                        <Paper className={classes.paper} variant='outlined'>
                                            <Brightness1Icon style={{ color: '#36A5DD' }} />
                                            <Typography className={classes.estadoActualLabel}>Iniciados</Typography>
                                            <Typography>{proyectos.iniciados}</Typography>
                                        </Paper>
                                    </Grid>
                                    <Grid item xl={2} lg={2} md={6} sm={6} xs={6}>
                                        <Paper className={classes.paper} variant='outlined'>
                                            <Brightness1Icon style={{ color: '#DE5235' }} />
                                            <Typography className={classes.estadoActualLabel}>En Proceso</Typography>
                                            <Typography>{proyectos.enProceso}</Typography>
                                        </Paper>
                                    </Grid>
                                    <Grid item xl={2} lg={2} md={6} sm={6} xs={6}>
                                        <Paper className={classes.paper} variant='outlined'>
                                            <Brightness1Icon style={{ color: '#36DD36' }} />
                                            <Typography className={classes.estadoActualLabel}>Terminados</Typography>
                                            <Typography>{proyectos.terminados}</Typography>
                                        </Paper>
                                    </Grid>
                                    <Grid item xl={2} lg={2} md={6} sm={6} xs={6}>
                                        <Paper className={classes.paper} variant='outlined'>
                                            <Brightness1Icon style={{ color: '#DE3552' }} />
                                            <Typography className={classes.estadoActualLabel}>Cancelados</Typography>
                                            <Typography>{proyectos.cancelados}</Typography>
                                        </Paper>
                                    </Grid>
                                    <Grid item xl={2} lg={2} md={6} sm={6} xs={6}>
                                        <Paper className={classes.paper} variant='outlined'>
                                            <Brightness1Icon style={{ color: '#8A36DD' }} />
                                            <Typography className={classes.estadoActualLabel}>En Espera</Typography>
                                            <Typography>{proyectos.enEspera}</Typography>
                                        </Paper>
                                    </Grid>
                                </Grid>
                            </div>
                        </Fragment>
                }
            </Paper>
        </div>
    )
}

export default CantidadProyectos
