import React, { useState, useEffect, Fragment } from 'react'
import { Paper, Typography, LinearProgress } from '@material-ui/core'
import { useStyles } from '../useStyles'
import { ResponsivePie } from '@nivo/pie'
import theme from '../../../styles/Theme'
import gradients from '../../../styles/Gradients'

/**APIs */
import { GetApi } from '../../../api/Services'

function CantidadUsuarios() {
    const classes = useStyles()
    const [datos, setDatos] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    //Efecto que trae los datos de la grafica
    useEffect(() => {
        GetApi('usuario/grafica-cantidad').then(response => {
            if (response.status === 200) {
                response.result.then(datos => {
                    if (datos.length > 0) {
                        let usr = []

                        //Guardo los datos en un arreglo para la grafica
                        for (let i = 0; i < datos.length; i++) {
                            usr.push({
                                id: datos[i].tipoUsuario,
                                label: datos[i].tipoUsuario.toString().toUpperCase(),
                                value: datos[i].cantidad
                            })
                        }

                        //Guardo en el estado
                        setDatos(usr)
                        setIsLoading(false)
                    }
                })
            } else {
                setIsLoading(false)
                setDatos([])
            }
        })
        return () => {
            setDatos([])
        }
    }, [])

    return (
        <Paper className={classes.paper}>
            <Typography variant='h5' gutterBottom>
                Distribución de usuarios
            </Typography>
            {
                isLoading ?
                    <LinearProgress variant='indeterminate' /> :
                    <Fragment>
                        <div className={classes.graficaContainer}>
                            <ResponsivePie
                                data={datos}
                                colors={gradients}
                                theme={{
                                    textColor: theme.palette.text.primary
                                }}
                                margin={{ top: 10, right: 80, bottom: 80, left: 100 }}
                                innerRadius={0.5}
                                padAngle={0.7}
                                cornerRadius={3}
                                activeOuterRadiusOffset={8}
                                arcLinkLabelsTextColor={{ from: 'color', modifiers: [] }}
                                legends={[
                                    {
                                        anchor: 'bottom',
                                        direction: 'column',
                                        justify: false,
                                        translateX: 0,
                                        translateY: 56,
                                        itemsSpacing: 0,
                                        itemWidth: 100,
                                        itemHeight: 18,
                                        itemTextColor: '#999',
                                        itemDirection: 'left-to-right',
                                        itemOpacity: 1,
                                        symbolSize: 18,
                                        symbolShape: 'circle',
                                        effects: [
                                            {
                                                on: 'hover',
                                                style: {
                                                    itemTextColor: '#000'
                                                }
                                            }
                                        ]
                                    }]} />
                        </div>
                    </Fragment>
            }
        </Paper>
    )
}

export default CantidadUsuarios
