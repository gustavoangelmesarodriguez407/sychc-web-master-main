/*eslint array-callback-return: 0 */
import React, { useState, useRef } from 'react'
import {
    Grid, Typography, Paper,
    TableContainer, Table, TableHead, TableBody, TableRow,
    TableCell,
    FormControlLabel,
    Switch, Button
} from '@material-ui/core'
import { useStyles } from './useStyles'
import { useReactToPrint } from 'react-to-print'

/**Helpers */
import DateFormatter, { datetimeISOFormatter } from '../../helpers/DateFormatter'

/**Images */
import sychc_logo from '../../assets/svg/sychc-logo-light-v2.webp'
import vsi_logo from '../../assets/svg/vsi-logo-light.svg'

/**Icons */
import GetAppIcon from '@material-ui/icons/GetApp'

function ReportePlantillaProyecto(props) {
    const { datos } = props
    const classes = useStyles()
    //Estados para mostrar secciones
    const [mostrarActividades, setMostrarActividades] = useState(true)
    const [mostrarCostos, setMostrarCostos] = useState(true)
    const [mostrarMontos, setMostrarMontos] = useState(false)

    //Impresión en PDF
    const refReporteDocumento = useRef()
    const handlePrint = useReactToPrint({
        content: () => refReporteDocumento.current,
        documentTitle: `Reporte de proyecto ${DateFormatter(new Date())}`
    });

    return (
        <div>
            <Grid container spacing={1} className={classes.grid}>
                <Grid item lg={4}>
                    <FormControlLabel
                        label='Mostrar desglose de actividades'
                        control={
                            <Switch
                                color='primary'
                                checked={mostrarActividades}
                                onChange={(e) => setMostrarActividades(e.target.checked)} />
                        } />
                </Grid>
                <Grid item lg={4}>
                    <FormControlLabel
                        label='Mostrar resumen por consultor'
                        control={
                            <Switch
                                color='primary'
                                checked={mostrarCostos}
                                onChange={(e) => setMostrarCostos(e.target.checked)} />
                        } />
                </Grid>
                <Grid item lg={4}>
                    <FormControlLabel
                        label='Mostrar montos totales del proyecto'
                        control={
                            <Switch
                                color='primary'
                                checked={mostrarMontos}
                                onChange={(e) => setMostrarMontos(e.target.checked)} />
                        } />
                </Grid>
            </Grid>
            <div>
                <Button
                    color='primary'
                    variant='contained'
                    startIcon={<GetAppIcon />}
                    onClick={handlePrint}>
                    Guardar como archivo PDF
                </Button>
                <div className={classes.pagina} ref={refReporteDocumento}>
                    <div className={classes.encabezado}>
                        <img src={vsi_logo} alt='vsi.svg' className={classes.logovsi} />
                        <img src={sychc_logo} alt='sychc.svg' className={classes.logosychc} />
                    </div>
                    <Typography variant='h4' className={classes.title}>
                        Reporte de Proyecto
                    </Typography>
                    <div className={classes.grid}>
                        <Typography style={{ fontSize: 18 }}><span className={classes.dataTitle}>Nombre del proyecto:</span> {datos.proyecto}</Typography>
                        <Typography><span className={classes.dataTitle}>Nombre del cliente:</span> {datos.cliente}</Typography>
                        <Typography><span className={classes.dataTitle}>Nombre del Consultor:</span> {datos.consultor}<span className={classes.dataTitle}>{" "}Etapa:{" "}</span>{datos.etapa}</Typography>
                        <Typography></Typography>
                        <Typography><span className={classes.dataTitle}>Total de Horas Trabajadas:{" "}</span> {datos.totalHoras}{" "}
                            <span className={classes.dataTitle}> {` `} Horas Facturables:{" "}</span> {datos.totalHorasFacturables}
                            <span className={classes.dataTitle}> {"    "}Horas no Facturables:{" "}</span> {datos.totalHorasNoFacturables}
                        </Typography>
                        <Typography><span className={classes.dataTitle}>Periodo:</span> {datos.periodo}</Typography>
                    </div>
                    {
                        mostrarActividades && (
                            <div>
                                <Typography variant='subtitle1' className={classes.subtitle}>
                                    Desglose de actividades
                                </Typography>
                                <TableContainer component={Paper} className={classes.table}>
                                    <Table size='small'>
                                        <TableHead className={classes.tableHeader}>
                                            <TableRow>
                                                <TableCell>Categoría</TableCell>
                                                <TableCell>Etapa</TableCell>
                                                <TableCell>Consultor</TableCell>
                                                <TableCell>Actividad</TableCell>
                                                <TableCell>Fecha Inicio</TableCell>
                                                <TableCell>Fecha Final</TableCell>
                                                <TableCell>Horas Trabajadas</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                datos.actividades.map((value, index) => (
                                                    <TableRow key={index}>
                                                        <TableCell>{value.categoriaName}</TableCell>
                                                        <TableCell>{value.nombreEtapa}</TableCell>
                                                        <TableCell>{value.nombreConsultor}</TableCell>
                                                        <TableCell>{value.descripcionActividad}</TableCell>
                                                        <TableCell className={classes.center}>{datetimeISOFormatter(value.fechaInicioA)}</TableCell>
                                                        <TableCell className={classes.center}>{datetimeISOFormatter(value.fechaFinalF)}</TableCell>
                                                        <TableCell className={classes.center} >{value.horasTrabajadas}</TableCell>
                                                    </TableRow>
                                                ))
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </div>
                        )
                    }
                    {
                        mostrarCostos && (
                            <div>
                                <Typography variant='subtitle1' className={classes.subtitle}>
                                    Resumen de costos por consultor
                                </Typography>
                                <TableContainer component={Paper} className={classes.table}>
                                    <Table size='small'>
                                        <TableHead className={classes.tableHeader}>
                                            <TableRow>
                                                <TableCell>Consultor</TableCell>
                                                <TableCell>Costo (Tarifa)</TableCell>
                                                <TableCell>Horas Facturables Trabajadas</TableCell>
                                                <TableCell>Subtotal ($USD)</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                datos.tarifas.map((value, index) => {
                                                    if (datos.consultor === 'Todos los consultores') {
                                                        return (
                                                            <TableRow key={index}>
                                                                <TableCell>{value.consultor}</TableCell>
                                                                <TableCell>{Number(value.costo).toFixed(2)}</TableCell>
                                                                <TableCell>{value.horasTrabajadas}</TableCell>
                                                                <TableCell>{Number(value.horasTrabajadas * value.costo).toFixed(2)}</TableCell>
                                                            </TableRow>
                                                        )
                                                    } else {
                                                        if (value.consultor === datos.consultor) {
                                                            return (
                                                                <TableRow key={index}>
                                                                    <TableCell>{value.consultor}</TableCell>
                                                                    <TableCell>{Number(value.costo).toFixed(2)}</TableCell>
                                                                    <TableCell>{value.horasTrabajadas}</TableCell>
                                                                    <TableCell>{Number(value.horasTrabajadas * value.costo).toFixed(2)}</TableCell>
                                                                </TableRow>
                                                            )
                                                        }
                                                    }
                                                })
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </div>
                        )
                    }
                    {
                        mostrarMontos && (
                            <div>
                                <Typography variant='subtitle1' className={classes.subtitle}>
                                    Montos totales del proyecto (Todos los consultores)
                                </Typography>
                                <TableContainer component={Paper} className={classes.table}>
                                    <Table size='small'>
                                        <TableHead className={classes.tableHeader}>
                                            <TableRow>
                                                <TableCell>Subtotal ($USD)</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <TableRow>
                                                <TableCell>{Number(datos.subtotal).toFixed(2)}</TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </div>
                        )
                    }
                    <div className={classes.pieDePagina}>
                        <Typography variant='caption'>
                            Reporte generado por el Sistema de Control de Horas de Consultoría
                        </Typography>
                        <br />
                        <Typography variant='caption'>
                            &copy; {new Date().getFullYear().toString()} Ventiv Solutions International
                        </Typography>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ReportePlantillaProyecto
