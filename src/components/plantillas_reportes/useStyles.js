import { makeStyles } from '@material-ui/core'
import { withThemeCreator } from '@material-ui/styles'

export const useStyles = makeStyles(theme => ({
    title: {
        textAlign: 'center',
        marginBottom: theme.spacing(2)
    },
    paper: {
        padding: theme.spacing(1),
        fontSize: 10
    },
    dataTitle: {
        fontWeight: 'bold',
        marginBottom: theme.spacing(0.5),
        marginLeft: 9
    },
    grid: {
        marginBottom: theme.spacing(2)
    },
    subtitle: {
        marginBottom: theme.spacing(1)
    },
    table: {
        marginBottom: theme.spacing(2)
    },
    tableHeader: {
        backgroundColor: theme.palette.primary.light,
        //color: theme.palette.primary.contrastText,
    },
    pagina: {
        margin: theme.spacing(2)
    },
    encabezado: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: theme.spacing(1.5)
    },
    logosychc: {
        width: 120
    },
    logovsi: {
        width: 150
    },
    pieDePagina: {
        marginBottom: theme.spacing(2),
        textAlign: 'center',
        pageBreakBefore: 'auto'
    },
    center:{
        textAlign: 'center',
    },
}))