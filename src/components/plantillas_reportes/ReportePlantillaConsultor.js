import React, { useState, useRef } from 'react'
import {
    Grid, Typography, Paper,
    TableContainer, Table, TableHead, TableBody, TableRow,
    TableCell,
    FormControlLabel,
    Switch, Button
} from '@material-ui/core'
import { useStyles } from './useStyles'
import { useReactToPrint } from 'react-to-print'

/**Helpers */
import DateFormatter from '../../helpers/DateFormatter'

/**Images */
import sychc_logo from '../../assets/svg/sychc-logo-light-v2.webp'
import vsi_logo from '../../assets/svg/vsi-logo-light.svg'

/**Icons */
import GetAppIcon from '@material-ui/icons/GetApp'

function ReportePlantillaConsultor(props) {
    const { datos } = props
    const classes = useStyles()
    //Estados para mostrar secciones
    const [mostrarListaProyectos, setMostrarListaProyectos] = useState(true)

    //Impresión en PDF
    const refReporteDocumento = useRef()
    const handlePrint = useReactToPrint({
        content: () => refReporteDocumento.current,
        documentTitle: `Reporte de consultor ${DateFormatter(new Date())}`
    });

    return (
        <div>
            <Grid container spacing={1} className={classes.grid}>
                <Grid item lg={4}>
                    <FormControlLabel
                        label='Mostrar lista de proyectos asignados'
                        control={
                            <Switch
                                color='primary'
                                checked={mostrarListaProyectos}
                                onChange={(e) => setMostrarListaProyectos(e.target.checked)} />
                        } />
                </Grid>
            </Grid>
            <div>
                <Button
                    color='primary'
                    variant='contained'
                    startIcon={<GetAppIcon />}
                    onClick={handlePrint}>
                    Guardar como archivo PDF
                </Button>
                <div className={classes.pagina} ref={refReporteDocumento}>
                    <div className={classes.encabezado}>
                        <img src={vsi_logo} alt='vsi.svg' className={classes.logovsi} />
                        <img src={sychc_logo} alt='sychc.svg' className={classes.logosychc} />
                    </div>
                    <Typography variant='h4' className={classes.title}>
                        Reporte de consultor
                    </Typography>
                    <Typography variant='subtitle1' className={classes.subtitle}>
                        Información general
                    </Typography>
                    <div className={classes.grid}>
                        <Typography style={{ fontSize: 18 }}><span className={classes.dataTitle}>Nombre del Consultor:</span> {datos.consultor}</Typography>
                        <Typography><span className={classes.dataTitle}>Nombre del proyecto:</span> {datos.proyecto}</Typography>
                        <Typography><span className={classes.dataTitle}>Nombre del cliente:</span> {datos.cliente}</Typography>
                        <Typography><span className={classes.dataTitle}>Periodo:</span> {datos.periodo}</Typography>
                    </div>
                    <div>
                        <Typography variant='subtitle1' className={classes.subtitle}>
                            Resumen de horas de trabajo
                        </Typography>
                        <TableContainer component={Paper} className={classes.table}>
                            <Table size='small'>
                                <TableHead className={classes.tableHeader}>
                                    <TableRow>
                                        <TableCell>Proyecto</TableCell>
                                        <TableCell>Cliente</TableCell>
                                        <TableCell>Total de Horas Facturables</TableCell>
                                        <TableCell>Total de Horas no Facturables</TableCell>
                                        <TableCell>Total de Horas Trabajadas</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {
                                        datos.cliente !== null && datos.cliente === "Todos los clientes" && (
                                            datos.clienteArray.map((value, index) => (
                                                <TableRow key={index}>
                                                    <TableCell>Todos los proyectos de {value.nombreCliente}</TableCell>
                                                    <TableCell>{value.nombreCliente}</TableCell>
                                                    <TableCell>{value.totalHorasFacturables}</TableCell>
                                                    <TableCell>{value.totalHorasNoFacturables}</TableCell>
                                                    <TableCell>{value.totalHoras}</TableCell>
                                                </TableRow>
                                            ))
                                        )
                                    }
                                    <TableRow>
                                        <TableCell>{datos.proyecto} en General</TableCell>
                                        <TableCell>{datos.cliente}</TableCell>
                                        <TableCell>{datos.totalHorasFacturables}</TableCell>
                                        <TableCell>{datos.totalHorasNoFacturables}</TableCell>
                                        <TableCell>{datos.totalHoras}</TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                    {
                        mostrarListaProyectos && (
                            <div>
                                <Typography variant='subtitle1' className={classes.subtitle}>
                                    Lista de proyectos asignados
                                </Typography>
                                <TableContainer component={Paper} className={classes.table}>
                                    <Table size='small'>
                                        <TableHead className={classes.tableHeader}>
                                            <TableRow>
                                                <TableCell>Proyecto</TableCell>
                                                <TableCell>Cliente</TableCell>
                                                <TableCell>Estado Actual</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                datos.listaProyectos.map((value, index) => (
                                                    <TableRow key={index}>
                                                        <TableCell>{value.nombreProyecto}</TableCell>
                                                        <TableCell>{value.nombreCliente}</TableCell>
                                                        <TableCell>{value.estadoActual}</TableCell>
                                                    </TableRow>
                                                ))
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </div>
                        )
                    }
                    <div className={classes.pieDePagina}>
                        <Typography variant='caption'>
                            Reporte generado por el Sistema de Control de Horas de Consultoría
                        </Typography>
                        <br />
                        <Typography variant='caption'>
                            &copy; {new Date().getFullYear().toString()} Ventiv Solutions International
                        </Typography>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ReportePlantillaConsultor
