import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    },
    title: {
        flexGrow: 1,
        textAlign: 'end'
    },
    logo: {
        width: '120px',
        marginTop: theme.spacing(0.5)
    }
}))

export default useStyles