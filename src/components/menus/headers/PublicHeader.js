import React from 'react';
import { AppBar, Toolbar, Typography, Container } from '@material-ui/core';
import useStyles from './useStylesPublicHeader';
import theme from '../../../styles/Theme'

/**Imagenes */
import logoLight from '../../../assets/svg/sychc-logo-light-v2.webp'
import logoDark from '../../../assets/svg/sychc-logo-dark-v2.webp'

function PublicHeader() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position='static' color='inherit'>
                <Toolbar>
                    <Container>
                        {
                            theme.palette.type === 'light' ?
                                <img
                                    src={logoLight}
                                    alt='sychc.svg'
                                    className={classes.logo} /> :
                                <img
                                    src={logoDark}
                                    alt='sychc.svg'
                                    className={classes.logo} />
                        }
                    </Container>
                    <Typography variant='h6' className={classes.title}>
                        Sistema de Control de Horas de Consultoría
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default PublicHeader
