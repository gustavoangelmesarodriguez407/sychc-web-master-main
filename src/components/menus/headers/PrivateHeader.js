import React, { useState, useEffect, Fragment } from 'react'
import {
    AppBar, Toolbar,
    Container, IconButton, Menu, MenuItem,
    Tooltip, Hidden, withStyles,
    Typography
} from '@material-ui/core'
import theme from '../../../styles/Theme'
import { useStyles } from '../useStylesMenu'

/**APIs */
import logout from '../../../helpers/Logout'
import { GetByIdApi } from '../../../api/Services'

/**Imagenes */
import logoLight from '../../../assets/svg/sychc-logo-light-v2.webp'
import logoDark from '../../../assets/svg/sychc-logo-dark-v2.webp'

/**Iconos */
import MenuIcon from '@material-ui/icons/Menu'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import PersonIcon from '@material-ui/icons/Person'
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew'
import NightsStayIcon from '@material-ui/icons/NightsStay'
import Brightness5Icon from '@material-ui/icons/Brightness5'

function Header(props) {
    const classes = useStyles()
    const [anchorEl, setAnchorEl] = useState(null)
    const [sesionData, setSesionData] = useState(null)
    const session_id = window.localStorage.getItem('SESSION_ID')

    useEffect(() => {
        GetByIdApi('sesion', session_id).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setSesionData(ses)
                })
            } else {
                setSesionData(null)
            }
        })

        return () => setSesionData([])
    }, [session_id])

    const HtmlTooltip = withStyles((theme) => ({
        tooltip: {
            backgroundColor: '#f5f5f9',
            color: 'rgba(0, 0, 0, 0.87)',
            maxWidth: 220,
            fontSize: theme.typography.pxToRem(12),
            border: '1px solid #dadde9',
        },
    }))(Tooltip);

    const closeMenu = () => setAnchorEl(false)

    return (
        <AppBar className={classes.app}>
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton}
                    aria-label="menu" onClick={() => props.OpenAction()}>
                    <MenuIcon className={classes.menuIcon} />
                </IconButton>
                <Container>
                    <Hidden smUp>
                        {
                            theme.palette.type === 'light' ?
                                <img
                                    src={logoLight}
                                    alt='sychc.svg'
                                    className={classes.logoHeader} /> :
                                <img
                                    src={logoDark}
                                    alt='sychc.svg'
                                    className={classes.logoHeader} />
                        }
                    </Hidden>
                </Container>
                <HtmlTooltip
                    title={
                        <Fragment>
                            <Typography color="inherit">{sesionData ? sesionData.nombreUsuario : 'Usuario'}</Typography>
                            <em>{sesionData ? sesionData.correoElectronico : 'Correo electrónico'}</em>
                            <br />
                            <br />
                            {sesionData ? sesionData.tipoUsuario.toString().toUpperCase() : 'Usuario'}
                        </Fragment>
                    }>
                    <IconButton
                        color='inherit'
                        style={{
                            marginInline: 5
                        }}>
                        <PersonIcon />
                    </IconButton>
                </HtmlTooltip>
                <Tooltip title='Opciones'>
                    <IconButton
                        color='inherit'
                        style={{
                            marginInline: 5
                        }}
                        aria-controls="option-menu"
                        onClick={(event) => setAnchorEl(event.currentTarget)}>
                        <MoreVertIcon />
                    </IconButton>
                </Tooltip>
                <Menu
                    id="option-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={closeMenu}>
                    <MenuItem disabled>
                        Paleta de colores
                    </MenuItem>
                    <MenuItem
                        selected={theme.palette.type === 'light'}
                        onClick={() => {
                            window.localStorage.setItem('PALETTE', 'light')
                            window.location.reload()
                        }}>
                        <Brightness5Icon className={classes.iconMenu} />
                        Modo claro
                    </MenuItem>
                    <MenuItem
                        divider
                        selected={theme.palette.type === 'dark'}
                        onClick={() => {
                            window.localStorage.setItem('PALETTE', 'dark')
                            window.location.reload()
                        }}>
                        <NightsStayIcon className={classes.iconMenu} />
                        Modo oscuro
                    </MenuItem>
                    <MenuItem
                        onClick={() => logout(session_id)}>
                        <PowerSettingsNewIcon className={classes.iconMenu} />
                        Cerrar sesión
                    </MenuItem>
                </Menu>
            </Toolbar>
        </AppBar>
    )
}

export default Header
