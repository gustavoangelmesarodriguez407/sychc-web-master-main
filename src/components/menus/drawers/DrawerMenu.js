import React, { useState, useEffect } from 'react'
import { Drawer, LinearProgress, Typography } from '@material-ui/core'
import { useStyles } from '../useStylesMenu'
import theme from '../../../styles/Theme'

/**Logos */
import logoLight from '../../../assets/svg/sychc-logo-light-v2.webp'
import logoDark from '../../../assets/svg/sychc-logo-dark-v2.webp'
import logoVsiLight from '../../../assets/svg/vsi-logo-light.svg'
import logoVsiDark from '../../../assets/svg/vsi-logo-dark.svg'

/**Lista */
import AdminMenuList from '../lists/AdminMenuList'
import PanelMenuList from '../lists/PanelMenuList'

/**APIs */
import { GetByIdApi } from '../../../api/Services'
import GerenteMenuList from '../lists/GerenteMenuList'

function AdminDrawerMenu(props) {
    const classes = useStyles()
    const [sesionData, setSesionData] = useState(null)

    //Efecto que trae la info del usuario
    useEffect(() => {
        GetByIdApi('sesion', window.localStorage.getItem('SESSION_ID')).then(response => {
            if (response.status === 200) {
                response.result.then(ses => {
                    setSesionData(ses)
                })
            } else {
                setSesionData(null)
            }
        })

        return () => {
            setSesionData({})
        }
    }, [])

    if (!sesionData) {
        return <LinearProgress variant='indeterminate' />
    }

    return (
        <Drawer
            className={classes.drawer}
            variant={props.variant}
            classes={{ paper: classes.drawerPaper }}
            open={props.open}
            onClose={props.onClose ? props.onClose : null}
            anchor="left">
            <div className={classes.toolbar}>
                <div className={classes.logo}>
                    <img src={theme.palette.type === 'light' ?
                        logoVsiLight : logoVsiDark} alt='vsi.svg'
                        className={classes.logoTop} />
                </div>
            </div>
            {
                sesionData.tipoUsuario === 'admin' ?
                    <AdminMenuList close={props.onClose} url={props.url} /> :
                    sesionData.tipoUsuario === 'gerente' ?
                        <GerenteMenuList close={props.onClose} url={props.url} /> :
                        <PanelMenuList close={props.onClose} url={props.url} />
            }
            <div className={classes.toolbar}>
                <div className={classes.logo}>
                    <img src={theme.palette.type === 'light' ?
                        logoLight : logoDark} alt='sychc.svg'
                        className={classes.logoSize} />
                </div>
            </div>
            <div style={{ margin: 10 }}>
                <Typography style={{ fontSize: '12px', fontWeight: 'bold' }}>
                    Sistema de Control de Horas de Consultoría
                </Typography>
                <Typography style={{ fontSize: '12px' }}>
                    v1.0
                </Typography>
            </div>
        </Drawer>
    )
}

export default AdminDrawerMenu
