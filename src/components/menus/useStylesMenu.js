import { makeStyles } from '@material-ui/core'

const drawerWidth = 240
export const useStyles = makeStyles((theme) => ({
    //Header
    menuButton: {
        marginRight: theme.spacing(2),
        color: theme.palette.text.primary,
        [theme.breakpoints.up('sm')]: {
            display: 'none'
        }
    },
    menuIcon: {
        color: theme.palette.text.primary
    },
    iconMenu: {
        marginRight: theme.spacing(1)
    },
    link: {
        textDecoration: 'none',
        color: theme.palette.text.primary,
        '&:hover': {
            textDecoration: 'none',
            color: theme.palette.text.primary,
        }
    },
    app: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        backgroundColor: theme.palette.background.paper,
        color: theme.palette.text.primary
    },
    logoHeader: {
        width: '53%',
        marginTop: 6
    },

    //Drawer
    drawer: {
        width: drawerWidth,
        flexShrink: 0
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        backgroundColor: theme.palette.background.paper
    },
    logo: {
        textAlign: "center",
        marginTop: theme.spacing(1)
    },
    logoSize: {
        width: '53%'
    },
    logoTop: {
        width: '80%'
    },

    //List
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper
    },
    list: {
        backgroundColor: theme.palette.background.paper
    },
    menuListIcon: {
        color: theme.palette.type === 'light' ?
            theme.palette.secondary.main : theme.palette.text.primary
    },
    sectionIcon: {
        color: theme.palette.primary.main
    },
    subSectionIcon: {
        color: theme.palette.primary.light
    },
    nested: {
        paddingLeft: theme.spacing(4),
    }
}))