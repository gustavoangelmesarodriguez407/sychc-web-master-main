import React, { useState } from 'react'
import { Link } from "react-router-dom";
import {
    List, ListItem, ListItemIcon, ListItemText, Collapse
} from "@material-ui/core"
import { useStyles } from '../useStylesMenu'

/**Icons */
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'

import SecurityIcon from '@material-ui/icons/Security'
import BookmarksIcon from '@material-ui/icons/Bookmarks'
import DataUsageIcon from '@material-ui/icons/DataUsage'

import HomeIcon from '@material-ui/icons/Home'
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks'
import GroupIcon from '@material-ui/icons/Group'
import ContactsIcon from '@material-ui/icons/Contacts'
import TimelineIcon from '@material-ui/icons/Timeline'
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd'
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle'
import GroupWorkIcon from '@material-ui/icons/GroupWork'
import CategoryIcon from '@material-ui/icons/Category'
import BookIcon from '@material-ui/icons/Book'
import ViewModuleIcon from '@material-ui/icons/ViewModule'
import FeaturedPlayListIcon from '@material-ui/icons/FeaturedPlayList'
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
import AccessAlarmIcon from '@material-ui/icons/AccessAlarm'
import AppsIcon from '@material-ui/icons/Apps';

function AdminMenuList(props) {
    const classes = useStyles()
    const [openSeguridad, setOpenSeguridad] = useState(false)
    const [openCatalogos, setOpenCatalogos] = useState(false)
    const [openRegistros, setOpenRegistros] = useState(false)
    const { url } = props

    return (
        <div className={classes.root}>
            <List component="nav" className={classes.list}>

                <Link to={'/admin'} className={classes.link}>
                    <ListItem button onClick={props.close} selected={url === '/admin' ? true : false}>
                        <ListItemIcon>
                            <HomeIcon className={classes.sectionIcon} />
                        </ListItemIcon>
                        <ListItemText primary="Inicio" />
                    </ListItem>
                </Link>

                <ListItem button onClick={() => setOpenCatalogos(!openCatalogos)}>
                    <ListItemIcon>
                        <BookmarksIcon className={classes.sectionIcon} />
                    </ListItemIcon>
                    <ListItemText primary="Catálogos" />
                    {openCatalogos ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={openCatalogos} timeout="auto" unmountOnExit>
                    <Link to={'/admin/perfiles'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/perfiles' ? true : false}>
                            <ListItemIcon>
                                <AssignmentIndIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Perfiles" />
                        </ListItem>
                    </Link>
                    <Link to={'/admin/consultores'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/consultores' ? true : false}>
                            <ListItemIcon>
                                <TimelineIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Consultores" />
                        </ListItem>
                    </Link>
                    <Link to={'/admin/clientes'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/clientes' ? true : false}>
                            <ListItemIcon>
                                <PersonPinCircleIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Clientes" />
                        </ListItem>
                    </Link>
                    <Link to={'/admin/etapas'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/etapas' ? true : false}>
                            <ListItemIcon>
                                <CategoryIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Etapas" />
                        </ListItem>
                    </Link>
                    <Link to={'/admin/categoria'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/categoria' ? true : false}>
                            <ListItemIcon>
                            <AppsIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Categoria" />
                        </ListItem>
                    </Link>
                    <Link to={'/admin/proyectos'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/proyectos' ? true : false}>
                            <ListItemIcon>
                                <GroupWorkIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Proyectos" />
                        </ListItem>
                    </Link>
                </Collapse>

                <ListItem button onClick={() => setOpenRegistros(!openRegistros)}>
                    <ListItemIcon>
                        <DataUsageIcon className={classes.sectionIcon} />
                    </ListItemIcon>
                    <ListItemText primary="Registros" />
                    {openRegistros ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={openRegistros} timeout="auto" unmountOnExit>
                    <Link to={'/admin/actividades'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/actividades' ? true : false}>
                            <ListItemIcon>
                                <BookIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Actividades" />
                        </ListItem>
                    </Link>
                </Collapse>

                <Link to={'/admin/reportes'} className={classes.link}>
                    <ListItem button onClick={props.close} selected={url === '/admin/reportes' ? true : false}>
                        <ListItemIcon>
                            <LibraryBooksIcon className={classes.sectionIcon} />
                        </ListItemIcon>
                        <ListItemText primary="Reportes" />
                    </ListItem>
                </Link>

                <ListItem button onClick={() => setOpenSeguridad(!openSeguridad)}>
                    <ListItemIcon>
                        <SecurityIcon className={classes.sectionIcon} />
                    </ListItemIcon>
                    <ListItemText primary="Seguridad" />
                    {openSeguridad ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={openSeguridad} timeout="auto" unmountOnExit>
                    <Link to={'/admin/usuarios'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/usuarios' ? true : false}>
                            <ListItemIcon>
                                <GroupIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Usuarios" />
                        </ListItem>
                    </Link>
                    <Link to={'/admin/tiposusuarios'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/tiposusuarios' ? true : false}>
                            <ListItemIcon>
                                <ContactsIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Tipos de usuario" />
                        </ListItem>
                    </Link>
                    <Link to={'/admin/modulos'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/modulos' ? true : false}>
                            <ListItemIcon>
                                <ViewModuleIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Módulos" />
                        </ListItem>
                    </Link>
                    <Link to={'/admin/funciones'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/funciones' ? true : false}>
                            <ListItemIcon>
                                <FeaturedPlayListIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Funciones" />
                        </ListItem>
                    </Link>
                    <Link to={'/admin/accesos'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/accesos' ? true : false}>
                            <ListItemIcon>
                                <MeetingRoomIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Accesos" />
                        </ListItem>
                    </Link>
                    <Link to={'/admin/sesiones'} className={classes.link}>
                        <ListItem button className={classes.nested} onClick={props.close} selected={url === '/admin/sesiones' ? true : false}>
                            <ListItemIcon>
                                <AccessAlarmIcon className={classes.subSectionIcon} />
                            </ListItemIcon>
                            <ListItemText primary="Sesiones" />
                        </ListItem>
                    </Link>
                </Collapse>
            </List>
        </div>
    )
}

export default AdminMenuList
