import React from 'react'
import { Link } from "react-router-dom";
import {
    List, ListItem, ListItemIcon, ListItemText, Divider
} from "@material-ui/core"
import { useStyles } from '../useStylesMenu'

/**Icons */
import HomeIcon from '@material-ui/icons/Home'
import GroupWorkIcon from '@material-ui/icons/Home'
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks'
//import LibraryBooksIcon from '@material-ui/icons/LibraryBooks'

function PanelMenuList(props) {
    const classes = useStyles()
    const { url } = props

    return (
        <div className={classes.root}>
            <List component="nav" className={classes.list}>
                <Link to={'/panel'} className={classes.link}>
                    <ListItem button onClick={props.close} selected={url === '/panel' ? true : false}>
                        <ListItemIcon>
                            <HomeIcon className={classes.sectionIcon} />
                        </ListItemIcon>
                        <ListItemText primary="Inicio" />
                    </ListItem>
                </Link>
                <Link to={'/panel/mis-proyectos'} className={classes.link}>
                    <ListItem button onClick={props.close} selected={url === '/panel/mis-proyectos' ? true : false}>
                        <ListItemIcon>
                            <GroupWorkIcon className={classes.sectionIcon} />
                        </ListItemIcon>
                        <ListItemText primary="Mis proyectos" />
                    </ListItem>
                </Link>
                <Link to={'/panel/reportes'} className={classes.link}>
                    <ListItem button onClick={props.close} selected={url === '/panel/reportes' ? true : false}>
                        <ListItemIcon>
                            <LibraryBooksIcon className={classes.sectionIcon} />
                        </ListItemIcon>
                        <ListItemText primary="Reportes" />
                    </ListItem>
                </Link>
                <Divider />
            </List>
        </div>
    )
}

export default PanelMenuList
