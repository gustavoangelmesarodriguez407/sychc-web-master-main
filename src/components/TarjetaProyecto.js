import React from 'react'
import { Link } from 'react-router-dom'
import {
    Typography, Button,
    Card, CardMedia, CardActionArea, CardContent,
    CardActions,
    makeStyles
} from '@material-ui/core'

/**Helpers */
import { BASE_PATH, API } from '../helpers/ApiConfig'

/**Icons */
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import Brightness1Icon from '@material-ui/icons/Brightness1'

const useStyles = makeStyles(theme => ({
    media: {
        height: 140,
    },
    estadoContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    link: {
        color: theme.palette.text.primary,
        textDecoration: 'none',
        '&:hover': {
            color: theme.palette.text.primary
        }
    }
}))

function TarjetaProyecto(props) {
    const classes = useStyles()

    return (
        <Card elevation={2}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    className={classes.media}
                    image={`${BASE_PATH}/${API}/archivo/${props.imagenProyecto}`}
                    title={props.nombre} />
            </CardActionArea>
            <CardContent>
                <Typography gutterBottom variant="h6" component="h2">
                    {props.nombre}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                    {props.cliente}
                </Typography>

                <div className={classes.estadoContainer}>
                    <Brightness1Icon size='small' style={{
                        color:
                            props.estadoActual === 'Iniciado' ?
                                '#36A5DD' :
                                props.estadoActual === 'En Proceso' ?
                                    '#DE5235' :
                                    props.estadoActual === 'Terminado' ?
                                        '#36DD36' :
                                        props.estadoActual === 'Cancelado' ?
                                            '#DE3552' : '#8A36DD',
                        marginRight: 6
                    }} />
                    <Typography variant="body2" color="textSecondary" component="p">
                        {props.estadoActual}
                    </Typography>
                </div>
            </CardContent>
            <CardActions>
                <Link to={`/panel/ver-proyecto/${props.id}`} className={classes.link}>
                    <Button
                        color='inherit'
                        endIcon={<ArrowForwardIosIcon />}>
                        Ver proyecto
                    </Button>
                </Link>
            </CardActions>
        </Card>
    )
}

export default TarjetaProyecto
