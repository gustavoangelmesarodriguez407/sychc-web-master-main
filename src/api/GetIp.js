export function getIpApi() {
    return fetch('https://api.ipify.org?format=json').then(response => {
        return response.json()
    }).then(result => {
        return result
    }).catch(err => {
        return err.message
    })
}