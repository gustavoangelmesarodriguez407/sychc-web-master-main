import { BASE_PATH, API } from '../helpers/ApiConfig'

export function GetApi(controller) {
    const session_id = window.localStorage.getItem('SESSION_ID')
    const url = `${BASE_PATH}/${API}/${controller}`
    const params = {
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
            "Session_id": session_id
        }
    }

    return fetch(url, params)
        .then(response => {
            if (response.status === 200) {
                return {
                    status: 200,
                    result: response.json()
                }
            } else {
                return {
                    status: response.status,
                    result: response.text()
                }
            }
        }).then(result => {
            return result
        }).catch(err => {
            return err.message
        })
}

export function GetByIdApi(controller, id) {
    const session_id = window.localStorage.getItem('SESSION_ID')
    const url = `${BASE_PATH}/${API}/${controller}/${id}`
    const params = {
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
            "Session_id": session_id
        }
    }

    return fetch(url, params)
        .then(response => {
            if (response.status === 200) {
                return {
                    status: 200,
                    result: response.json()
                }
            } else {
                return {
                    status: response.status,
                    result: response.text()
                }
            }
        }).then(result => {
            return result
        }).catch(err => {
            return err.message
        })
}

export function PostApi(controller, data) {
    const session_id = window.localStorage.getItem('SESSION_ID')
    const url = `${BASE_PATH}/${API}/${controller}`
    const params = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Session_id": session_id
        }
    }

    return fetch(url, params)
        .then(response => {
            if (response.status === 200) {
                return {
                    status: 200,
                    result: response.json()
                }
            } else {
                return {
                    status: response.status,
                    result: response.text()
                }
            }
        }).then(result => {
            return result
        }).catch(err => {
            return err.message
        })
}

export function PutApi(controller, id, data) {
    const session_id = window.localStorage.getItem('SESSION_ID')
    const url = `${BASE_PATH}/${API}/${controller}/${id}`
    const params = {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
            "Session_id": session_id
        }
    }

    return fetch(url, params)
        .then(response => {
            if (response.status === 200) {
                return {
                    status: 200,
                    result: response.json()
                }
            } else {
                return {
                    status: response.status,
                    result: response.text()
                }
            }
        }).then(result => {
            return result
        }).catch(err => {
            return err.message
        })
}

export function DeleteApi(controller, id) {
    const session_id = window.localStorage.getItem('SESSION_ID')
    const url = `${BASE_PATH}/${API}/${controller}/${id}`
    const params = {
        method: 'DELETE',
        headers: {
            "Content-Type": "application/json",
            "Session_id": session_id
        }
    }

    return fetch(url, params)
        .then(response => {
            return {
                status: response.status,
                result: response.text()
            }
        }).then(result => {
            return result
        }).catch(err => {
            return err.message
        })
}

export function UploadFileApi(file) {
    const dataForm = new FormData()
    dataForm.append('archivo', file)

    const url = `${BASE_PATH}/${API}/archivo`
    const params = {
        method: 'POST',
        body: dataForm
    }

    return fetch(url, params)
        .then(response => {
            return {
                status: response.status,
                result: response.text()
            }
        }).then(result => {
            return result
        }).catch(err => {
            return err.message
        })
}