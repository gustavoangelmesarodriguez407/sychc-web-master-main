import { BASE_PATH, API } from '../helpers/ApiConfig'

export function iniciarSesionApi(data) {
    const url = `${BASE_PATH}/${API}/sesion`
    const params = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        }
    }

    return fetch(url, params)
        .then(response => {
            if (response.status === 200) {
                return {
                    status: 200,
                    result: response.json()
                }
            } else {
                return {
                    status: response.status,
                    result: response.text()
                }
            }
        }).then(result => {
            return result
        }).catch(err => {
            return err.message
        })
}

export function obtenerSesionApi(clave) {
    const url = `${BASE_PATH}/${API}/sesion/${clave}`
    const params = {
        method: 'GET',
        headers: {
            "Content-Type": "application/json"
        }
    }

    return fetch(url, params)
        .then(response => {
            if (response.status === 200) {
                return {
                    status: 200,
                    result: response.json()
                }
            } else {
                return {
                    status: response.status,
                    result: response.text()
                }
            }
        }).then(result => {
            return result
        }).catch(err => {
            return err.message
        })
}

export function eliminarSesionApi(clave) {
    const url = `${BASE_PATH}/${API}/sesion/${clave}`
    const params = {
        method: 'DELETE',
        headers: {
            "Content-Type": "application/json"
        }
    }

    return fetch(url, params)
        .then(response => {
            if (response.status === 200) {
                return {
                    status: 200,
                    result: response.json()
                }
            } else {
                return {
                    status: response.status,
                    result: response.text()
                }
            }
        }).then(result => {
            return result
        }).catch(err => {
            return err.message
        })
}