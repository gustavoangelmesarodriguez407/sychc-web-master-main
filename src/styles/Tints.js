const tints = [
    '#FFFFFF',
    '#FDF1F1',
    '#FAE2E2',
    '#F8D4D4',
    '#F5C6C6',
    '#F3B7B7',
    '#F0A9A9',
    '#EE9B9B',
    '#EC8C8C',
    '#E97E7E',
    '#E77070',
    '#E46161',
    '#E25353',
    '#DF4545',
    '#DD3434'
]

export default tints