import { createTheme } from '@material-ui/core/styles'
const palette = window.localStorage.getItem('PALETTE')

const theme = createTheme({
    typography: {
        fontFamily: "Open Sans, sans-serif",
        button: {
            textTransform: "none",
            fontSize: "1rem"
        }
    },

    palette: {
        type: palette ? palette : 'light',
        primary: {
            light: "#E25353",
            main: "#DD3434",
            dark: "#DA2626",
            contrastText: "#FFFFFF"
        },
        secondary: {
            light: "#929292",
            main: "#666666",
            dark: "#5F5F5F",
            contrastText: "#FFFFFF"
        }
    },
})

export default theme