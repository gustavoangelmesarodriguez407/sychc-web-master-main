const gradients = [
    '#DD3434',
    '#DE5235',
    '#DE6F35',
    '#DE8C35',
    '#DEA935',
    '#DEC635',
    '#D9DE35',
    '#BCDE35',
    '#BC35DE',
    '#D935DE',
    '#DE35C6',
    '#DE35A9',
    '#DE358C',
    '#DE356F',
    '#DE3552'
]

export default gradients