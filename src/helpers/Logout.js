import { eliminarSesionApi } from '../api/Sesion'

export default function logout(clave) {
    eliminarSesionApi(clave).then(response => {
        if (response.status === 200) {
            //Elimina las variables locales
            window.localStorage.removeItem('SESSION_ID')
            window.localStorage.removeItem('ID')
            window.localStorage.removeItem('TYPE')

            //Redirige al login
            window.location.href = '/'
        } else {
            //Redirige al login
            window.location.href = '/'
        }
    })
}