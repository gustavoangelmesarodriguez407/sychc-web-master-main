export default function dateISOFormatter(fullDate) {
    const date = new Date(fullDate);
    const day = date.getDate()
    const month = date.getMonth() + 1
    const year = date.getFullYear()

    return `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`
}

export function datetimeISOFormatter(fullDateTime) {
    const date = new Date(fullDateTime);
    const day = date.getDate()
    const month = date.getMonth() + 1
    const year = date.getFullYear()

    return `${day < 10 ? '0' + day : day}-${month < 10 ? '0' + month : month}-${year} `
}